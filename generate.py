import argparse
import json
import os
# import yaml
# from ruamel.yaml import yaml
import ruamel.yaml as yaml
import shutil

from unidecode import unidecode

SUBJECT_MAP = {
    'morning': 'Matin · Morning',
    'evening': 'Soir · Evening',
    'annunciation': 'Annonciation · Annunciation',
    'advent': 'Avent · Advent',
    'christmas': 'Noël · Christmas',
    'epiphany': 'Epiphanie · Epiphany',
    'candlemas': 'Chandeleur · Candlemas',
    'shrovetide': 'Avant le Carême · Shrovetide',
    'lent': 'Carême · Lent',
    'passion': 'Passion · Passiontide',
    'easter': 'Pâques · Easter',
    'ascension': 'Ascension · Ascension',
    'pentecost': 'Pentecôte · Pentecost',
    'trinity': 'Trinité · Trinity',
    'rogation': 'Rogation · Rogation',
    'praise': 'Louange · Praise',
    'communion': 'Communion · Communion',
    'dedication': "Dedication d'une église · Dedication of a church",
    'thanksgiving': 'Action de grâce · Thanksgiving',
    'sea': 'Sur mer · At sea',
    'militant': "L'Eglise militante · The Church militant",
    'triumphant': "L'Eglise triomphante · The Church triumphant"
}

class GenericScalar:
    def __init__(self, value, tag, style=None):
        self._value = value
        self._tag = tag
        self._style = style

    @staticmethod
    def to_yaml(dumper, data):
        # data is a GenericScalar
        return dumper.represent_scalar(data._tag, data._value, style=data._style)


def default_constructor(loader, tag_suffix, node):
    if isinstance(node, yaml.ScalarNode):
        return GenericScalar(node.value, tag_suffix, style=node.style)
    else:
        raise NotImplementedError('Node: ' + str(type(node)))

def get_lyrics_only(filename_no_name):
    table = []
    table.append('=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"\n')
    # table.append('\t| | |')
    # table.append('\t| - | - |')
    table.append('<table style="font-size:16px">')

    ly_file = os.path.join('lilypond', 'ly', '{0}.ly'.format(filename_no_name))
    verses = []
    in_verse = False
    with open(ly_file, 'r', encoding='utf-8') as fp:
        lines = [x.strip() for x in fp.readlines()]
    for line in lines:
        split_line = line.split()
        if line and split_line[0] in ['verseOne', 'verseTwo', 'verseThree', 'verseFour']:
            in_verse = True
            verse = []
        if line and '\\line { \\bold' in line:
            in_verse = True
            verse = []
        if in_verse and '}' in line:
            in_verse = False
            verses.append(verse)
        if in_verse:
            verse.append(line)

    # Get first four verses
    verses_clean = []
    for verse in verses:
        verse = [x for x in verse if '%' not in x]
        verse = [x for x in verse if '{' not in x]
        verse = [x for x in verse if 'stanza' not in x]
        verse = [x for x in verse if 'Melismata' not in x]
        verse = [x for x in verse if '\\break' not in x]
        verse = [x for x in verse if '\\repeat' not in x]
        verse = [x.replace('\\l ', '') for x in verse]
        verse = [x.replace(' -- _ ', '') for x in verse]
        verse = [x.replace(' _ ', ' ') for x in verse]
        verse = [x.replace(' -- ', '') for x in verse]
        verse = [x.replace(' -', '-') for x in verse]
        verse = [x.replace('"', '') for x in verse]
        verse = [x.replace("_", '') for x in verse]
        verse = [x for x in verse if x.strip() != ""]
        verse = '<br>'.join(verse)
        verses_clean.append(verse)
    print(verses_clean)

    for i, verse in enumerate(verses_clean):
        # table.append('\t| {0} | {1} |'.format(i + 1, verse))
        table.append('<tr>')
        table.append('<td> {0} </td>'.format(i + 1))
        table.append('<td> {0} </td>'.format(verse))
        table.append('</tr>')
    
    table.append('</table>')

    table = ['\t{0}'.format(x) if i > 0 else x for i, x in enumerate(table)]

    table = '\n'.join(table)
    return table


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--book', action='store_true', help='Generate the book.')
    parser.add_argument('--site', action='store_true', help='Generate the site.')
    parser.add_argument('--local', action='store_true', help='For local dev.')
    parser.add_argument('--remote', action='store_true', help='Before pushing to remote.')
    args = parser.parse_args()

    yaml.add_multi_constructor('', default_constructor, Loader=yaml.SafeLoader)
    yaml.add_representer(GenericScalar, GenericScalar.to_yaml, Dumper=yaml.SafeDumper)

    with open('lca.json', 'r') as fp:
        lca = json.load(fp)

    if args.site:
        # local_key = 'b0fd7fc5c26f449287753b36e70c6e7f'
        # remote_key = 'd7518740bf7a4af7b4c3a8b5a7bcff85'
        # if args.local:
        #     key = local_key
        #     adobe_url = '/assets/pdf/{1}.pdf'
        # elif args.remote:
        #     key = remote_key
        #     adobe_url = '/assets/pdf/{1}.pdf'
        # else:
        #     raise ValueError('Provide --local or --remote.')

        with open('mkdocs.yml', 'r') as fp:
            mkdocs_yml = yaml.safe_load(fp)
        mkdocs_yml['nav'] = [{'Accueil · Home': 'index.md'}]
        mkdocs_yml['nav'].append({'Cantiques · Hymns': []})

        for k, v in SUBJECT_MAP.items():
            subject_dir = os.path.join('docs', 'hymns', k)
            if os.path.exists(subject_dir):
                shutil.rmtree(subject_dir)
            os.makedirs(subject_dir)
            mkdocs_yml['nav'][1]['Cantiques · Hymns'].append({v: []})

        index = []

        for i, hymn in enumerate(lca):
            num = i + 1
            title = hymn['title'].encode("latin1").decode("utf_8")
            safe_title = unidecode(title)
            text = [x.encode("latin_1").decode("utf_8") for x in hymn['text']]
            tune = hymn['music']['tune'].encode("latin_1").decode("utf_8")
            source = [x.encode("latin_1").decode("utf_8") for x in hymn['music']['source']]

            subject = hymn['subject'].encode("latin_1").decode("utf_8")
            subject_dir = os.path.join('docs', 'hymns', subject)

            filename_title = safe_title.replace(' ', '_').replace(',', '').replace(';', '').replace('!', '').replace('?', '').replace('-', '_').lower()
            filename_tune = unidecode(tune.replace("'", '').replace(' ', '_').replace(',', '').upper())
            filename = '{0}_{1}_{2}'.format(num, filename_title, filename_tune)
            filename_no_num = '{0}_{1}'.format(filename_title, filename_tune)
            md_path = os.path.join(subject_dir, '{0}.md'.format(filename))

            # local_key = 'b0fd7fc5c26f449287753b36e70c6e7f'
            local_key = 'd0ec35d952174ebfb03c3e15eae8ce10'
            remote_key = 'd7518740bf7a4af7b4c3a8b5a7bcff85'
            if args.local:
                key = local_key
                adobe_url = '/assets/pdf/{0}.pdf'.format(filename_no_num)
            elif args.remote:
                key = remote_key
                adobe_url = '/livre-de-cantiques-anglicans/assets/pdf/{0}.pdf'.format(filename_no_num)
            else:
                raise ValueError('Provide --local or --remote.')

            nav_info = {title: 'hymns/{0}/{1}.md'.format(subject, filename)}
            found = False
            i = 0
            while not found:
                try:
                    mkdocs_yml['nav'][1]['Cantiques · Hymns'][i][SUBJECT_MAP[subject]].append(nav_info)
                except KeyError:
                    i = i + 1
                    continue
                found = True

            info = {'num': num, 'first_line': title, 'tune': tune, 'url': '\\'.join(md_path.split('\\')[1:]).replace('.md', ''), 'subject': subject}
            # print('\\'.join(info['url'].split('\\')[1:]))
            index.append(info)

            # Write code for main page for each hymn
            pdf_js = """\t<div id="adobe-dc-view" style="width: 100%;"></div>
\t<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
\t<script type="text/javascript">
    \tdocument.addEventListener("adobe_dc_view_sdk.ready", function(){{
        \tvar adobeDCView = new AdobeDC.View({{clientId: "{0}", divId: "adobe-dc-view"}});
        \tadobeDCView.previewFile({{
            \tcontent:{{location: {{url: "{1}"}}}},
            \tmetaData:{{fileName: "{2}.pdf"}}}}, {{embedMode: "IN_LINE"}});
    \t}});
\t</script>""".format(key, adobe_url, filename_no_num)

            print(md_path)
            with open(md_path, 'w', encoding='utf-8') as fp:
                # Tags come before everything; always include subject
                fp.write('---\n')
                fp.write('tags:\n')
                fp.write('  - {0}\n'.format(SUBJECT_MAP[subject]))
                if 'tags' in hymn:
                    for tag in hymn['tags']:
                        tag = tag.encode("latin1").decode("utf_8")
                        fp.write('  - {0}\n'.format(tag))
                fp.write('---\n\n')
                # Then title
                fp.write('# {0}\n\n'.format(title))
                fp.write('\n\n')
                # Score
                fp.write('=== ":material-playlist-music: Partition · Score"\n\n')
                fp.write(pdf_js)
                fp.write('\n\n')
                # Lyrics
                lyrics_only = get_lyrics_only(filename_no_num)
                fp.write(lyrics_only)
                fp.write('\n\n')
                # Info
                fp.write('## Information\n')
                # fp.write('<div class="info">\n\n')
                fp.write('| | |\n')
                fp.write('| - | - |\n')
                if hymn['original_title']:
                    fp.write('| Titre original · Original title | {0} |\n'.format(hymn['original_title'].encode("latin1").decode("utf_8")))
                fp.write('| Texte · Text  | {0} |\n'.format('<br/>'.join(text)))
                fp.write('| Musique · Music | *{0}* <br/> {1} |'.format(tune, '<br/>'.join(source)))
                # fp.write('</div>')

        # Index by first line
        mkdocs_yml['nav'].append({'Index · Indices': []})

        index_first_line = {}
        for hymn in index:
            if hymn['first_line'] not in index_first_line:
                index_first_line[hymn['first_line']] = [[hymn['num']], [hymn['tune']], [hymn['url']], [hymn['subject']]]
            else:
                index_first_line[hymn['first_line']][0].append(hymn['num'])
                index_first_line[hymn['first_line']][1].append(hymn['tune'])
                index_first_line[hymn['first_line']][2].append(hymn['url'])
                index_first_line[hymn['first_line']][3].append(hymn['subject'])
        index_first_line = dict(sorted(index_first_line.items()))

        index_dir = os.path.join('docs', 'index')
        if os.path.exists(index_dir):
            shutil.rmtree(index_dir)
        os.makedirs(index_dir)

        mkdocs_yml['nav'][2]['Index · Indices'].append({'Première phrase · First line': 'index/premiere_phrase_first_line.md'})

        with open(os.path.join(index_dir, 'premiere_phrase_first_line.md'), 'w', encoding='utf-8') as fp:
            starting_letter = ''
            for k, v in index_first_line.items():
                new_starting_letter = k[0]
                if new_starting_letter != starting_letter:
                    fp.write('\n\n## {0}\n\n'.format(new_starting_letter))
                    fp.write('| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number \n')
                    fp.write('| - | - | - | - |\n')
                    starting_letter = new_starting_letter
                row = '| {0} | *{1}* | {4} | [{2}](../../{3}) |'.format(k, v[1][0], v[0][0], v[2][0], SUBJECT_MAP[v[3][0]])
                fp.write('{0}\n'.format(row))
                if len(v[1]) > 1:
                    for i, item in enumerate(v[1]):
                        if i == 0:
                            continue
                        row = '| | *{0}* | {3} | [{1}](../../{2}) |'.format(v[1][i], v[0][i], v[2][i], SUBJECT_MAP[v[3][i]])
                        fp.write('{0}\n'.format(row))

        # Index by tune
        index_tune = {}
        for hymn in index:
            if hymn['tune'] not in index_tune:
                index_tune[hymn['tune']] = [[hymn['num']], [hymn['first_line']], [hymn['url']], [hymn['subject']]]
            else:
                index_tune[hymn['tune']][0].append(hymn['num'])
                index_tune[hymn['tune']][1].append(hymn['first_line'])
                index_tune[hymn['tune']][2].append(hymn['url'])
                index_tune[hymn['tune']][3].append(hymn['subject'])
        index_tune = dict(sorted(index_tune.items()))

        mkdocs_yml['nav'][2]['Index · Indices'].append({'Mélodie · Tune': 'index/melodie_tune.md'})

        with open(os.path.join(index_dir, 'melodie_tune.md'), 'w', encoding='utf-8') as fp:
            starting_letter = ''
            for k, v in index_tune.items():
                new_starting_letter = k[0]
                if new_starting_letter != starting_letter:
                    fp.write('\n\n## {0}\n\n'.format(new_starting_letter))
                    fp.write('| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number \n')
                    fp.write('| - | - | - | - |\n')
                    starting_letter = new_starting_letter
                row = '| {0} | *{1}* | {4} | [{2}](../../{3}) |'.format(k, v[1][0], v[0][0], v[2][0], SUBJECT_MAP[v[3][0]])
                fp.write('{0}\n'.format(row))
                if len(v[1]) > 1:
                    for i, item in enumerate(v[1]):
                        if i == 0:
                            continue
                        row = '| | *{0}* | {3} | [{1}](../../{2}) |'.format(v[1][i], v[0][i], v[2][i], SUBJECT_MAP[v[3][i]])
                        fp.write('{0}\n'.format(row))

        # Index by subject
        index_subject = {}
        for hymn in index:
            if hymn['subject'] not in index_subject:
                index_subject[hymn['subject']] = [[hymn['num']], [hymn['first_line']], [hymn['tune']], [hymn['url']]]
            else:
                index_subject[hymn['subject']][0].append(hymn['num'])
                index_subject[hymn['subject']][1].append(hymn['first_line'])
                index_subject[hymn['subject']][2].append(hymn['tune'])
                index_subject[hymn['subject']][3].append(hymn['url'])

        mkdocs_yml['nav'][2]['Index · Indices'].append({'Sujet · Subject': 'index/sujet_subject.md'})

        with open(os.path.join(index_dir, 'sujet_subject.md'), 'w', encoding='utf-8') as fp:
            starting_subject = ''
            for k, v in index_subject.items():
                new_starting_subject = SUBJECT_MAP[k]
                if new_starting_subject != starting_subject:
                    fp.write('\n\n## {0}\n\n'.format(new_starting_subject))
                    fp.write('| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number \n')
                    fp.write('| - | - | - |\n')
                    starting_subject = new_starting_subject
                for i in range(len(v[1])):
                    row = '| {0} | *{1}* | [{2}](../../{3}) |'.format(v[1][i], v[2][i], v[0][i], v[3][i])
                    fp.write('{0}\n'.format(row))
                # if len(v[1]) > 1:
                #     for i, item in enumerate(v[1]):
                #         if i == 0:
                #             continue
                #         row = '| | *{0}* | [{1}](../../{2}) |'.format(v[1][i], v[0][i], v[2][i])
                #         fp.write('{0}\n'.format(row))

        # Tags page
        mkdocs_yml['nav'].append({'Etiquettes · Tags': 'etiquettes_tags.md'})

        # Write mkdocs_yml
        with open('mkdocs.yml', 'w', encoding='utf-8') as fp:
            # yaml.dump(mkdocs_yml, fp)
            yaml.safe_dump(mkdocs_yml, fp, default_flow_style=False, allow_unicode=True)

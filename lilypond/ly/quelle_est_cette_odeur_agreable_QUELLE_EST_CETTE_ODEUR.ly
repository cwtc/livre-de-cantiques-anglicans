\include "../common_liturgy.ly"
% https://www.reformedworship.org/article/september-2005/christmas-classics-revisited
% https://www.cpchamilton.ca/wp-content/uploads/2021/02/February-28-2021-Service.pdf

global = {
  \key d \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  fis4 fis4 e4 | d2 a'4 | d2 a4 | b4( fis4) g4 |
  a4 d,4 e4 | fis2 a4 | fis4 e2 | d2.
  d'4 d4 cis4 | b2 a4 | d4 b2 | a2 a4 |
  fis fis e4 | d2 e4 | fis2 g4 | a2. |
  fis4 fis4 e4 | d2 a'4 | d2 a4 | b4( fis4) g4 |
  a4 d,4 e4 | fis2 a4 | fis4 e2 | d2.
  \bar "|."
} 

alto = \relative c' {
  \global
  d4 d4 cis4 | cis4( b4) cis4 | d2 d4 | d2 e4 |
  e4 d4 cis4 | e4( d4) e4 | d4 d4( cis4) | a2. |
  fis'4 fis4 fis4 | g4( fis4) e4 | a4 a4( g4) | e2 e4 |
  d4 d4 cis4 | cis4( b4) d4 | d2 d4 | d4( b4 cis4) |
  d4 d4 cis4 | cis4( b4) cis4 | d2 d4 | d2 e4 |
  e4 d4 cis4 | e4( d4) e4 | d4 d4( cis4) | a2. 
}

tenor = \relative c' {
  \global
  a4 a4 a4 | fis2 fis4 | b2 a4 | g4( b4) d4 |
  cis4 a4 g4 | a2 cis4 | b4 b4( a4) | g4( e4 fis4) |
  b4 b4 cis4 | d2 cis4 | d4 d2 | d4( b4) cis4 |
  a4 a4 a4 | fis2 b4 | a2 g4 | e2( a4) |
  a4 a4 a4 | fis2 fis4 | b2 a4 | g4( b4) d4 |
  cis4 a4 g4 | a2 cis4 | b4 b4( a4) | g4 e4 fis4
}

bass = \relative c {
  \global
  d4 d4 a4 | b2 a4 | g4( g'4) fis4 | e2 b4 |
  fis4 fis'4 e4 | d2 a4 | b4 g4( a4) | d2( cis4)
  b4 b4 a4 | g4 g'2 | fis4 g2 | a2 a4 |
  d,4 d4 a4 | b2 g4 | d'4( cis4) b4 | a2. |
  d4 d4 a4 | b2 a4 | g4( g'4) fis4 | e2 b4 |
  fis4 fis'4 e4 | d2 a4 | b4 g4( a4) d2.
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Quelle est cette o -- deur a -- gré -- a -- _ ble,
  \l ber -- gers, qui ra -- vit tous nos sens?
  \l S'ex -- ha -- le -- "t-il" rien de sem -- bla -- ble
  \l au mi -- lieu des fleurs du prin -- temps?
  \l Quelle est cette o -- deur a -- gré -- a -- _ ble,
  \l ber -- gers, qui ra -- vit tous nos sens?
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Mais quelle é -- cla -- tant -- e lu -- miè -- _ re
  \l dans la nuit vient frap -- per nos yeux!
  \l L'as -- tre de jour, dans sa car -- riè -- re,
  \l fut -- il ja -- mais si ra -- di -- eux!
  \l Mais quelle é -- cla -- tan -- te lu -- miè -- _ re
  \l dans la nuit vient frap -- per nos yeux.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l À Beth -- lé -- em, dans u -- ne crè -- _ che,
  \l il vient de vous naître un Sau -- veur.
  \l Al -- lons, que rien ne vous em -- pê -- che
  \l d'a -- do -- rer vo -- tre Ré -- demp -- teur.
  \l À Beth -- lé -- em, dans u -- ne crè -- _ che,
  \l il vient de vous naître un Sau -- veur.
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Dieu "Tout-" Puis -- sant, gloire é -- ter -- nel -- _ le
  \l te soit ren -- du -- e jus -- qu'aux cieux.
  \l Que la paix soit u -- ni -- ver -- sel -- le;
  \l que la grâce a -- bonde en tout lieu.
  \l Dieu "Tout-" Puis -- sant, gloire é -- ter -- nel -- _ le
  \l te soit ren -- du -- e jus -- qu'aux cieux.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  d4 b g |
  a( g) fis |
  g2. |
  
  g4 a b |
  c8[( d] c4) b |
  a2. |
  
  d4 b g |
  d2. | \break
  
  a'4 b c |
  b4. a8 g4 |
  a b c |
  b4. a8 g4 |
  
  g b d |
  d4. e8 d4 |
  c b a |
  g2. \bar "|."
} 

alto = \relative c'' {
  \global
  g4 d d |
  e( d) d |
  d2. |
  
  d4 d g |
  fis( a) g |
  fis2. |
  
  d'4 b g |
  d2. |
  
  fis4 g a |
  g4. fis8 g4 |
  
  fis g a |
  g4. fis8 g4 |
  
  g b d |
  g,4. g8 g4 |
  a g fis |
  g2. |
}

tenor = \relative c' {
  \global
  b4 d d |
  c( b) a |
  b2. |
  
  b4 d d |
  d2 d4 |
  d2. |
  
  d4 b g |
  d2. |
  
  d'4 d d |
  d4. c8 b4 |
  d d d |
  d4. c8 b4 |
  
  g b d |
  b4. c8 b4 |
  e d c |
  b2. |
}

bass = \relative c' {
  \global
  g4 g b, |
  c4( d) d |
  g,2. |
  
  g'4 fis g | 
  a( fis) g |
  d2. |
  
  d'4 b g |
  d2. |
  
  d4 d d |
  g4. g8 g4 |
  d4 d d |
  g4. g8 g4 |
  
  g b d |
  g,4. g8 g4 |
  c, d d |
  g,2. |
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Come, thou Almighty King,
  % help us thy name to sing,
  % help us to praise.
  % Father, all glorious,
  % o'er all victorious,
  % come and reign over us,
  % Ancient of Days.
  \l Viens, ô Roi tout -puis -- sant;
  \l ac -- cep -- te no -- tre chant;
  \l viens au se -- cours!
  \l Pè -- re tout glo -- ri -- eux,
  \l sur tout vic -- to -- ri -- eux:
  \l chan -- tons, é -- lo -- gi -- eux,
  \l l'An -- cien des Jours!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Come, thou Incarnate Word,
  % gird on thy mighty sword,
  % scatter thy foes.
  % Let thine almighty aid
  % our sure defense be made,
  % our souls on thee be stayed;
  % thy wonders show.
  \l Viens, ô Verbe in -- car -- né;
  \l porte en -- fin ton é -- pée
  \l à deux tran -- chants!
  \l Que ton se -- cours puis -- sant,
  \l qui tou -- jours nous dé -- fend,
  \l pé -- né -- tre jus -- qu'au fond
  \l des cœurs croy -- ants!
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Come, Holy Comforter,
  % thy sacred witness bear
  % in this glad hour.
  % Thou who almighty art,
  % now rule in ev'ry heart,
  % and ne'er from us depart,
  % Spirit of power.
  \l Viens, ô Con -- so -- la -- teur;
  \l en -- tre dans cha -- que cœur
  \l en ce mo -- ment!
  \l Sur les eaux tu pla -- nais,
  \l ton feu nous in -- spi -- rait:
  \l ne nous quit -- te ja -- mais,
  \l Es -- prit puis -- sant!
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % To the great One in Three
  % eternal praises be,
  % hence evermore.
  % His sov'reign majesty
  % may we in glory see,
  % and to eternity
  % love and adore.
  \l A ce grand Un en Trois
  \l gloire é -- ter -- nel -- le soit,
  \l gloire à ja -- mais!
  \l Que cet -- te ma -- jes -- té
  \l soit en -- fin dé -- voi -- lée
  \l pour que la Tri -- ni -- té
  \l soit a -- do -- rée!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

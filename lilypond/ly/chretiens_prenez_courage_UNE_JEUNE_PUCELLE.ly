\include "../common_liturgy.ly"

% http://openhymnal.org/Pdf/Twas_In_The_Moon_Of_Wintertime-Huron_Carol-Jesous_Ahatonhia-Une_Jeune_Pucelle.pdf
% http://www.hymntime.com/tch/pdf/en/t/w/a/Twas%20in%20the%20Moon%20of%20Wintertime.pdf

global = {
  \key bes \major
  \time 2/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 8 d8 | g8[ a8] bes8[ c8] | bes8[ a8] g8[ f8] | g8[ g8] a8[ f8] | g4.  \bar "|" \break
  d8 | g8[ a8] bes8[ c8] | bes8[ a8] g8[ f8] | g8[ g8] a8[ f8] | g4. \bar "|" \break
  g8 | d'8[ d8] a8[ bes8] | c8.[ bes16] a4 | bes8[ a8] g4 | a8[ g8] g8[ f8] | \break
  d8[ d8] g4 | f8[ ees8] d8[ d8] |
  \time 3/4
  g4^\markup { \italic "Refrain" } f8[ d8] g4
  \time 2/4
  g8[ a8] bes8[ c8] | d8[ d,8] g4
  \bar "|."
} 

alto = \relative c' {
  \global
  bes8 | g'4 f4 | ees4 d4 | c4 bes8[ a8] | d4.
  bes8 | c4 ees4 d4 d4 | d4 c4 | bes4.
  g'8 | f4 f4 | g4 f8[ ees8] | d4. c8 | bes4 c4 |
  c8[ bes8] a4 | a4 d8[ c8] |
  bes4 d4 ees4 |
  ees4 d4 | c8 bes16[ a16] bes4
}

tenor = \relative c {
  \global
  f8 | g4 g4 | c4 d4 | ees4 d4 | bes4. f8 |
  ees4 c'4 | g4 bes4 | ees4 d4 | d4. g,8 |
  bes4 d4 | ees4 f4 | bes,4 ees4 | c4 a4 |
  bes4 c4 | d4 d4 | ees4 d4 c4 |
  c4 bes4 | a8[ d8] g,4
}

bass = \relative c {
  \global
  d8 | g,4 g4 | c4 d4 | ees4 d4 | g4. f8 |
  ees4 c4 | g4 bes4 | ees4 d4 | g4. g,8 |
  bes4 d4 | ees4 f4 | bes,4 ees4 | c4 a4 |
  bes4 c4 | d4 d4 | ees4 d4 c4 |
  c4 bes4 | a8[ d8] g,4
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Chré -- tiens, pre -- nez cou -- ra -- _ ge:
  \l Jé -- sus Sau -- veur est né!
  \l Du ma -- lin les ou -- vra -- _ ges
  \l à ja -- mais sont rui -- nés.
  \l Quand il chan -- te mer -- veil -- _ le
  \l à ces trou -- blants ap -- pas, _
  \l ne  prê -- tez plus l'o -- reil -- le:
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l O -- yez cet -- te nou -- vel -- _ le
  \l dont un ange est por -- teur!
  \l O -- yez, â -- mes fi -- dè -- _ les,
  \l et di -- la -- tez vos cœurs.
  \l La Vier -- ge dans l'é -- ta -- _ ble
  \l en -- tou -- ra de ses bras _
  \l l'En -- fant Dieu a -- do -- ra -- ble:
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Voi -- ci que trois Rois Ma -- _ ges,
  \l per -- dus en O -- ri -- ent,
  \l dé -- chif -- frent ce mes -- sa -- _ ge
  \l é -- crit au fir -- ma -- ment:
  \l L'As -- tre nou -- veau les han --  _ te;
  \l ils la sui -- vront là -bas, _
  \l cette é -- toi -- le mar -- chan -- te:
  \l Jé -- sus est né!
  \l In ex -- cel -- sis glo -- ri -- a!  
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Jé -- sus leur met en tê -- _ te
  \l que l'E -- toile en la nuit
  \l qui ja -- mais ne s'ar -- rê -- _ te
  \l les con -- dui -- ra vers lui.
  \l Dans la nuit ra -- di -- eu -- _ se,
  \l en route ils sont dé -- jà; _
  \l ils vont l'â -- me joy -- eu -- se:
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Pour l'En -- fant qui re -- po -- _ se
  \l dans un pe -- tit ber -- ceau,
  \l hum -- ble -- ment ils dé -- po -- _ sent
  \l hom -- ma -- ges et ca -- deaux.
  \l Comme eux, l'â -- me ra -- vi -- _ e,
  \l chré -- tiens, sui -- vons ses pas; _
  \l son a -- mour nous con -- vi -- e:
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


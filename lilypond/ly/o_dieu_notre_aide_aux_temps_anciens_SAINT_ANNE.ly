\include "../common_liturgy.ly"

global = {
  \key bes \major
  \time 4/2
  \partial 2*1
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 |
  d g f bes |
  bes a bes f |
  bes f g e |
  f1. \bar "|" \break a2 |
  bes2 g c a |
  bes g a f |
  g bes c a |
  bes1. \bar "|."        
}

alto = \relative c' {
  \global
  bes2 |
  bes bes bes d |
  g f f d |
  d d d c |
  c1. c2 |
  d bes ees c |
  bes g' fis f |
  ees d c c |
  d1.
}

tenor = \relative c {
  \global
  d2 |
  f g bes bes |
  c c d bes |
  bes a g g |
  a1. f2 |
  f g g a |
  d2. c4 a2 bes |
  ees, f g f |
  f1.
}

bass = \relative c {
  \global
  bes2 |
  bes ees d g |
  ees f bes, bes |
  g d' bes c | 
  < \tweak #'font-size #-2 f f,>1. f2 |
  bes, ees c f | 
  g ees d d |
  c bes ees f |
  bes,1.
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % \l O God, our help in a -- ges past,
  \l Ô Dieu, notre aide aux temps an -- ciens,
  % \l Our hope for years to come,
  \l es -- poir des temps nou -- veaux,
  % \l Our shel -- ter from the storm -- y blast,
  \l Aux jours mau -- vais, puis -- sant sou -- tien,
  % \l And our e -- ter -- nal home;
  \l notre é -- ter -- nel re -- pos.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % \l Un -- der the sha -- dow of thy throne,
  \l À l'om -- bre de ton trône, as -- sis,
  % \l Thy saints have dwelt se -- cure;
  \l tes saints ont ha -- bi -- té.
  % \l Suf -- fi -- cient is thine arm a -- lone,
  \l Ton bras vai -- lant nous garde aus -- si
  % \l And our de -- fence is sure.
  \l en tou -- te sû -- re -- té.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % \l Be -- fore the hills in or -- der stood,
  \l Quand les col -- li -- nes et les flots
  % \l Or earth re -- ceived her frame,
  \l se con -- fon -- daient mê -- lés,
  % \l From ev -- er -- last -- ing thou art God,
  \l Seul tu ré -- gnais sur le cha -- os
  % \l To end -- less years the same.
  \l de toute é -- ter -- ni -- té.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % A thousand ages in Thy sight
  \l Mille ans, Seig -- neur, sont à tes yeux
  % Are like an evening gone;
  \l plus brefs qu'un soir en -- fui,
  % Short as the watch that ends the night
  \l Plus brefs que l'au -- be dans les cieux
  % Before the rising sun.
  \l lors -- que prend fin la nuit.
}

% verseSix = \lyricmode {
%   \set stanza = "6 "
%   % Our God, our Help in ages past,
%   \l Ô Dieu notre aide aux temps an -- ciens,
%   % Our Hope for years to come,
%   \l es -- poir des temps nou -- veaux,
%   % Be Thou our Guard while life shall last,
%   \l Aux jours mau -- vais, puis -- sant sou -- tien,
%   % And our eternal Home.
%   \l Notre é -- ter -- nel re -- pos.
% }

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % Time, like an ever-rolling stream,
          "Le temps comme un torrent fougueux"
          % Bears all its sons away;
          "emporte ses enfants."
          % They fly forgotten, as a dream
          "Ils passent, comme un rêve heureux"
          % Dies at the opening day.
          "s'envoie au jour naissant."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Our God, our Help in ages past,
          "Ô Dieu notre aide aux temps anciens,"
          % Our Hope for years to come,
          "espoir des temps nouveaux,"
          % Be Thou our Guard while life shall last,
          % "Aux jours mau -- vais, puis -- sant sou -- tien,"
          "De toujours notre gardien,"
          % And our eternal Home.
          "notre éternel repos."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}


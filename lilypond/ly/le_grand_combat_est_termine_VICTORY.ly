\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  b4\rest^\markup { \italic "Antiphon" } fis4 fis |
  g2. |
  fis4 a a |
  b2. |
  a4 a d |
  cis2. | \noBreak
  d2.  \bar "|" \break
  
   a4 a a |
  b2 a4 |
  \sd a( g) \ss fis4 |
  a2. | \break
  fis4 fis fis | 
  fis2 fis4 |
  \sd fis( e) \ss d |
  e2. | \break
  a4 a a |
  b2 a4 |
  a( g) fis |
  a2. |
  b4\rest a4 d |
  cis2. |
  d2. \bar "|."
}

alto = \relative c' {
  \global
  s4 d4 d |
  d2. |
  d4 d d |
  d2. |
  d4 d fis |
  e2. |
  fis |

  fis4 fis fis |
  g2 fis4 |
  \sd fis( b,) \ss d |
  e2. |
  d4 d d |
  d2 cis4 |
  \sd d( b) \ss b |
  cis2. |
  cis4 fis fis |
  g2 fis4 |
  fis( b,) d |
  e2. |
  s4 d fis |
  e2. |
  fis |
}

tenor = \relative c' {
  \global
  s4 a a |
  b2. |
  a4 fis fis |
  g2. |
  fis4 a a |
  a2. |
  a |

  d4 d d |
  d2 d4 |
  \sd cis( e) \ss d|
  cis2. |
  a4 a a |
  b2 a4 |
  \sd a( g) \ss fis |
  a2. |
  a4 d d |
  d2 d4 |
  cis( e) d |
  cis2.
  d,4\rest d' a |
  a2. |
  a
}

bass = \relative c, {
  \global
  r4 d' d
  g,2. |
  d'4 d d |
  g,2. |
  d'4 fis d |
  a2. |
  d |

  d4 d d |
  g2 d4 |
  \sd fis( e) \ss b' |
  a2. |
  d,4 d d |
  b2 fis'4 |
  \sd d( e) \ss b |
  a2. |
  fis'4 d d |
  g2 d4 |
  fis( e) b' |
  a2. |
  s4 fis d |
  a2. |
  d
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  % \l Al -- le -- lu -- ia, al -- le -- lu -- ia, al -- le -- lu -- ia.
  \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia, al -- lé -- lu -- ia!


  \set stanza = "1 "
  % ADAPTED
  % \l The strife is o'er, the bat -- tle done;
  \l Le grand com -- bat est ter -- _ mi -- né;
  % \l Now is the Vic -- tor's tri -- umph won;
  \l Le Christ Vain -- queur a tri -- _ om -- phé;
  % \l O let the song of praise be sung:
  % \l et dans sa gloire il s'est mon -- tré;
  \l Que ses lou -- an -- ges soi -- ent chan -- tés:
  % \l Al -- le -- lu -- ia!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  _ _ _ _   _ _ _ _    _ _ _ _
  \set stanza = "2 "
  % ADAPTED
  % \l Death's migh -- tiest pow'rs have done their worst,
  \l Il est vi -- vant! La mort __ _ n'est plus!
  % \l And Je -- sus hath his foes dis -- persed;
  % \l Dieu nous dé -- li -- vre par Jé -- sus.
  \l Ses en -- ne -- mis il a __ _ bat -- tu.
  % \l Let shouts of praise and joy out -- burst:
  % \l Il rend la vie à ses é -- lus! % Whoa!
  \l Chan -- tons la gloi -- re de _ Jé -- sus!
  % \l Al -- le -- lu -- ia!
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
    _ _ _ _   _ _ _ _    _ _ _ _
  \set stanza = "3 "
  % ADAPTED
  % % \l On the third morn he rose a -- gain
  % \l Gloire au Seig -- neur res -- sus -- ci -- té!
  % % \l Glo -- rious in ma -- jes -- ty to reign;
  % \l Pre -- mier des morts il s'est le -- vé,
  % % \l O let us swell the joy -- ful strain:
  % \l Re -- splen -- dis -- sant de ma -- jes -- té,
  % % \l Al -- le -- lu -- ia!
  % \l Al -- lé -- lu -- ia!
  \l A -- près trois jours il s'est __ _ le -- vé,
  \l Re -- splen -- dis -- sant de ma -- _ jes -- té,
  \l Cri -- ez de joie et pro -- _ cla -- mez:
  \l Al -- lé -- lu -- ia!
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
    _ _ _ _   _ _ _ _    _ _ _ _
  \set stanza = "4 "
  % \l He brake the age -- bound chains of hell;
  % \l The bars from heav'n's high por -- tals fell;
  % \l Let hymns of praise his tri -- umph tell:
  % \l Al -- le -- lu -- ia!
  \l Il a vi -- dé l'in -- fer -- nal pri -- son;
  \l Pre -- mier -né de la Ré -- sur -- rec -- tion;
  \l Nous fai -- sons donc ce joy -- _ eux son:
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
    _ _ _ _   _ _ _ _    _ _ _ _
  \set stanza = "5 "
  %   \l Lord, by the stripes which wound -- ed Thee
  %   \l From death's dread sting Thy ser -- vants free,
  %   \l That we may live, and sing to Thee:
  %   \l Al -- le -- lu -- ia!
  \l Que, par tes coups de fla -- gel -- la -- tion,
  % \l on soit gué -- ri du mor -- tel ai -- guil -- lon;
  \l on se rit du mor -- tel ai -- guil -- lon;
  \l que pour tou -- jours nous chan -- _ ti -- ons:
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


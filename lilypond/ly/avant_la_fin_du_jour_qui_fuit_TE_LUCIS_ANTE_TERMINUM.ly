\include "../common_liturgy.ly"

% https://hymnary.org/hymn/HPEC1890/page/298

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4 g4 | bes4 bes4 bes4 bes4 | c4 bes4 a4 \bar "|" \break
  g4 | g4 g4 g4 a4 | a4 g4 f4 \bar "|" \break
  a4 | a4 g4 f4 d4 | f4 g4 g4 \bar "|" \break
  bes4 | bes4 bes4 bes4 g4 | a4 g4 f2
  \bar "|."
} 

alto = \relative c' {
  \global
  e4 | f4 f4 f4 f4 | f4 f4 f4
  e4 | e4 e4 e4 e4 | f4 e4 d4 
  c4 | f4 e4 d4 bes4 | c4 e4 e4 
  d4 | d4 d4 c4 e4 | f4 e4 f2
}

tenor = \relative c' {
  \global
  c4 | d4 d4 d4 d4 | c4 d4 c4
  c4 | c4 c4 c4 c4 | c4 c4 a4
  a4 | c4 c4 a4 bes4 | a4 c4 c4
  f,4 | f4 f4 e4 c'4 | c4 bes4 a2
}

bass = \relative c {
  \global
  c4 | bes4 bes4 bes4 bes4 | a4 bes4 f'4 
  c4 | c4 c4 c4 a4 | f4 c'4 d4
  f4 | f4 c4 d4 g4 | f4 c4 c4 
  bes4 | bes4 bes4 c4 c4 | f4 c4 f2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l A -- vant la fin du jour qui fuit,
  \l de ton grand a -- mour in -- fi -- ni,
  \l Cré -- a -- teur des im -- men -- ses cieux,
  \l sois no -- tre Gar -- di -- en, ô Dieu.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Chas -- se les rê -- ves dan -- ge -- reux
  \l et les fan -- tô -- mes mal -- heu -- reux;
  \l d'i -- ci ter -- ras -- se l'en -- ne -- mi;
  \l gar -- de nos cœurs purs cet -- te nuit.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l O Pè -- re clé -- ment, fait ain -- si,
  \l par Jé -- sus Christ ton Fils ché -- ri,
  \l a -- vec le Saint Es -- prit vi -- vant
  \l et rè -- gnant é -- ter -- nel -- le -- ment.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

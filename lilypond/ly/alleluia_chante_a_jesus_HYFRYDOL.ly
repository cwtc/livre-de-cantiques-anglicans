\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 g4 |
  f4.( g8) a4 |
  bes2 a4 |
  g( f) g |
  c2 bes4 |
  a2 a4 |
  g( f) g |
  f2. \bar "|"

  f2 g4 |
  f4.( g8) a4 |
  bes2 a4 |
  g( f) g |
  c2 bes4 |
  a2 a4 |
  g( f) g |
  f2. \bar "|"
  
  c'2 c4 |
  c( bes) a |
  bes2 bes4 |
  bes( a) g |
  a2 a4 |
  a( bes) c |
  c( bes) a |
  g2. \bar "|"

  c4( a) c |
  bes( g) bes |
  a( f) a |
  g8[( a] bes[ a]) g4 |
  c2 c4 |
  d( c) bes |
  a2 g4 |
  f2. \bar "|."
}

alto = \relative c' {
  \global
  c2 e4 |
  f2 f4 |
  g2 f4 |
  e( d) e |
  f( c) d8[ e] |
  f2 f8[ e] |
  d2 e4 |
  f2. |

  c2 e4 |
  f2 f4 |
  g2 f4 |
  e( d) e |
  f( c) d8[ e] |
  f2 f8[ e] |
  d2 e4 |
  f2. |

  e2 e4 |
  f2 f4 |
  f( e) d |
  e( f) c |
  c2 f4 |
  f( g) a |
  a( g) f |
  f2( e4) |
  
  f2 a4 |
  g2 g4 |
  f( c) f |
  g8[ e] f4 c |
  c( f) ees |
  d( f) f |
  f2 e4 |
  f2. \bar "|." 
}

tenor = \relative c' {
  \global
  a2 g4 |
  a4.( bes8) c4 |
  d( c) c |
  c( a) c |
  c2 c4 |
  c2 c4 |
  bes( a) c |
  a2. |

  a2 g4 |
  a4.( bes8) c4 |
  d( c) c |
  c( a) c |
  c2 c4 |
  c2 c4 |
  bes( a) c |
  a2. |

  c2 c4 |
  f,( g) a |
  g2 g4 |
  g( c) bes |
  a2 c4 |
  d2 c4 |
  d2 d4 |
  g,2. |
  c2 d4 |
  d2 c4 |
  c( a) b |
  c bes bes |
  a2 c4 |
  bes( c) d |
  c2 bes4 |
  a2. \bar "|."
}

bass = \relative c, {
  \global
  f4( a) c |
  f2 f4 |
  f( e) f |
  c( d) c |
  a'2 g4 |
  f2 a,4 |
  bes( d) c |
  f2. |

  f,4( a) c |
  f2 f4 |
  f( e) f |
  c( d) c |
  a'2 g4 |
  f2 a,4 |
  bes( d) c |
  f2. |

  a2 a4 |
  d,2 d4 |
  g2 g4 |
  c,( d) e |
  f2 e4 |
  d2 a4 |
  g2 bes4 |
  c2. |
  
  a'2 f4 |
  f( e8[ d]) e4 |
  f( e) d |
  e8[ c] d4 e |
  f2 a,4 |
  bes( a) bes |
  c2 c4 |
  f2. \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % Alleluia! sing to Jesus!
  % His the sceptre, His the throne;
  % Alleluia! His the triumph,
  % His the victory alone:
  % Hark! the songs of peaceful Sion
  % Thunder like a mighty flood;
  % Jesus out of every nation
  % Hath redeemed us by His blood.
  \l Al -- lé -- lu -- ia! \l Chante à Jé -- sus,
  \l dont le scep -- tre et le trône!
  \l Al -- lé -- lu -- ia! \l À lui seul soient
  \l la vic -- toire et la tri -- omphe!
  \l E -- cou -- tons les chants de Si -- on
  \set ignoreMelismata = ##t
  \l ton -- ner comme _ une in -- on -- da -- tion:
  \set ignoreMelismata = ##f
  \l Le Sei -- gneur de cha -- que na -- tion
  \set ignoreMelismata = ##t
  \l nous a ra -- che -- tés par son sang.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Alleluia! not as orphans
  % Are we left in sorrow now;
  % Alleluia! He is near us,
  % Faith believes, nor questions how:
  % Though the cloud from sight received Him,
  % When the forty days were o'er:
  % Shall our hearts forget His promise,
  % "I am with you evermore"?
  \l Al -- lé -- lu -- ia!
  \set ignoreMelismata = ##t
  \l Nous ne som -- mes pas
  \l com -- me des or -- phé -- lins lais -- sés.
  \set ignoreMelismata = ##f
  \l Al -- lé -- lu -- ia! \l C'est par la foi
  \l qu'on per -- çoit qu'il est tout près.
  \set ignoreMelismata = ##t
  \l A -- près qua -- ran -- te jours de Pâ -- _ ques,
  \set ignoreMelismata = ##f
  \l aux cieux il fait son re -- tour,
  \l on tient fort à sa pro -- mes -- se:
  \set ignoreMelismata = ##t
  \l Je suis a -- vec vous tous les jours.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Alleluia! Bread of Heaven,
  % Thou on earth our Food, our Stay!
  % Alleluia! here the sinful
  % Flee to thee from day to day:
  % Intercessor, Friend of sinners,
  % Earth's Redeemer, plead for me,
  % Where the songs of all the sinless
  % Sweep across the crystal sea.
  \l Al -- lé -- lu -- ia! \l Pain des an -- ges,
  \l toi, notre A -- li -- ment sur Terre.
  \l Al -- lé -- lu -- ia! \l I -- ci -bas, on
  \l fuit vers toi, nous trans -- gres -- seurs.
  \l In -- ter -- ces -- seur, proche aux pé -- cheurs,
  \set ignoreMelismata = ##t
  \l ô Ré -- demp -- _ teur, plai -- de pour moi,
  \set ignoreMelismata = ##f
  \l là où la mer cris -- ta -- li -- ne
  \l ré -- son -- ne de nom -- breux chants.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % Alleluia! King eternal,
  % Thee the Lord of lords we own;
  % Alleluia! born of Mary,
  % Earth Thy footstool, heaven Thy throne:
  % Thou within the veil hast entered,
  % Robed in flesh, our great High-Priest;
  % Thou on earth both Priest and Victim
  % In the Eucharistic feast.
  \l Al -- lé -- lu -- ia! \l Roi é -- ter -- nel,
  \l assis sur ton trô -- ne cé -- leste;
  \l Al -- lé -- lu -- ia! \l Né par -- mi nous,
  \set ignoreMelismata = ##t
  \l i -- ci, ton mar -- che -- pied ter -- restre.
  \set ignoreMelismata = ##f
  \l De la chair le Roi s'est pa -- ré,
  \l lui, le Prêtre et la Vic -- time;
  \set ignoreMelismata = ##t
  \l le _ Grand Prê -- _ tre s'of -- fre lui -mê -- _ _ _ me,
  \set ignoreMelismata = ##f
  \l pré -- sent dans l'Eu -- cha -- ri -- stie.
}

verseFive = \lyricmode {
  \set stanza = "5 "
  % Alleluia! sing to Jesus!
  % His the sceptre, His the throne;
  % Alleluia! His the triumph,
  % His the victory alone:
  % Hark! the songs of peaceful Sion
  % Thunder like a mighty flood;
  % Jesus out of every nation
  % Hath redeemed us by His blood.
  \l Al -- lé -- lu -- ia! \l Chante à Jé -- sus,
  \l dont le scep -- tre et le trône!
  \l Al -- lé -- lu -- ia! \l À lui seul soient
  \l la vic -- toire et la tri -- omphe!
  \l E -- cou -- tons les chants de Si -- on
  \set ignoreMelismata = ##t
  \l ton -- ner comme _ une in -- on -- da -- tion:
  \set ignoreMelismata = ##f
  \l Le Sei -- gneur de cha -- que na -- tion
  \set ignoreMelismata = ##t
  \l nous a ra -- che -- tés par son sang.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

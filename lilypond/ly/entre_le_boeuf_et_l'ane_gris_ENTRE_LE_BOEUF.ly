\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 2/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d4 d8[ e8] | f4 d4 | a'4 a4 | a2 |
  d2 | a2 | a8[ g8] f8[ g8] | a2 | \bar "||" \break

  d,8^\markup { \italic "Refrain" }[ e8] f8[ g8] | g2 |
  a8[ g8] f8[ e8] | d2 |
  d8[ e8] f8[ g8] | a8[ f8] g8[ a8] |
  f4 e4 | d2 \bar "|."
} 

alto = \relative c' {
  \global
  d4 d8[ e8] | f4 d4 | a'4 a4 | a2 |
  f2 | e2 | d2 | cis2 |

  d8[ e8] f8[ g8] | d2 |
  cis2 | d2 |
  cis2 | e4 d4 | d4 cis4 | a2
}

tenor = \relative c {
  \global
  d4 d8[ e8] | f4 d4 | a'4 a4 | a2 |
  a2 | a2 | a2 | e2 |

  f4 a4 | f2 | e4 g4 | f2 |
  f2 | a4 g4 | a4 g4| f2 
}

bass = \relative c {
  \global
  d4 d8[ e8] | f4 d4 | a'4 a4 | a2 |
  d,2 | c2 | bes2 | a2 |

  d2 | bes2 | a2 | bes2 |
  d2 | c4 bes4 | a4 a4 | d2
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l En -- tre le bœuf et l'â -- ne gris
  \l dort, dort, dort le pe -- tit Fils.

  \l Mille an -- ges di -- vins,
  \l mil -- le sé -- ra -- phins
  \l vo -- lent à l'en -- tour de ce grand 
  \l Dieu d'a -- mour. 
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l En -- tre les deux bras de Ma -- rie
  \l dort, dort, dort le Fruit de Vie.

}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l En -- tre les pas -- tour -- eaux jo -- lis
  \l dort, dort Jé -- sus qui sou -- rit.

}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l En ce beau jour si so -- lon -- nel
  \l dort, dort, dort l'Em -- man -- u el.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/I-N/Jesus%20Christ%20is%20Risen%20Today%20(EASTER%20HYMN).ly

global = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  c4 e g c, |
  f a \sd a( g) \ss \bar "||"
  e8[( f g c,] f4) e8[ f] |
  e4( d) c2 \bar "||" \break

  f4 g a g |
  f e \sd e( d) \ss \bar "||"
  e8[( f g c,] f4) e8[ f] |
  e4( d) c2 \bar "||" \break

  b'4 c d g, |
  c d e2 \bar "||"
  b8[( c d g,] c4) b8[ c] |
  b4( a) g2 \bar "||" \break

  g8[ a] b[ g] c4 e, |
  f a \sd a( g) \ss \bar "||"
  c8[( b c g] a[ b]) c[ d] |
  c4( b) c2 \bar "|."
  \bar "|."
} 

alto = \relative c' {
  \global
  c4 c d c |
  c f \sd f( e) \ss |
  c2~( c8[ b]) c4 |
  c( b) c2 |

  c4 c c c |
  c8[ b] c4 \sd c( b) \ss |
  c2~( c8[ b]) c4 |
  c( b) c2 |

  g'4. fis8 g4 g |
  g f! e2 |
  g2~( g8[ fis]) g4 |
  g( fis) g2 |

  b,8[ c] d[ b] c4 c |
  c f \sd f( e) \ss |
  e8[( f g e] f4) e8[ f] |
  e4( g8[ f]) e2
}

tenor = \relative c {
  \global
  e4 g g e |
  c' c c2 |
  g( a8[ f]) g[ f] |
  g4.( f8) e2 |

  f4 e f g |
  a8[ f] g4 g2 |
  g( a8[ f]) g[ f] |
  g4.( f8) e2 |

  d'4 c b b |
  c4. b8 c2 |
  d2~ d4 d8[ c] |
  d4.( c8) b2 |

  g4 g g c8[ b] |
  c4 c c2 |
  g4.( c8~ c[ d]) c[ a] |
  g2 g
}

bass = \relative c {
  \global
  c4 c b c |
  a' f c2 |
  c8[( d] e4 d) c8[ a] |
  g2 c |

  a4 c f e |
  d c g2 |
  c8[( d] e4 d) c8[ a] |
  g2 c |

  g4 a b g'8[ f!] |
  e4 d c2 |
  g'8[( a] b4 a) g8[ e] |
  d2 g, |

  g'4. f8 e4 c'8[ b] |
  a4 f c2 |
  c8[( d e c] f[ d]) e[ f] |
  g4( g,) c2
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Je -- sus Christ is ris'n to -- day, Al -- le -- lu -- ia!
  % Our tri -- um -- phant ho -- ly day; Al -- le -- lu -- ia!
  % Who did once, up -- on the cross, Al -- le -- lu -- ia!
  % Suf -- fer to re -- deem our loss. Al -- le -- lu -- ia!
  \l Jé -- sus -Christ, il rés -- sus -- ci -- te,
  _ _ _ _ _ _ _ _ _ _
  \l C'est son tri -- omphe au -- jour -- d'hui;  __ _
  _ _ _ _ _ _ _ _ _ _
  \l Sur la croix il a souf -- fert,
  _ _ _ _ _ _ _ _ _ _
  \l Pour _ ra -- _ che -- ter no -- tre per -- te.
  _ _ _ _ _ _ _ _ _ _ _
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Hymns of praise then let us sing Al -- le -- lu -- ia!
  % Un -- to Christ our heav'n -- ly King: Al -- le -- lu -- ia!
  % Who en -- dured the cross and grave, Al -- le -- lu -- ia!
  % Sin -- ners to re -- deem and save. Al -- le -- lu -- ia!
  \l Que l'on chante à no -- tre Dieu, _
  _ _ _ _ _ _ _ _ _ _
  \l À Jé -- sus, le Roi des Cieux; _
  _ _ _ _ _ _ _ _ _ _
  % \l La mort, il l'a en -- du -- rée,
  \l Pour ses pé -- cheurs a -- do -- rés,
  _ _ _ _ _ _ _ _ _ _
  % \l Pour que le _ pé -- cheur soit sauvé. _
  \l La _ mort _ il a en -- du -- ré -- e.
  _ _ _ _ _ _ _ _ _ _ _
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % But the pains which He en -- dured Al -- le -- lu -- ia!
  % Our sal -- va -- tion hath pro -- cured: Al -- le -- lu -- ia!
  % Now a -- bove the sky He's King, Al -- le -- lu -- ia!
  % Where the an -- gels ever sing. Al -- le -- lu -- ia!
  % \l Par ses souf -- frances, ob -- te -- nu, _ 
  \l Par ses plai -- es, ob -- te -- nu, __ _
  \set ignoreMelismata = ##f
  \l Al -- lé -- lu -- ia!
  \set ignoreMelismata = ##t
  \l Est le don de son sa -- lut; __ _
  \set ignoreMelismata = ##f
  \l Al -- lé -- lu -- ia!
  \set ignoreMelismata = ##t
  \l Au cieux il se -- ra as -- sis,
  \set ignoreMelismata = ##f
  \l Al -- lé -- lu -- ia!
  \set ignoreMelismata = ##t
  % \l La chan -- son cé -- les -- te s'am -- pli -- fi -- e.
  \l L'air _ cé -- _ les -- te s'am -- pli -- fi -- e.
  \set ignoreMelismata = ##f
  \l Al -- lé -- lu -- ia!
  \set ignoreMelismata = ##t
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Sing we to our God a -- bove— Al -- le -- lu -- ia!
  % Praise e -- ter -- nal as His love; Al -- le -- lu -- ia!
  % Praise Him all ye heav'n -- ly host, Al -- le -- lu -- ia!
  % Fa -- ther, Son, and Ho -- ly Ghost. Al -- le -- lu -- ia!
  \l Chan -- te, l'ar -- mé -- e cé -- les -- te,
  _ _ _ _ _ _ _ _ _ _
  \l Comme il nous aim -- e: sans ces -- se;
  _ _ _ _ _ _ _ _ _ _
  \l Chan -- tons -lui tous au -- jour -- d'hui,
  _ _ _ _ _ _ _ _ _ _
  \l Pè -- _ re, _ Fils, et Saint -Es -- prit. _
  _ _ _ _ _ _ _ _ _ _ _
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

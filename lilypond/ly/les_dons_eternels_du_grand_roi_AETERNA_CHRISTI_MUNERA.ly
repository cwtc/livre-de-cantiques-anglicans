\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/T-Z/The%20Eternal%20Gifts%20of%20Christ%20the%20King%20(ROUEN%20-%20AETERNA%20CHRISTI%20MUNERA).ly
% https://hymnary.org/tune/aeterna_christi_munera_rouen

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 
  d'4 | 
  c( b) a | 
  b( g) a | 
  b( a4.) g8 | 
  g2 \bar "|" 
  
  b4 | 
  d2 d4 | 
  b2 g4 | 
  c( b) a | 
  a2 \bar "|" \break
  
  a4 | 
  b2 g4 | 
  e2 a4 | 
  g( fis) e  | 
  d2 \bar "|" 
  
  d'4 | 
  a4( b) g | 
  c( b) a | 
  b( a4.) g8 | 
  g2
  \bar "|."
} 

alto = \relative c' {
  \global
  \global
  \partial 4 b'4 | 
  a g fis | 
  fis e fis | 
  g fis4. d8 | 
  d2

  g4 | 
  b2 b4 | 
  fis2 e4 | 
  g2 e4 |   
  fis2 
  
  fis4 | 
  fis2 e4 | 
  e2 e4 | 
  d2 c4 | 
  d2 
  
  b'4 | 
  fis4 g4 e | 
  a g4 fis4 | 
  fis4 fis4. d8 | 
  d2
}

tenor = \relative c' {
  \global
  g4 | 
  e' d d | 
  d b4 d4 | 
  d4 d4. b8 | 
  b2  

  d4 | 
  g2 g4 | 
  d2 b4 | 
  e4 d c | 
  d2  
  
  d4 | 
  d2 b4 | 
  c2 c4 | 
  b2 g4 | 
  fis2 
  
  fis'4 | 
  d2 c4 | 
  e d2 | 
  d4 d4. b8 | 
  b2 
}

bass = \relative c {
  \global
  g4 | 
  a b d | 
  b4 e4 d4 | 
  g4 d4. g8 | 
  g2  
  
  g4 | 
  g2 g4 | 
  b2 e,4 | 
  c4 g a | 
  d2 
  
  d4 | 
  b2 e4 | 
  c2 a4 | 
  b2 c4 | 
  d2  
  
  b4 | 
  d g, c | 
  a b d | 
  b4 d4. g8 | 
  g2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Les dons é -- ter -- nels du grand Roi
  \l et des saints a -- pô -- tres la gloire,
  \l en chants d'a -- mour et de vic -- toire
  \l cé -- lé -- brons du cœur a -- vec foi.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l De l'É -- glise ils sont les pre -- miers,
  \l tri -- om -- phants dans la sain -- te guer -- re;
  \l ils por -- tent par -- tout la lu -- miè -- re,
  \l cé -- les -- te troupe de guer -- ri -- ers.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Ils ont la fer -- me foi des saints
  \l et l'in -- é -- bran -- lable es -- pé -- rance;
  \l du Christ ils ont l'a -- mour im -- mense
  \l qui de Sa -- tan rompt les des -- seins.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Du Père en eux l'hon -- neur bril -- la,
  \l du Fils, la vo -- lon -- té sou -- mise;
  \l par eux se ré -- jou -- it l'É -- glise;
  \l en eux l'Es -- prit Saint tri -- om -- pha.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Nous t'en pri -- ons, ô Ré -- demp -- teur,
  \l car à toi seul notre âme as -- pire;
  \l a -- vec eux au cé -- leste em -- pire,
  \l ras -- sem -- ble -nous pour le bon -- heur.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


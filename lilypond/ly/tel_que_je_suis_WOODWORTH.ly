\include "../common_liturgy.ly"

% https://www.mutopiaproject.org/ftp/BradburyWB/woodworth/
% TRANSLATION NEEDS TO BE ADAPTED

global = {
  \key d \major
  \time 6/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody =  {
  \global
  \partial 4 d'8[ e'] 
  fis'2 fis'4 a'4.( g'8) fis'4 
  e'4.( fis'8) g'4 fis'2 \bar "|" \break 
  a'4 a'( e') fis' g'2 b'4 b'2 a'4 fis'2 \bar "|" \break
  d'8[ e'] fis'2 fis'4 a'4.( g'8) fis'4
  b'2 b'4 d''4.( cis''8) \bar "|" \break
  b'4^\markup { \italic "Refrain" } a'2 a'4 a'4.( g'8) fis'4 e'2. a' fis'1.
  \bar "|."
} 

alto =  {
  \global
  d'8[ e'] d'2 d'4 fis'4.( e'8) d'4
  cis'4.( d'8) e'4 d'2
  fis'4 e'2 d'4 e'2 g'4 g'2 fis'4 d'2
  d'8[ e'] d'2 d'4 fis'4.( e'8)
  d'4 g'2 g'4 g'2 g'4 fis'2 fis'4 fis'4.( e'8) d'4 cis'2. cis' d'1.
}

tenor =  {
  \global
  fis8[ g] a2 a4 a2 a4 a2 a4 a2 d'4 cis'2 a4 a2 cis'4 d'2 d'4 a2
  fis8[ g] a2 a4 a2 d'4 d'2 d'4 b4.( cis'8) d'4 d'2 d'4 a2 a4 a2. a a1.
}

bass =  {
  \global
  d4 d2 d4 d2 d4 a,2 a,4 d2 d4 a2 a4 a2 a,4 d2 d4 d2 d4 d2 d4 d4.( e8) fis4 g2 g4 g2 g4 d2 d4 d2 d4 a2. a, d1.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Just as I am, without one plea
  % But that Thy blood was shed for me,
  % And that Thou bidd'st me come to Thee,
  % O Lamb of God, I come, I come.

  \l Tel que je suis, sans rien à moi,
  \l si -- non ton sang ver -- sé pour moi
  \l et ta voix qui m'ap -- pelle à toi:
  \l A -- gneau de Dieu, je viens, je viens!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Just as I am, and waiting not
  % To rid my soul of one dark blot,
  % To Thee, whose blood can cleanse each spot,
  % O Lamb of God, I come!
  \l Tel que je suis, je me re -- lâche
  \l de la souil -- lu -- re qui me gâche;
  \l ton sang peut sé -- cher cha -- que tache: 
  % \l A -- gneau de Dieu, je viens, je viens!
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Just as I am, though tossed about
  % With many a conflict, many a doubt,
  % Fightings within, and fears without,
  % O Lamb of God, I come!

  \l Tel que je suis, bien va -- cil -- lant,
  \l en proie au doute à chaque in -- stant,
  \l lutte au de -- hors, crainte au de -- dans:
  % \l A -- gneau de Dieu, je viens, je viens!
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Just as I am, poor,wretched, blind,
  % Sight, riches, healing of the mind,
  % Yea, all I need, in Thee to find,
  % O Lamb of God, I come!
  \l Tel que je suis, pauvre et mau -- dit, 
  \l tu re -- sti -- tues et tu gué -- ris;
  \l cha -- que be -- soin, tu ré -- ta -- blis:
  % \l A -- gneau de Dieu, je viens, je viens!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % Just as I am--Thou wilt receive,
          % Wilt welcome, pardon, cleanse, relieve;
          % Because Thy promise I believe,
          % O Lamb of God, I come!
          "Tel que je suis, ton cœur est prêt"
          "à prendre le mien tel qu'il est,"
          "pour tout changer, Sauveur parfait!"
          "Agneau de Dieu, je viens, je viens!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Just as I am-- Thy love unknown
          % Hath broken every barrier down;
          % Now, to be Thine, yea, Thine alone,
          % O Lamb of God, I come!
          "Tel que je suis, ton grand amour"
          "a tout pardonné sans retour."
          "Je veux être à toi dès ce jour;"
          "Agneau de Dieu, je viens, je viens!"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

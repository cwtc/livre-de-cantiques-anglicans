\include "../common_liturgy.ly"

% https://www.pgdp.net/wiki/The_English_Hymnal_-_Wiki_Music_Experiment/Hymns101-150/186

global = {
  \key f \major
  \time 4/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 g a g | a c bes a  \bar "|" \break
  a g f e | d e d c  \bar "|" \break 
  g' f g a | g2. f4 f1
  \bar "|."
} 

alto = \relative c' {
  \global
  c2 e f g | f c d4( e) f2   f e d c |
  b c b c   d d e f | f2 e4( f) c1
}

tenor = \relative c {
  \global
  a'2 c c c | c a bes c | c c a g | 
  g g g4( f) e2   g a c c | c c a1
}

bass = \relative c {
  \global
  f2 c f e | f a g f   f c d e4( f) |
  g2 c, g c   bes! d c a | c c f1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  % TRADUCTION III
  \set stanza = "1 "
  \l La Mère é -- tait dou -- lou -- reu -- se
  \l à la croix âpre et pe -- neu -- se,
  \l pleu -- rant son Fils qui pen -- dait.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  % TRADUCTION III
  \set stanza = "2 "
  \l A -- donc son â -- me gé -- mis -- sant,
  \l com -- plai -- gnant et dou -- leur sen -- tant,
  \l le glai -- ve le trans -- per -- çait.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Com -- ment af -- flicte et a -- mè -- re
  \l fut cet -- te bé -- noî -- te Mè -- re
  \l du vrai Dieu, vrai jour sans nuit!
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l La -- quel -- le moult se dou -- lou -- sa
  \l et qui, vo -- yant son Fils, trem -- bla
  \l des pei -- nes du no -- ble fruit.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "Qui est-il qui ne pleurerait,"
          "s'il la Mère du Fils voyait"
          "complaindre si durement?"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "6 "
        \column {
          "Qui, sans tristesse agnoisseuse,"
          "verrait la Mère pitieuse"
          "et son Fils traités tellement?"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "7 "
        \column {
          "Pour ôter les griefs de sa gent,"
          "elle vit Jésus en tourment,"
          "sujet à chaque despit."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "8 "
        \column {
          "Elle vit son très doux Enfant"
          "laissé seul des siens et mourant"
          "quand il mis hors son esprit."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "9 "
        \column {
          "Ô Mère, fontaine d'amour,"
          "fais-moi voir ta forte douleur,"
          "avec toi tristesse faire!"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "10 "
        \column {
          "Fais mon cœur ardoir doucement"
          "en aimant Jésus fortement,"
          "pour que je lui puisse plaire."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "11 "
        \column {
          "Sainte Mère, hâtivement"
          "fixe en mon cœur profondement"
          "chaque plaie de ton Fils;"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "12 "
        \column {
          "Et que j'aie ma portion"
          "du fruit que par sa Passion"
          "par ses souffrances acquis."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "13 "
        \column {
          "Permets-moi d'avec toi pleurer"
          "à cette croix et lamenter"
          "par dévote compassion."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "14 "
        \column {
          "Et qu'avec toi, sans départir,"
          "puisse compagnie tenir"
          "par sainte méditation."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "15 "
        \column {
          "Vièrge toute nette et claire,"
          "ne sois envers moi amère."
          "Fais-moi ici lamenter,"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "16 "
        \column {
          "et porter de Jésus la mort"
          "et de sa passion, l'effort"
          "de ses peines recorder."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "17 "
        \column {
          "Fais que mon cœur soit si navré"
          "des plaies, et tout enivré"
          "de la croix de ton cher Fils;"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "18 "
        \column {
          "Et que par tel embrasement"
          "je puisse avoir au jugement"
          "défense contre le péril."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "19 "
        % TRADUCTION IV
        \column {
          "Fais que cette croix me garde,"
          "que sa mort me sauvegarde;"
          "par sa grâce il me nourrit;"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "20 "
        \column {
          % TRADUCTION IV
          "Impètre à mon âme le don"
          "de paradis, gloire et pardon"
          "quand mon corps sera pourri."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

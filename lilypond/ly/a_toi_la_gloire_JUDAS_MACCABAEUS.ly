\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \set Staff.midiInstrument = "church organ"
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  a2 fis4. g8 |
  a2 d, |
  e8[ fis] g[ a] g4 fis |
  e1 | \break
  fis8[ g] a[ b] a4 a |
  d2 a |
  g4 fis e4. d8 |
  d1 | \bar "||" \break

  fis8[ e] fis[ g] fis4 fis |
  e2 d |
  g4 fis e d |
  cis1 |
  d8[ cis] d[ e] d4 d |
  b'2 gis |
  a4 b8[ a] gis4. a8 |
  a1 \bar "||" \break

  a2^\markup { \italic "Refrain" } fis4. g8 |
  a2 d, |
  e8[ fis] g[ a] g4 fis |
  e1 | \break
  fis8[ g] a[ b] a4 a |
  d2 a |
  g4 fis e4. d8 d1 \bar "|."
}

alto = \relative c' {
  \global
  d2 d4. cis8 |
  d2 d |
  cis8[ d] e[ fis] e4 d |
  cis1 |
  d4 d d d |
  d2 d |
  b8[ cis] d4 d cis |
  a1 |
  
  d8[ cis] d[ e] d4 d |
  cis2 b |
  e4 d cis b |
  as1 |
  b8[ as] b[ cis] b4 b |
  d2 d |
  cis4 d8[ cis] d4 e |
  e1 |
  
    d2 d4. cis8 |
  d2 d |
  cis8[ d] e[ fis] e4 d |
  cis1 |
  d4 d d d |
  d2 d |
  b8[ cis] d4 d cis |
  a1 |
}

tenor = \relative c {
  \global
  fis2 a4. g8 |
  fis2 a |
  g4 e a a |
  a1 |
  a4 a8[ g] a4 a |
  a2 fis |
  g4 a a g |
  fis1 |
  
  a4 a a a |
  g2 fis |
  as4 b g fis |
  fis1 |
  fis4 fis fis b |
  b2 b |
  a4 fis b b |
  cis1 |
  
  fis,2 a4. g8 |
  fis2 a |
  g4 e a a |
  a1 |
  a4 a8[ g] a4 a |
  a2 fis |
  g4 a a g |
  fis1 | 
}

bass = \relative c {
  \global
  d2 fis4. e8 |
  d2 fis |
  e4. d8 cis4 d |
  a1 |
  d8[ e] fis[ g] fis4 fis |
  fis2 d |
  e4 fis8[ g] a4 a, |
  d1 |
  
  d4 d d d |
  a2 b |
  cis4 d e b |
  fis'1 |
  b,4 b b a' |
  gis2 e |
  fis4 d e e |
  a,1 |
  
  d2 fis4. e8 |
  d2 fis |
  e4. d8 cis4 d |
  a1 |
  d8[ e] fis[ g] fis4 fis |
  fis2 d |
  e4 fis8[ g] a4 a, |
  d1 |
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % \l Thine be the glo -- ry, ris -- en, con -- quering Son,
  \l À toi la gloi -- re, Ô Res -- sus -- ci -- té!
  % \l End -- less is the vic -- t'ry Thou o'er death has won;
  \l À toi la vic -- toi -- re pour l'é -- ter -- ni -- té!
  % \l An -- gels in bright rai -- ment rolled the stone a -- way,
  \l Bril -- lant de lu -- miè -- re, l'ange est de -- scen -- du;
  % \l Kept the fold -- ed grave -- clothes where Thy bo -- dy lay.
  \l Il roule la pi -- er -- re du tom -- beau vain -- cu.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % \l Lo! Je -- sus meets us, ris -- en from the tomb;
  \l Vois -le par -- aî -- tre: c'est lui, c'est Jé -- sus,
  % \l Lov -- ing -- ly he greets us, scat -- ters fear and gloom;
  \l Ton Sau -- veur, ton Maî -- tre, oh! Ne dou -- te plus!
  % \l Let the Church with glad -- ness hymns of tri -- umph sing,
  \l Sois dans l'al -- lé -- gres -- se, peu -- ple du Seig -- neur,
  % \l For her Lord now liv -- eth, death hath lost its sting;
  \l Et re -- dis sans ces -- se: le Christ est vain -- queur!
  
  % \l Thine be the glo -- ry, ris -- en, con -- quering Son,
  \l À toi la gloi -- re, Ô Res -- sus -- ci -- té!
  % \l End -- less is the vic -- t'ry Thou o'er death has won;
  \l À toi la vic -- toi -- re pour l'é -- ter -- ni -- té!
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % \l No more we doubt Thee, glo -- rious Prince of Life;
  \l Crain -- drais "-je en" -- co -- re? Il vit à ja -- mais,
  % \l Life is nought with -- out Thee; aid us in our strife;
  \l Ce -- lui que j'a -- do -- re, le Prin -- ce de paix;
  % \l Make us more than con -- querors through Thy death -- less love;
  \l Il est ma vic -- toi -- re, mon puis -- sant sou -- tien,
  % \l Bring us safe through Jor -- dan to Thy home a -- bove.
  \l Ma vie et ma gloi -- re: non, je ne crains rien!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

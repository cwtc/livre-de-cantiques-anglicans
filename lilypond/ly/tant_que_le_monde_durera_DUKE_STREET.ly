\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d2 fis4 g |
  a2 b4( cis) |
  d2 cis4( b) |
  a1 \bar "|" \break

  a2 a4 a |
  b2 a |
  g fis |
  e1 \bar "|" \break

  fis2 fis4 e |
  d( fis) a( d) |
  b( a) g( fis) |
  e1 \bar "|" \break

  a2 b4 cis |
  d2. g,4 |
  fis2 e |
  d1 \bar "|."
}

alto = \relative c' {
  \global
  d2 d4 cis |
  d2 d4( e) |
  fis2 e2 |
  cis1 |

  d2 d4 d |
  d2 d4( a) |
  b( cis) d2 |
  cis1 |

  d2 d4 cis |
  a( d) d2 |
  d cis4( d) |
  cis1 |

  d2 d4 e |
  fis4.( e8 d4) e |
  d2 cis |
  s1
}

tenor = \relative c {
  \global
  fis2 a4 a |
  a2 g |
  fis4( a) a( gis) |
  a1 |

  fis2 fis4 a |
  g2 a4( fis) |
  g( a) a2 |
  a1 |

  a2 a4 g |
  fis( a) a2 |
  b4( d) a2 |
  a1 |

  a2 g4 g |
  fis4.( g8 a4) b |
  a2 a4( g) |
  fis1
}

bass = \relative c {
  \global
  d2 d4 e |
  fis2 g4( e) |
  d2 e |
  a,1 |

  d2 d4 fis |
  g2 fis |
  e d |
  a1 |

  d2 d4 a |
  d2 fis |
  g4( fis) e( d) |
  a1 |

  fis'2 g4 e |
  d4.( e8 fis4) g |
  a2 a, |
  d1
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % \l Je -- sus shall reign wher -- e'er the sun
  \l Tant que le mon -- de du -- re -- ra,
  % \l Does his suc -- ces -- sive jour -- neys run;
  \l Christ no -- tre Maî -- tre ré -- gne -- ra;
  % \l His king -- dom stretch from shore to shore,
  \l Tant que les as -- tres glo -- ri -- eux
  % \l Till moons shall wax and wane no more.
  \l Ray -- on -- ne -- ront du haut des cieux.
}

% verseTwo = \lyricmode {
%   \set stanza = "2 " 
%   \l For Him shall end -- less prayer be made,
%   \l And prais -- es throng to crown His head;
%   \l His Name, like sweet per -- fume, shall rise
%   \l With ev' -- ry morn -- ing sac -- ri -- fice.
% }

verseThree = \lyricmode {
  \set stanza = "3 "
  % \l Peo -- ple and realms of ev' -- ry tongue
  \l Les peu -- ples chan -- te -- ront tou -- jours
  % \l Dwell on His love with sweet -- est song;
  \l les grands bien -- faits de son a -- mour;
  % \l And in -- fant voic -- es shall pro -- claim
  \l Les voix d'en -- fants pro -- cla -- me -- ront
  % \l Their ear -- ly bles -- sings on His Name.
  \l la ma -- jes -- té de son saint Nom.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % \l Bles -- sings a -- bound wher -- e'er He reigns;
  \l Il a sou -- mis le monde en -- tier,
  % \l The pris' -- ner leaps to lose his chains,
  \l Rom -- pu les liens des pri -- son -- niers,
  % \l The wea -- ry find e -- ter -- nal rest,
  \l Don -- né la paix aux mal -- heur -- eux,
  % \l And all the sons of want are blest.
  \l et bé -- ni tous les mi -- sé -- reux.
}


verseFive = \lyricmode {
  \set stanza = "5 "
  % \l Let ev' -- ry crea -- ture rise and bring
  \l Frè -- res, le -- vons -nous a -- vec -- foi
  % \l Pe -- cu -- liar ho -- nors to our King,
  \l pour rendre hom -- mage à no -- tre Roi;
  % \l An -- gels des -- cend with songs a -- gain,
  \l Les an -- ges chan -- tent dans les cieux
  % \l And earth re -- peat the loud A -- men.
  \l Glo -- ri -- fi -- ant le seul vrai Dieu.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    % \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

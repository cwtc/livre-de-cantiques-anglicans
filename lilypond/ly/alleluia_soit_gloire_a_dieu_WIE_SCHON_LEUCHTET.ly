\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \set Staff.midiInstrument = "church organ"
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
 \partial 4
  d4 |
  a' fis d a' |
  b b a a |
  b cis d cis | \break
  b b a fis |
  b a g fis |
  e2 d4 \bar "||" \break

  d4 |
  a' fis d a' |
  b b a a |
  b cis d cis | \break
  b b a fis |
  b a g fis |
  e2 d2 \bar "||" \break

  a'2 fis |
  a fis |
  fis4 fis e e |
  fis fis e fis |
  g fis e2 |
  d d'4 cis |
  b a g fis |
  e2 d2 \bar "|."
}

alto = \relative c' {
  \global
  \partial 4
  a4 |
  a a b cis |
  fis e e fis |
  g g fis e8[ a] |
  a4 gis e d |
  d d d8[ cis] d4 |
  d( cis) a

  a4 |
  a a b cis |
  fis e e fis |
  g g fis e8[ a] |
  a4 gis e d |
  d d d8[ cis] d4 |
  d( cis) a2

  e'2 d |
  e d |
  d4 d cis cis |
  d d cis dis |
  e d!8[ cis] b4( cis) |
  a2 fis'4 fis8[ e] |
  d8[ e] fis4 fis8[ e] e[ d] |
  d4( cis) a2
}

tenor = \relative c {
  \global
  \partial 4
  fis4 |
  e d8[ e] fis[ gis] a4 |
  a gis cis d |
  d e a,8[ b] cis4 |
  fis,8[ fis'] e[ d] cis4 a |
  g a b8[ g] a4 |
  a4.( g8) fis4

  fis4 |
  e d8[ e] fis[ gis] a4 |
  a gis cis d |
  d e a,8[ b] cis4 |
  fis,8[ fis'] e[ d] cis4 a |
  g a b8[ g] a4 |
  a4.( g8) fis2

  a2 a |
  cis a |
  a4 a a a |
  a a a a |
  b8[ cis] d4 d8[( cis16 b] a8[ g]) |
  fis2 fis8[ gis] as4 |
  b cis d8[ e] a,4 |
  b( a8[ g]) fis2
}

bass = \relative c {
  \global
   \partial 4
  d4 |
  cis d b fis'8[ e] |
  d4 e a, d |
  g! fis8[ e] fis[ gis] a4 |
  d, e a, d |
  g fis e d |
  a2 d4

  d4 |
  cis d b fis'8[ e] |
  d4 e a, d |
  g! fis8[ e] fis[ gis] a4 |
  d, e a, d |
  g fis e d |
  a2 d2

  cis2 d |
  a d |
  d8[ e] fis[ g] a[ b] a[ g] |
  fis[ e] fis[ d] a[ a'] g[ fis] |
  e4 b'8[ a] g4( a) |
  d,2 b4 fis |
  g a b8[ cis] d4 |
  g,4( a) d2 %<< {\stemDown \ignore  \override Stem #'length = #4.0  d4} \\ {\teeny \override Stem #'length = #5.6 d,4 } >>
}

verseOne = \lyricmode {
  \set stanza = "1 "
  \l How bright ap -- pears the Morn -- ing Star,
  \l With mer -- cy beam -- ing from a -- far;

  \l The host of heav'n re -- joi -- ces;
  \l O Righ -- teous Branch, O Jes -- se's Rod!
  \l Thou Son of man and Son of God!
  \l We, too, will lift our voi -- ces:

  \l Je -- sus, Je -- sus!
  \l Ho -- ly, ho -- ly, yet most low -- ly,
  \l Draw Thou near us;
  \l Great Em -- man -- uel, come and hear us.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  \l Though cir -- cled by the hosts on high,
  \l He deigned to cast a pit -- ying eye
  \l Up -- on His help -- less crea -- ture;
  \l The whole cre -- a -- tion's Head and Lord,
  \l By high -- est se -- ra -- phim a -- dored,
  \l As -- sumed our ve -- ry na -- ture;
  \l Je -- sus, grant us,
  \l Through Thy me -- rit, to in -- he -- rit
  \l Thy sal -- va -- tion;
  \l Hear, O hear our sup -- pli -- ca -- tion.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  \l Re -- joice, ye heav'ns; thou earth, re -- ply;
  \l With praise, ye sin -- ners, fill the sky,
  \l For this His in -- car -- na -- tion.
  \l In -- car -- nate God, put forth Thy pow'r,
  \l Ride on, ride on, great Con -- quer -- or,
  \l Till all know Thy sal -- va -- tion.
  \l A -- men, A -- men!
  \l Hal -- le -- lu -- jah! Hal -- le -- lu -- ia!
  \l Praise be gi -- ven
  \l Ev -- er -- more, by earth and hea -- ven.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

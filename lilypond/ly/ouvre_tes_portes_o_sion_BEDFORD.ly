\include "../common_liturgy.ly"

% https://hymnary.org/hymn/HPEC1940/page/155

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4
  a | fis4 d4 b'4 a4 | g4 fis4 e4 a4 |
  d4 cis4 b4 b4 | a2. \bar "|" \break
  fis4 | g4 a4 b4 a4 | g4 fis4 e4 a4 |
  d4 fis,4 g4 e4 | d1
  \bar "|."
} 

alto = \relative c' {
  \global
  d4 | d4 d4 d4 d4 | d8[ cis8] d4 cis4
  cis4 | d4 e4 fis4 e8[ d8] | cis2.
  d4 | d4 d4 d4 cis4 | d4 d4 cis4 
  cis4 | d4 d4 d4 cis4 | s1
}

tenor = \relative c {
  \global
  fis4 | a4 a4 g4 fis4 | g4 a4 a4
  a4 | gis4 a4 a4 gis4 | a2.
  a4 | g4 fis4 g4 e4 | g4 fis8[ g8] a4 
  a4 | a4 b4 b4 a8[ g8] | fis1 
}

bass = \relative c {
  \global
  d4 | d4 fis4 g4 d4 | e4 d4 a4 a4 |
  b4 cis4 d4 e4 | a,2.
  d4 | b4 d4 g,4 a4 | b4 d4 a4 a4
  fis4 b4 g4 a4 | d1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Ou -- vre tes por -- tes, ô Si -- on:
  \l L'om -- bre va dis -- pa -- raître;
  \l voi -- ci, mer -- veil -- leuse u -- ni -- on!
  \l La vic -- time et le prêtre.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Plus d'ho -- lo -- causte à con -- su -- mer:
  \l Par son grand sa -- cri -- fice,
  \l le Fils de Dieu vient dés -- ar -- mer
  \l la cé -- les -- te jus -- tice. 
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l L'hum -- ble Vier -- ge, dans cet en -- fant,
  \l voit la Dé -- i -- té même;
  \l et deux co -- lom -- bes pour pré -- sent
  \l elle offre au Dieu su -- prême.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l A Si -- mé -- on pa -- raît en -- fin
  \l l'ob -- jet de son at -- tente;
  \l An -- na peut voir l'en -- fant di -- vin,
  \l et va mou -- rir con -- tente.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "La sainte Mère, à deux genoux,"
          "en silence l'adore,"
          "ce Verbe, petit comme nous,"
          "ne parlant pas encore."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          "À toi gloire, ô Père éternel;"
          "gloire à toi, Fils du Père,"
          "à toi, Saint-Esprit, gloire au ciel,"
          "gloire aussi sur la terre."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

\include "../common_liturgy.ly"

% TO CONSIDER:
% http://www.hymntime.com/tch/non/fr/p/v/b/e/pvbethlm.htm
% http://www.hymntime.com/tch/non/fr/p/e/v/i/pevibeth.htm

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4 b4 |
  b b ais b |
  \slurDotted d( c) e, a |
  \slurSolid g fis8[ g] a4 d, |
  b'2. \bar"|"\break b4 |
  
  b b e d |
  d c e, a |
  g fis8[ g] b4. a8 |
  g2. \bar"|"\break b4 |
  
  b b a g |
  fis2 fis4 fis |
  e fis g a |
  b2. \bar"|"\break b4 |
  
  b b ais b |
  d c e, e' |
  d g, b4. a8 |
  \partial 4*3 g2. \bar "|."
}

alto = \relative c' {
  \global
  d4 |
  d d cis d |
  \slurDotted f( e) c e |
  d d d d |
  d2. d4 |
  
  d g gis gis |
  a e c e |
  d d fis fis |
  g2. g4 |
  
  g g fis e |
  dis2 dis4 dis |
  e fis g e |
  fis2. g4 |
  
  d d cis d |
  \slurSolid e e c e8[ fis] |
  g4 cis, d4 c |
  b2. \bar "|."
}

tenor = \relative c' {
  \global
  g4 |
  g g g g |
  \slurDotted gis( a) a c |
  \slurSolid b a8[ b] c4 c |
  b2. g4 |
  
  g b b e |
  e e e c |
  b a8[ b] d4 c |
  b2. d4 |
  
  d b c cis |
  dis2 dis4 b |
  e, fis g e' |
  dis2. d4 |
  
  d b g g |
  gis a a a |
  b a8[ g] g4 fis |
  g2. \bar "|."
}

bass = \relative c' {
  \global
  g4 |
  g g g g |
  \slurDotted c,( c) c c |
  d d d d |
  g,2. g'4 |
  
  g f e e |
  a a a, c |
  d4. d8 d4 d |
  g2. g4 |
  
  g g a ais | 
  b2 b4 b, |
  e fis g c |
  b2. g4 |
  
  g g g g |
  c, c c c |
  d e d4 d |
  g,2. \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1 "
   \set ignoreMelismata = ##t
  % \l O lit -- tle town of Beth -- le -- hem,
  \l Pe -- ti -- te vil -- le Beth -- lé -- em,
  % \l How still we _ see thee lie!
  \l tu te re -- po -- ses en paix!
  %% TL: que tu nous parais tranquille! (2)
  % \l tu dors tran -- _ quil -- le -- ment;
  % \l A -- bove thy deep and dream -- less sleep
  \l Là quand tu dors, l'é -- toi -- le d'or
  %% TL: Tu dors la nuit dans l'infini
  % \l Sur ton som -- meil, l'é -- toi -- le d'or 
  % \l The si -- lent _ \set associatedVoice = "altos" stars go by; \unset associatedVoice
  \l en si -- len -- _ ce sur -- "viel - le."
  %% TL: où les étoiles défilent. (2)
  % \l se lève au _ fir -- ma -- ment.
  % \l Yet in thy dark streets shin -- eth
  %% TL: Caché dans tes rues sombres
  \l Sa lu -- mière é -- ter -- nel -- le
  % \l The ev -- er -- last -- ing Light;
  \l é -- clai -- re le ciel noir;
  %% TL: l'éternelle lumière, (2)
  % \l nous ap -- por -- te la joie;
  % \l The hopes and fears of all the years
  \l Nos peurs, nos es -- poirs sont, en toi,
  %% TL: Espoir des sages de tous les âges,
  % \l Oui, la ré -- ponse à nos ap -- pels
  % \l Are met in \set associatedVoice = "altos" thee to -- night.
  \l bien ex -- au -- cés ce soir.
  %% TL: le Christ vient sur la terre. (2)
  % \l ce soir se trouve en toi.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  \set ignoreMelismata = ##t
  % \l For Christ is born of Ma -- _ ry,
  \l Car Jé -- sus de Ma -- rie est né,
  % \l And gath -- er’d _ all a -- bove,
  \l et tan -- dis _ que tout dort,
  % \l While mor -- tals sleep, the an -- gels keep
  \l Les an -- ges sur -- veillent, as -- sem -- blés,
  % \l les an -- ges là -- haut as -- sem -- blés
  % \l Their watch of _ \set associatedVoice = "altos" won -- d’ring love. \unset associatedVoice
  \l chan -- tent leurs _ doux ac -- cords.
  % \l O morn -- ning stars, to -- geth -- er
  \l Pro -- cla -- mez sa nai -- san -- ce,
  % \l Pro -- claim the ho -- ly birth!
  \l é -- toi -- les du ma -- tin!
  % \l And prais -- es sing to God the King,
  \l Chan -- tez sa gloi -- re, c'est le Roi;
  % \l Ap -- por -- tant la paix et l'a -- mour,
  % \l And peace to \set associatedVoice = "altos" men on earth!
  \l voi -- ci l'En -- fant di -- vin.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  \set ignoreMelismata = ##t
  % \l How si -- lent -- ly, how si -- lent -- ly
  %% TL: Le Messie vient nous rejoindre
  \l Ah! Qu'il fut don -- né sim -- ple -- ment
  % \l The won -- drous _ gift is giv’n!
  %% TL: sans fanfare et sans bruit.
  \l ce pré -- sent _ mer -- veil -- leux!
  % \l So God im -- parts to hu -- man hearts
  %% TL: Dieu, par son don, donne son pardon
  \l Et Dieu bé -- nit l'homme ain -- _ si:
  % \l Ain -- si Dieu bé -- nit les hu -- mains
  % \l The bless -- ings _ \set associatedVoice = "altos" of His Heav’n. \unset associatedVoice
  %% À ceux qui ont foi en lui.
  \l Il les ouv -- _ re les cieux.
  % \l en leur ouv -- _ rant les cieux.
  % \l No ear may hear His com -- ing,
  \l Nul n'en -- tend sa ve -- nu -- e
  % \l But in this world of sin;
  \l dans ce mon -- de pé -- cheur.
  % \l Where meek souls will re -- ceive Him still,
  \l Mais, bien -- ve -- nu, en -- tre Jé -- sus,
  % \l Mais Jé -- sus en -- tre, bien -- ve -- nu,
  % \l The dear Christ \set associatedVoice = "altos" en -- ters in.
  \l dans les plus hum -- bles cœurs.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  \set ignoreMelismata = ##t
  %% AC
  % \l Where chil -- dren pure and hap -- py
  \l Où les en -- fants heu -- reux et purs
  % \l pray to the bless -- ed Child,
  \l se tour -- nent à l'En -- fant Saint,
  % \l Where mis -- e -- ry cries out to Thee,
  \l Où la mi -- sè -- re voit la Mère
  % \l Son of the \set associatedVoice = "altos" mo -- ther mild; \unset associatedVoice
  \l a -- vec son _ Fils di -- vin,
  % \l Where cha -- ri -- ty stands watch -- ing
  \l Où l'on voit la cha -- ri -- té,
  % \l and faith holds wide the door,
  \l on sait que Dieu est là.
  % \l The dark night wakes, the glo -- ry breaks,
  \l Comme au ci -- el, fê -- tons No -- ël
  % \l and Christ -- \set associatedVoice = "altos" mas comes once more.
  \l en -- co -- re un -- e fois.
}

verseFive = \lyricmode {
  \set stanza = "5 "
  \set ignoreMelismata = ##t
  % TL, ad. AC:
  % O Holy Child of Bethlehem,
  \l Cher En -- fant Saint de Beth -- lé -- em,
  % Descend to us, we pray;
  \l Des -- cends nous _ voir ce jour;
  % Cast out our sin, and enter in,
  %% TL: Tu brises les chaînes qui nous entrâinent
  \l Verbe in -- car -- né, ô viens en -- trer,
  % Be born in us today.
  %% TL: vers la mort aux alentours.
  \l Sau -- ve -nous _ de la mort.
  % We hear the Christmas angels
  \l Nous en -- ten -- dons les an -- ges
  % The great glad tidings tell;
  \l qui chan -- tent pour No -- ël;
  % O come to us, abide with us,
  % Groupons nos voix, trouvons la joie
  \l Ô viens vers nous, reste a -- vec nous,
  % Our Lord Emmanuel!
  % avec Emmanuel.
  \l Sei -- gneur Em -- man -- u -- el!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 a4 f |
  c'2. f,4 |
  bes4. bes8 bes4 a |
  g2. \bar "" \break

   g4 |
   e f d c |
   f a g g |
   f1 \bar "|."
} 

alto = \relative c' {
  \global
  c2 f4 c |
  c2. f4 |
  f4. f8 g4 f |
  e2.

  d4 |
  c c b a |
  d f f e |
  s1
}

tenor = \relative c' {
  \global
  a2 c4 a |
  g2. a4 |
  bes4. bes8 ees4 c |
  c2.
  
  bes4 |
  g f f f |
  a c c4. bes8 |
  a1
}

bass = \relative c {
  \global
  f2 f4 f |
 f( e2) f4 |
 d4. d8 ees4 f |
 c2. 
 
 g4 |
 c a bes f' |
 d a c c |
 f1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Drop, drop, slow tears, and bathe those beauteous feet,
  % which brought from heaven the news and Prince of Peace.
  \l Gout -- tez, mes larmes, et la -- vez ces beaux pieds
  \l ap -- por -- tant les nou -- vel -- les de la paix.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Cease not, wet eyes, his mercies to entreat;
  % to cry for vengeance sin doth never cease.
  \l Cher -- chez, mes yeux, à ja -- mais sa clé -- mence;
  \l le pé -- ché cher -- che tou -- jours la ven -- geance.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % In your deep floods drown all my faults and fears;
  % nor let his eye see sin, but through my tears.
  \l In -- on -- dez donc mes peurs et mes dé -- fauts,
  \l qu'il voi -- e le pé -- ché par mes san -- glots.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/A-C/Crown%20him%20with%20many%20crowns%20(DIADEMATA).ly
% https://topmusic.topchretien.com/chant/a-lagneau-sur-son-trone-2/


global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
    % Lyrics.LyricText.font-size = #-1
  }
}

melody = \relative c' {
  \global
  d4 d8 d fis4 fis |
  b2. b4 |
  a d, g fis |
  e2. \bar "|"  \break
  
  e4 |
  fis a b a |
  gis fis8[ e] a4 d |
  cis d b b |
  a2. \bar "|" 
  
  a4 |
  a fis e d |
  b'2. b4 |
  b gis fis e |
  cis'2. \bar "|" \break
  
  cis4 |
  d4. cis8 b4 a |
  g e fis a |
  g fis e e |
  d1 \bar "|."
} 

alto = \relative c' {
  \global
  a4 a8 a d4 d |
  d2. d4 
  d d cis d |
  cis2. 
  
  cis4 |
  d e d d |
  b b e d |
  e fis b, e |
  cis2.
  
  cis4 |
  d d cis d |
  d2. dis4 |
  e e d d |
  cis2. 
  
  e4 |
  d d d d |
  d cis d d |
  d d d cis |
  s1
}

tenor = \relative c {
  \global
  fis4 fis8 fis fis4 fis |
  g2. g4 |
  a b g a |
  a2. 
  
  a4 |
  a a fis fis |
  gis gis a gis |
  a a a gis |
  a2.
  
  a4 |
  a a g a |
  g2. a4 |
  gis b a b |
  a2. 
  
  a4 |
  a a g a |
  b a a a |
  b a a4. g8 |
  fis1
}

bass = \relative c {
  \global
  d4 d8 d8 b4 b |
  g2. g'4 |
  fis g e d |
  a2. 
  
  a4 |
  d cis b d |
  e d cis b |
  a d e e |
  a,2. 
  
  g'4 |
  fis d e fis |
  g2. fis4 |
  e e fis gis |
  a2. 
  
  g4 |
  fis fis g fis | 
  e a d, fis, |
  g d' a a |
  d1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Crown Him with many crowns,
  % The Lamb upon His throne;
  % Hark! how the heavenly anthem drowns
  % All music but its own:
  % Awake, my soul, and sing
  % Of Him Who died for thee,
  % And hail Him as thy matchless King
  % Thro' all eternity.
  \l À l'A -- gneau sur son trône,
  \l ap -- por -- tons la cou -- ronne!
  \l Il l'a con -- qui -- se sur la croix;
  \l il est le Roi des rois!
  \l E -- veil -- le toi, mon âme!
  \l Bé -- nis, a -- dore, ac -- clame,
  \l a -- vec tous les an -- ges du ciel,
  \l Jé -- sus, Em -- man -- u -- el!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Crown Him the Son of God
  % Before the worldis began,
  % And ye, who tread where he hath trod,
  % Crown Him the Son of Man;
  % Who every grief hath known
  % That wringis the human breast,
  % And takes and bears them for His own,
  % That all in Him may rest.
  \l À l'A -- gneau sur son trône,
  \l l'en -- cens et la cou -- ronne!
  \l Car il est le Verbe in -- car -- né;
  \l d'u -- ne vierge, il est né!
  \l Ô Sa -- ges -- se pro -- fonde!
  \l Le Cré -- a -- teur du monde,
  \l pour vain -- cre le mal tri -- om -- phant
  \l s'est fait pe -- tit en -- fant!
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Crown Him the Lord of Life,
  % Who triumphed o'er the grave,
  % And rose victorious in the strife
  % For those He came to save;
  % His glories now we sing
  % Who died, and rose on high,
  % Who died, eternal life to bring,
  % And lives that death may die.
 \l Il eut la croix pour trône,
 \l l'é -- pin -- e pour cou -- ronne.
 \l Mais le Père a glo -- ri -- fi -- é
 \l son Fils cru -- ci -- fi -- é!
 \l Au Prin -- ce de la Vie,
 \l la mort est as -- ser -- vie!
 \l Hors de la tombe il est mon -- té:
 \l Christ est res -- su -- ci -- té!
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Crown Him of lordis the Lord,
  % Who over all doth reign,
  % Who once on earth, the Incarnate Word,
  % For ransomed sinners slain,
  % Now lives in realms of light,
  % Where saints and angels sing
  % Their songis before Him day and night,
  % Their God, Redeemer, King.
  \l À l'A -- gneau sur son trône,
  \l la palme et la cou -- ronne!
  \l Car il est le Prin -- ce de Paix;
  \l il rè -- gne dé -- sor -- mais!
  \l Les fu -- reurs de la guerre
  \l s'é -- tein -- dront sur la terre,
  \l où re -- naî -- tront, com -- me ja -- dis,
  \l les fleurs du Pa -- ra -- dis!
}

verseFive = \lyricmode {
    % Crown Him the Lord of heaven,
  \set stanza = "5 "
  % Enthroned in worldis above;
  % Crown Him the King, to whom is given,
  % The wondrous name of Love.
  % Crown Him with many crowns,
  % As thrones before Him fall,
  % Crown Him, ye kingis, with many crowns,
  % For He is King of all.
  % ADAPT?
  \l À l'A -- gneau tous les trônes,
  \l et tou -- tes les cou -- ronnes!
  \l Il est le Maî -- tre sou -- ve -- rain;
  \l les temps sont dans sa main!
  \l Ren -- dons l'hon -- neur su -- prême
  \l à ce Dieu qui nous aime,
  \l et qui re -- vient vic -- to -- ri -- eux
  \l pour nous ouv -- rir les cieux!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseOne
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseTwo
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseThree
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseFour
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

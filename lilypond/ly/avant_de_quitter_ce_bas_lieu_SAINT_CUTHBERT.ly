\include "../common_liturgy.ly"

% https://hymnary.org/hymn/HoLC1910/page/105

global = {
  \key ees \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 
  ees4 | ees4. ees8 f4 g4 | aes4. aes8 g4 
  bes4 | ees4 g,4 g4 a4 | bes2. \bar "|" \break
  aes4 | g4. g8 aes4 bes4 | c4 c4 bes4
  g4 | bes2. aes4 | g1
  \bar "|."
} 

alto = \relative c' {
  \global
  bes4 | bes4. bes8 bes4 ees4 | ees4. ees8 ees4 
  ees8[ d8] | ees4 ees4 ees4 ees4 | d2. \bar "|" \break
  d4 | bes4. ees8 ees4 des4 | c4 d4 ees4
  ees4 | ees2 d2 | ees1
  \bar "|."
}

tenor = \relative c' {
  \global
  g4 | g4. g8 aes4 bes4 | c4 c4 bes4 
  g8[ aes8] | bes4 bes4 bes4 c4 | bes2. \bar "|" \break
  f4 | g4. bes8 c4 g4 | aes4 aes4 bes4
  bes4 | g2 f4( bes4) | bes1
  \bar "|."
}

bass = \relative c {
  \global
  ees4 | ees4. ees8 ees4 ees4 | aes,4 aes4 ees'4 
  ees8[ f8] | g4 ees4 c4 f4 | bes,2. \bar "|" \break
  bes4 | ees4. ees8 ees4 ees4 | aes4 f4 g4
  ees4 | bes2 bes2 | ees1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l A -- vant de quit -- ter ce bas lieu,
  \l le bien -ai -- mé Sau -- veur
  \l nous an -- non -- ça l'Es -- prit de Dieu
  \l Con -- so -- la -- teur.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Il vient é -- ta -- blir son sé -- jour
  \l comme a -- mi ferme et sûr,
  \l ap -- por -- tant la paix et l'a -- mour
  \l dans un cœur pur.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Dans le se -- cret sa dou -- ce voix,
  \l comme un souf -- fle du soir
  \l cal -- me le pé -- cheur aux a -- bois,
  \l par -- lant d'es -- poir.
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Tou -- te ver -- tu, tout saint dé -- sir,
  \l qui dans notre âme à lui;
  \l dou -- ceur, force, in -- no -- cent plai -- sir,
  \l tout vient de lui.
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Es -- prit de grâce et pu -- re -- té,
  \l re -- tiens -nous sous ta loi;
  \l fais de nos cœurs u -- ne ci -- té
  \l di -- gne de toi.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

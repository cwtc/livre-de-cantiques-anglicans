\include "../common_liturgy.ly"

% https://hymnary.org/hymn/CHRC1920/page/425

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4
  a4 | bes4 a4 a4 g4 | f2.
  c'4 | d4 a4 g4 f4 | g2. \bar "|" \break
  g4 | a4 c4 c4 d4 | c2.
  g4 | a4 bes4 a4. g8 | f1
  \bar "|."
} 

alto = \relative c' {
  \global
  f4 | f4 f4 e4 e4 | f2.
  f4 | f4 c4 d4 f4 | e2.
  e4 | f4 f4 g4 f4 | e2.
  g4 | f4 f4 f4 e4 | s1
}

tenor = \relative c' {
  \global
  c4 | d4 c4 c4 bes4 | a2.
  c4 | bes4 a4 bes4 a4 | c2.
  c4 | c4 c4 c4 b4 | c2.
  c4 | c4 bes4 c4. bes8 | a1
}

bass = \relative c {
  \global
  f4 | f4 f4 c4 c4 | d2.
  a4 | bes4 f'4 bes,4 d4 | c2.
  c4 | f4 a4 e4 g4 | c,2.
  e4 | f4 d4 c4 c4 | f1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Nous ai -- mons le saint lieu
  \l tout plein de ta mé -- moire;
  \l nous y chan -- tons ta gloire,
  \l pleins de joie, ô grand Dieu.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l I -- ci tes ser -- vi -- teurs,
  \l u -- nis dans la pri -- ère,
  \l de -- vant toi, ten -- dre Père,
  \l vien -- nent ou -- vrir leur cœur.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Voi -- ci les fonts sa -- crés
  \l où l'Es -- prit Saint lui -même,
  \l à Dieu, dans le bap -- tême,
  \l nous a tous con -- sa -- crés.
  
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Nous ai -- mons ton au -- tel,
  \l où dans le saint my -- stère,
  \l aux en -- fants de la terre
  \l s'of -- fre le pain du ciel.
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Ic -- i nous en -- ten -- dons
  \l tes di -- vi -- nes pa -- roles;
  \l par el -- les tu con -- soles;
  \l tu nous of -- fres tes dons.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "6 "
        \column {
          "Quel bonheur quand nos voix"
          "sous ces voûtes bénies"
          "t'offrent leurs symphonies,"
          "ô Père, ô Roi des rois!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "7 "
        \column {
          "Accorde-nous, Seigneur,"
          "du saint amour les flammes,"
          "avant-goût pour nos âmes,"
          "du céleste bonheur."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      % \line { \bold "8 "
      %   \column {
      %     % O that with yonder sacred throng,
      %     % We at his feet may fall;
      %     % We'll join the everlasting song,
      %     % And crown him Lord of all.
      %     "Nous voyons les archanges se"
      %     "prosternant devant toi!"
      %     "Nous nous unissons avec eux;"
      %     "Nous te couronnons Roi!"
      %   }
      % }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
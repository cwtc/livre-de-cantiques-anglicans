\include "../common_liturgy.ly"

global = {
  \key ees \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g2 f2 |
  ees4 f g bes |
  aes2 g2 |
  bes g |
  aes4 g8[ f] ees4 g |
  f2 ees2 |
  
  g2 f2 |
  ees4 f g bes |
  aes2 g2 |
  bes g |
  aes4 g8[ f] ees4 g |
  f2 ees2 | \bar "||"

  \override Staff.TimeSignature #'stencil = ##f   
  \time 8/4 bes'2 c4 
  %\time 5/4 
  ees2 ees4 d bes |
  \time 4/4 c2 bes |
  \time 8/4 bes c4 
  %\time 5/4 
  ees2 ees4 d bes | 
  \time 4/4 c2 bes |
  
  \time 4/4 f2 g2 |
  aes4 aes g f |
  f( e) f2 |
  g bes |
  aes4 g f ees |
  f2 ees \bar "|."
}

alto = \relative c' {
  \global
  ees2. d4 |
  c f ees ees |
  ees( d) ees2 |
  d ees |
  ees4 d c ees |
  ees( d) ees2 |

  ees2. d4 |
  c f ees ees |
  ees( d) ees2 |
  d ees |
  ees4 d c ees |
  ees( d) ees2 |

  ees2 aes4 bes2 g4 
  aes4 bes bes( aes) g2 
  ees2 aes4 g2 g4 
  f d f4.( ees8) d2 
  
  d e |
  f4 f des aes8[ bes] |
  c2 c |
  c bes |
  c4 bes c ees |
  ees4( d) ees2 |
}

tenor = \relative c' {
  \global
  g4( aes) bes2 |
  g4 bes bes bes |
  f2 g |
  g g |
  c4 bes8[ aes] g4 bes |
  bes2 g |

  g4( aes) bes2 |
  g4 bes bes bes |
  f2 g |
  g g |
  c4 bes8[ aes] g4 bes |
  bes2 g |

  \override Staff.TimeSignature #'stencil = ##f   
  bes2 ees4 ees2 bes4 
  d ees ees2 ees 
  ees c4 c2 g4 
  bes4 bes bes( a) bes2 
  
  d c4( bes) 
  aes c bes f | 
  g2 aes2 |
  ees ees |
  ees4 g aes g |
  bes2 g |
}

bass = \relative c {
  \global
  ees2 bes |
  c4 d ees g, |
  bes2 ees |
  g, c |
  f,4 bes c g |
  bes2 ees |
  
  ees2 bes |
  c4 d ees g, |
  bes2 ees |
  g, c |
  f,4 bes c g |
  bes2 ees |

  g2 aes4 g2 ees4 
  f g aes2 ees2 
  g2 f4 c2 c4 
  d g f2 bes,2 

  bes' aes4( g) |
  f f bes, des |
  c2 f, |
  c' g |
  c4 ees aes, c |
  bes2 ees |
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % \l Jé -- sus, Sau -- veur a -- do -- ra -- ble,
  % \l tu m'in -- vi -- tes à ta ta -- ble;
  % \l Plein d'a -- mour, plein de ten -- dres -- se,
  % \l Tu veux cal -- mer ma dé -- tres -- se.
  % \l Pour me re -- le -- ver, toi -mê -- me,
  % \l di -- vin Fils du Dieu su -- prê -- me,
  % \l Tu m'of -- fres, dans ta clé -- men -- ce,
  % \l Le par -- don de mon of -- fen -- se.

  % Deck thyself, my soul, with gladness,
  % leave the gloomy haunts of sadness;
  % come into the daylight's splendour,
  % there with joy thy praises render
  % unto him whose grace unbounded
  % hath this wondrous banquet founded:
  % high o'er all the heavens he reigneth,
  % yet to dwell with thee he deigneth.
  \l Or -- ne -- toi, â -- me, de bon -- heur;
  \l quitte en -- fin ces en -- droits som -- bres;
  \l En -- tre dans le jour ra -- di -- eux,
  \l pour le mag -- ni -- fi -- er, joy -- eux:
  \l Mag -- ni -- fie -- le dont la bon -- té
  \l a ce fam -- eux ban -- quet fon -- dé!
  \l Des cieux il a l'au -- tor -- i -- té,
  \l mais il daigne a -- vec toi gî -- ter.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % Now I sink before thee lowly,
  \l À ge -- noux en ré -- vé -- re -- nce,
  % filled with joy most deep and holy,
  \l rem -- pli d'u -- ne joie pro -- fon -- de,
  % as with trembling awe and wonder
  \l Je ré -- flé -- chis à ta grand -- eur;
  % on thy mighty works I ponder:
  \l au fond de mon cœur je trem -- ble.
  % how, by mystery surrounded,
  \l Quel -- les di -- vi -- nes mys -- tè -- res
  % depth no mortal ever sounded,
  \l dans ce re -- pas qu'on ré -- vè -- re!
  % none may dare to pierce unbidden
  \l Ton in -- vi -- ta -- tion à per -- cer
  % secrets that with thee are hidden.
  \l ces se -- crets vers nous est ver -- sée.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % % \l Sun, who all my life dost bright -- en,
  % \l À tes pieds, Sau -- veur fi -- dè -- le,
  % % \l Light, who dost my soul en -- light -- en,
  % \l en ce jour je re -- nou -- vel -- le.
  % % \l Joy, the sweet -- est man e'er know -- eth,
  % \l L'al -- li -- an -- ce qui m'as -- su -- re
  % % \l Fount, whence all my be -- ing flow -- eth,
  % \l la fé -- li -- ci -- té fu -- tu -- re.
  % % \l At Thy feet I cry, my Ma -- ker,
  % \l De cet -- te fa -- veur in -- si -- gne,
  % % \l Let me be a fit par -- ta -- ker
  % \l Hé -- las! je ne suis pas di -- gne.
  % % \l Of this bles -- sed food from hea -- ven,
  % \l Dai -- gne, se -- lon ta pro -- mes -- se,
  % % \l For our good, Thy glo -- ry, giv -- en.
  % \l sub -- ve -- nir à ma fai -- bles -- se.
  \l Lu -- mière é -- clair -- ant ma vi -- e,
  \l So -- leil qui mon âme a -- vi -- ve,
  \l Joie dou -- ce sans quoi j'é -- crou -- le,
  \l Sour -- ce dont mon ê -- tre cou -- le:
  \l Mon Cré -- a -- teur, je sup -- pli -- e,
  \l ô, que tu me fas -- ses dig -- ne
  \l De man -- ger ce pain très sa -- cré
  \l que tu don -- nes de ta bon -- té.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % % \l Je -- sus, Bread of Life, I pray Thee,
  % \l Seig -- neur, que ma ré -- pen -- tan -- ce
  % % \l Let me glad -- ly here o -- bey Thee,
  % \l trou -- ve grâce en ta pré -- sen -- ce!
  % % \l Nev -- er to my hurt in -- vit -- ed,
  % \l Fais que ce re -- pas de vi -- e
  % % \l Be Thy love with love re -- quit -- ed;
  % \l dans la foi me for -- ti -- fi -- e.
  % % \l From this ban -- quet let me mea -- sure,
  % \l Dans mon â -- me dès cette heu -- re,
  % % \l Lord, how vast and deep its trea --  -- sure;
  % \l Ah! viens fai -- re ta de -- meu -- re:
  % % \l Through the gifts Thou here dost give me
  % \l Dé -- sor -- mais el -- le n'as -- pi -- re
  % % \l As Thy guest in heav'n re -- ceive me.
  % \l Qu'à vi -- vre sous ton em -- pi -- re.
  \l Jé -- sus, Pain de Vi -- e, fais que, 
  \l a -- vec joie, je t'o -- bé -- is -- se;
  \l Tê -- tu, je ni -- e ton a -- mour;
  \l fais que je t'en donne en re -- tour!
  \l Je di -- scer -- ne de ce ban -- quet
  \l son trés -- or pro -- fond dé -- mon -- tré;
  \l Par ces dons que tu m'y of -- fres,
  \l je suis ton in -- vi -- té aux cieux.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

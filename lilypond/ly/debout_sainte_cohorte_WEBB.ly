\include "../common_liturgy.ly"
% https://cantiques.yapper.fr/CV/CV_204a.html
% https://hymnary.org/hymn/WS1916/page/181

global = {
  \key bes \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 f4 | bes4. bes8 d4 bes4 | bes2 g4 bes4 |
  f4 bes4 c4 d4 | c2. \bar "|" \break
  f,4 | bes4. bes8 d4 bes4 | bes2 g4 bes4 |
  f4 bes4 d4 c4 | bes2. \bar "|" \break
  f4 | c'4. c8 bes4 c4 | d2 d4 d4 | ees4 d4 g,4 c4 | bes2( a4) \bar "|"
  f4 | bes4. bes8 d4 bes4 | bes2 g4 bes4 |
  f4 bes4 d4 c4 | bes1
  \bar "|."
} 

alto = \relative c' {
  \global
  d4 | d4. d8 f4 f4 | g2 ees4 g4 |
  f4 f4 f4 f4 | f2.
  f4 | d4. d8 f4 f4 | g2 ees4
  g4 | f4 d4 f4 ees4 | d2. 
  f4 | f4. f8 f4 f4 | f2 f4 f4 |
  ees4 f4 g4 g4 | f2.
  f4 | d4. d8 f4 f4 | g2 ees4
  g4 | f4 d4 f4 ees4 | d1 
}

tenor = \relative c' {
  \global
  bes4 | bes4. bes8 bes4 bes4 | bes2 bes4 bes4 |
  bes4 bes4 a4 bes4 | a2.
  a4 | bes4. bes8 bes4 bes4 | bes2 bes4 bes4 |
  bes4 bes4 bes4 a4 | bes2.
  f4 | a4. a8 g4 a4 | bes2 bes4 bes4 |
  bes4 bes4 bes4 ees4 | d2( c4)
  a4 | bes4. bes8 bes4 bes4 | bes2 bes4 bes4 |
  bes4 bes4 bes4 a4 | bes1
}

bass = \relative c {
  \global
  bes4 | bes4. bes8 bes4 d4 | ees2 ees4 ees4 |
  d4 d4 c4 bes4 | f'2.
  f4 | bes,4. bes8 bes4 d4 | ees2 ees4 ees4 |
  d4 bes4 f4 f4 | bes2.
  f'4 | f4. f8 f4 f4 | bes2 bes4 bes8[ a8] |
  g4 f4 ees4 c4 | f2.
  f4 | bes,4. bes8 bes4 d4 | ees2 ees4 ees4 |
  d4 bes4 f4 f4 | bes1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Stand up! stand up for Jesus,
  % Ye soldiers of the cross;
  % Lift high his royal banner,
  % It must not suffer loss:
  % From victory unto victory
  % His army shall be led,
  % Till every foe is vanquished,
  % And Christ is Lord indeed.
  \l De -- bout, sain -- te co -- hor -- te,
  \l sol -- dats du Roi des rois!
  \l Te -- nez d'u -- ne main for -- te
  \l l'é -- ten -- dard de la croix!
  \l Au sen -- tier de la gloi -- re
  \l Jé -- sus -Christ nous con -- duit;
  \l de vic -- toire en vic -- toi -- re
  \l il mè -- ne qui le suit.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Stand up! stand up for Jesus!
  % The trumpet-call obey;
  % Forth to the mighty conflict,
  % In this his glorious day:
  % Ye that are men, now serve him,
  % Against unnumbered foes;
  % Your courage rise with danger,
  % And strength to strength oppose.
  \l La trom -- pet -- te ré -- son -- ne:
  \l De -- bout, vail -- lants sol -- dats!
  \l L'im -- mor -- tel -- le cour -- on -- ne
  \l est le prix des com -- bats.
  \l Si l'en -- ne -- mi fait ra -- ge,
  \l so -- yez fer -- mes et forts;
  \l re -- dou -- blez de cou -- ra -- ge
  \l s'il re -- dou -- ble d'ef -- forts.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Stand up! stand up for Jesus!
  % Stand in his strength alone:
  % The arm of flesh will fail you--
  % Ye dare not trust your own:
  % Put on the gospel armor,
  % And watching unto prayer,
  % Where duty calls, or danger,
  % Be never wanting there.
  \l De -- bout pour la ba -- tail -- le!
  \l Par -- tez, n'hé -- si -- tez plus!
  \l Si vo -- tre bas dé -- fail -- le,
  \l re -- gar -- dez à Jé -- sus!
  \l De l'ar -- mure in -- vin -- ci -- ble,
  \l sol -- dats, re -- vê -- tez vous!
  \l Le tri -- omphe est pos -- si -- ble
  \l pour qui lutte à ge -- noux.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Stand up! stand up for Jesus!
  % The strife will not be long;
  % This day the noise of battle,
  % The next the victor's song:
  % To him that overcometh,
  % A crown of life shall be;
  % He with the King of Glory
  % Shall reign eternally!
  \l De -- bout, de -- bout en -- co -- re!
  \l Lut -- tez jus -- qu'au ma -- tin;
  \l dé -- jà bril -- le l'au -- ro -- re
  \l à l'ho -- ri -- zon loin -- tain.
  \l Bien -- tôt je -- tant nos ar -- mes
  \l au pieds du Roi des rois!
  \l Les chants a -- près les lar -- mes,
  \l le trône a -- près la croix.
  }


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


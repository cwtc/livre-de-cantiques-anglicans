% https://github.com/bbloomf/lilypond-songs/blob/master/ly/Nearer%2C%20My%20God%2C%20to%20Thee.ly
% http://www.hymntime.com/tch/non/fr/m/d/p/l/mdpluspr.htm
\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 2 g'2 |
  g4 fis e d |
  g2 a |
  b \bar"" b |
  b4 b a g |
  c2 b |
  a \bar""  \bar"|" \break g |
  a4 b a g |
  e2 fis |
  g \bar"" d' |
  b4 g a c |
  b2 a |
  g \bar"|."
}

alto = \relative c' {
  \global
  d2 |
  d4 d b b |
  b2 d |
  d d |
  d4 g fis g |
  g2 g |
  fis

  g |
  fis4 g fis d |
  e2 d |
  d d |

  d4 g fis e8[ fis] |
  g2 fis g \bar"|."
}

tenor = \relative c' {
  \global
  b2 |
  b4 a g fis |
  e2 fis g

  g |
  b4 d d b |
  e2 d
  d

  b |
  d4 d d d |
  c( b) a2 |
  b
  b |
  g4 b d e |
  d2 d4( c) b2 \bar"|."
}

bass =  \relative c' {
  \global
  g2 |
  g4 d e b |
  e2 d |
  g,

  g' |
  g4 g d e |
  c2 g |
  d'

  e |
  d4 g d b |
  c2 d |
  g,

  g' |
  g4 e d a |
  b( c) d2 |
  g, \bar"|."
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % All people that on earth do dwell,
  % Sing to the Lord with cheerful voice.
  % Him serve with mirth, his praise forth tell,
  % Come ye before him and rejoice.
  \l Vous, qui sur la terre ha -- bi -- tez,
  \l Chan -- tez à hau -- te voix, chan -- tez;
  \l Ré -- jou -- is -- sez -- vous au Seig -- neur,
  \l Par un saint hymne à son hon -- neur.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % Know that the Lord is God indeed;
  % Without our aid he did us make:
  % We are his flock, he doth us feed,
  % And for his sheep he doth us take.
  \l Sa -- chez qu'il est le Sou -- ver -- ain,
  \l Qu'il nous a for -- més de sa main,
  \l Nous, le peu -- ple qu'il veut ché -- rir,
  \l Et le trou -- peau qu'il veut nour -- rir.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % O enter then his gates with praise,
  % Approach with joy his courts unto:
  % Praise, laud, and bless his name always,
  % For it is seemly so to do.
  \l En -- trez dans son temple au -- jour -- d'hui;
  \l Pros -- ter -- nez -- vous tous de -- vant Lui;
  \l Et, de con -- cert a -- vec les cieux,
  \l Cé -- lé -- brez son Nom glo -- ri -- eux.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % For why? the Lord our God is good,
  % His mercy is for ever sure;
  % His truth at all times firmly stood,
  % And shall from age to age endure.
  \l C'est un Dieu rem -- pli de bon -- té,
  \l D'une é -- ter -- nel -- le vé -- ri -- té,
  \l Tou -- jours pro -- pice à nos sou -- haits,
  \l Et sa grâ -- ce dure à ja -- mais.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

% \markup { \vspace #0.5 } 

% \markup {
%   \fill-line {
%     \hspace #0.1 % moves the column off the left margin;
%      % can be removed if space on the page is tight
%      \column {
%       \line { \bold "5 "
%         \column {
%           % Or if on joyful wing
%           % Cleaving the sky,
%           % Sun, moon, and stars forgot,
%           % Upward I fly,
%           % Still all my song shall be,--
%           % Nearer, my God, to Thee,--
%           % Nearer, my God, to Thee,
%           % Nearer to Thee.
%         }
%       }
%     }
%     \hspace #0.1 % adds horizontal spacing between columns;
%   \hspace #0.1 % gives some extra space on the right margin;
%   % can be removed if page space is tight
%   }
% }

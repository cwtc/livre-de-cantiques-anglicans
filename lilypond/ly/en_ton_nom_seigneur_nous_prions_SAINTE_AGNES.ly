\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/I-N/Jesus,%20The%20Very%20Thought%20of%20Thee%20(ST.%20AGNES).ly

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  b4 b b |
  a2 b4 |
  c2 fis,4 |
  g2. \bar "|"

  d4 d d |
  b'2 a4 |
  a2. \bar "|"

  c4 c b |
  a2 g4 |
  fis2 e4 |
  d2. \bar "|"

  d4 e g |
  b2 a4 |
  g2. \bar "|."
} 

alto = \relative c' {
  \global
  d4 d d |
  e2 d4 |
  c( e) d |
  d2. |

  d4 d d |
  d2 cis4 |
  d2. |

  e4 e d |
  c2 cis4 |
  d2 cis4 |
  d2. |

  d4 d c |
  b2 c4 |
  b2.
}

tenor = \relative c' {
  \global
  b4 b g |
  c2 g4 |
  a2 a4 |
  b2. |

  c4 b a |
  g2 g4 |
  fis2. |

  e4 fis gis |
  a2 a4 |
  a2 g4 |
  fis2. |

  g4 g g |
  g2 fis4 |
  g2.
}

bass = \relative c' {
  \global
  g4 g b, |
  c2 b4 |
  a2 d4 |
  g2. |

  a4 g fis |
  g2 e4 |
  d2. \bar "|"

  a4 a a |
  a2 a4 |
  d2 d4 |
  d2( c!4) |

  b4 c e |
  d2 d4 |
  g,2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l En ton nom, Sei -- gneur, nous pri -- ons;
  \l tu nous pro -- mis d'en -- tendre.
  \l À toi se -- mece et les mois -- sons,
  \l le prin -- temps et dé -- cembre.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Lor -- sque le vent d'hi -- ver souf -- fla,
  \l tu fus notre es -- pé -- rance;
  \l nous sen -- tons, quand a -- vril est là,
  \l ta sa -- ge pro -- vi -- dence.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Par la pluie au temps dé -- si -- ré,
  \l l'air pur et la lu -- mière,
  \l le vert é -- pi, le grain do -- ré,
  \l tu bé -- nis la pri -- ère.
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Aus -- si de toi vien -- nent en nous
  \l l'in -- vi -- si -- ble crois -- sance,
  \l le sage ef -- froi, l'a -- mour si doux,
  \l la cal -- mante es -- pé -- rance.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Fais, Sei -- gneur, que tous les bien -- faits
  \l que no -- tre cœur ap -- pelle
  \l nous en jou -- is -- sons à ja -- mais
  \l dans la Ter -- re nou -- velle.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


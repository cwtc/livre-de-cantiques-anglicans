\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  b4 a g |
  g( fis) g |
  a( e) fis |
  g2. |

  g4 fis e |
  d2 g4 |
  g( fis) g |
  a2. |

  b4 a g |
  g( fis) g |
  a( e) fis |
  g2. |

  g4 fis e |
  d2 b'4 |
  a b a |
  g2. \bar "||" \break

  c4^\markup { \italic "Refrain" } c c |
  b2 b4 | 
  a2 a4 |
  b2. |

  b4 a g |
  e2 g4 | 
  a2 a4 |
  g2. \bar "|."
} 

alto = \relative c' {
  \global
  d4 d d |
  d2 d4 |
  e2 d4 |
  d2. |

  e4 d c |
  b2 d4 |
  cis2 cis4 |
  d2. |

  d4 d d |
  d2 d4 |
  e2 d4 |
  b2. |

  e4 d c |
  b2 d4 |
  e2 c4 |
  b2. |

  g'4 g g |
  g2 g4 |
  g( e) fis4 |
  g2. |

  b,4 c d |
  c2 b4 |
  g'4( e) fis4 |
  d2.
}

tenor = \relative c' {
  \global
  d4 c b |
  b( c) b |
  c2 a4 |
  b2. |

  g4 g g |
  b2 b4 |
  a2 g4 |
  fis2. |

  g4 c b |
  b( a) g |
  c2 a4 |
  g2. |

  g4 g g |
  g2 g4 |
  g2 fis4 |
  g2. |

  e'4 c e |
  d2 d4 |
  d2 d4 |
  d2. |
  
  g,4 g g | 
  g( a) g  |
  d'2 c4 |
  b2.
}

bass = \relative c' {
  \global
  g4 g g |
  g( a) g |
  c,2 d4 |
  g,2. |

  c4 c e |
  g2 g4 |
  e2 e4 |
  d2. |

  g4 g g |
  d( c) b |
  a2 d4 |
  e2. |

  c4 c c |
  g2 b4 |
  a2 d4 |
  g,2. |

  c4 e c |
  g'2 g4 |
  d2 d4 |
  g,2.|

  g4 a b |
  c2 e4 |
  d2 d4 |
  g,2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Faith of our fathers, living still
  % In spite of dungeon, fire, and sword,
  % Oh, how our hearts beat high with joy
  % Whene'er we hear that glorious word!
  \l La foi de nos aï -- eux vi -- vante
  \l mal -- gré le feu et le ca -- chot:
  \l Nos cœurs bat -- tons joy -- eu -- se -- ment
  \l quand on en -- tend ce glo -- ri -- eux mot:

  % Faith of our fathers, holy faith!
  % We will be true to thee till death.
  \l La foi de nos aï -- eux vi -- vante!
  \l Jus -- qu'à la mort nous la sui -- vons.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Our fathers, chain'd in prisons dark,
  % Were still in heart and conscience free;
  % And blest would be their children's fate,
  % If they, like them, should die for thee:
  \l Nos aï -- eux, mal -- gré leurs fers forts,
  \l é -- tai -- ent li -- bres dans leurs cœurs;
  \l bé -- nis se -- ront donc leurs en -- fants
  \l s'ils, com -- me leurs pa -- rents, pour toi meurent:
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Faith of our fathers, we will love
  % Both friend and foe in all our strife,
  % And preach thee, too, as love knows how
  % By kindly words and virtuous life: 
  \l La foi de nos aï -- eux s'é -- tend 
  \l aux en -- ne -- mis comme aux a -- mis;
  \l mal -- gré nos sou -- fran -- ces pré -- sentes,
  \l prê -- chons dans les fru -- its de l'Es -- prit.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\include "../common_liturgy.ly"
% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/A-C/Bread%20of%20the%20World%20in%20Mercy%20Broken%20(RENDEZ%20%C3%80%20DIEU).ly
% https://www.hymnologyarchive.com/rendez-dieu

global = {
  \key g \major
  % \time 4/4
  \cadenzaOn
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g2 e4 d \bar "|"
  g g a c \bar "|"
  b2 a \bar "|"

  d2 b4 b \bar "|"
  g4 c b2 a \bar "|"
  g1 \bar "|"
  \break

  g2 e4 d \bar "|"
  g4 g a c \bar "|"
  b2 a \bar "|"

  d2 c4 b \bar "|"
  a g g fis \bar "|"
  g1 \bar "|"
  \break

  d'2 d4 c \bar "|"
  b2 a g4 fis \bar "|"
  e2 d \bar "|"

  d2 g4 g fis e \bar "|"
  g2 a \bar "|"
  b1 \bar "|"
  \break

  g2 g4 a \bar "|"
  b g c c \bar "|"
  b2 a \bar "|"

  d2 c4 b \bar "|"
  a g g fis \bar "|"
  g1 \bar "|."
} 

alto = \relative c' {
  \global
  b4( d) c a |
  e' d e e |
  g2 fis |

  fis2 fis4 fis |
  g a g( e) fis2 |
  s1 |

  b,2 c4 a |
  d e fis g |
  g2 fis |

  g2 fis4 g |
  e e d d |
  d1 |

  g2 g4 fis |
  d( e) fis2 e4 d |
  d( cis) d2 |

  a2 d4 e d b |
  e2. fis4 |
  gis1 |

  e2 e4 d |
  d e e4.( fis8) |
  gis2 e4( fis) |

  g2 g4 g |
  e e d d |
  d1
}

tenor = \relative c' {
  \global
  g2 g4 fis |
  b g c c |
  d( e) a,2 |

  b2 b4 b |
  b e d2. c4 |
  b1 |

  g2 g4 fis |
  g b d e |
  d2 d |
  b2 c4 d |
  c b a a |
  b1 |

  b2 b4 c |
  g2 d' b4 a |
  b( a) fis2 |

  fis2 g4 c a g |
  g2 c |
  b1 |

  b2 b4 a |
  g b e e |
  e( d) c2 |

  d2 e4 d |
  c b a a |
  b1
}

bass = \relative c {
  \global
  g4( b) c d |
  e b c a |
  b( c) d2 |

  b2 d4 d |
  e a, b( c) d2 |
  g,1 |

  e'4( d) c2 |
  b4 e d c |
  g( b) d2 |
  g,2 a4 b |
  c c d d |
  g,1 |

  g2 g4 a |
  b( c) d2 e4 fis |
  g( a) d,2 |

  d2 b4 c d e |
  c( b) a2 |
  e'1 |

  e2 e4 fis |
  g e c4. d8 |
  e2 a,2 |

  b2 c4 g |
  a8[ b] c4 d d |
  g,1 |
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Bread of the world, in mercy broken,
  % Wine of the soul, in mercy shed,
  % By Whom the words of life were spoken,
  % And in Whose death our sins are dead;
  \l Dans l'a -- mour, il rom -- pit ce Pain, __ _
  \l don -- nant les pa -- ro -- les de vie.
  \l Dans l'a -- mour, il ver -- sa ce Vin, __ _
  \l et nos pé -- chés meurent a -- vec lui.

  % Look on the heart by sorrow broken,
  % Look on the tears by sinners shed;
  % And be Thy feast to us the token
  % That by Thy grace our souls are fed.
  % \l Re -- gar -- de cha -- que cœur bri -- sé, __ _
  % \l cha -- que lar -- me de nous pé -- cheurs;
  % \l que ta fê -- te soit à ja -- mais __ _
  % \l 
  \l Re -- gar -- de les lar -- mes ver -- sé -- es;
  \l re -- gar -- de nos fra -- gi -- les cœurs;
  \l que ce re -- pas soit à ja -- mais __ _
  \l la pro -- vi -- sion pour nous pé -- cheurs.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


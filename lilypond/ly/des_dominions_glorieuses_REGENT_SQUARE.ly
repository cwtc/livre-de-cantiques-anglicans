\include "../common_liturgy.ly"

global = {
  \key bes \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f4 d bes' f |
  d'4. c8 bes4 f |
  g g f bes |
  f ees d2 \bar "||"

  f4 d bes' f |
  d'4. c8 bes4 a |
  bes a g a8[ bes] |
  a4 g f2 \bar "||"

  c'4.^\markup { \italic "Refrain" } c8 a4 f |
  d'4. c8 bes4 g |
  ees' d c bes |
  bes a bes2 \bar "|."
} 

alto = \relative c' {
  \global
  d4 bes f' d |
  f4. f8 f4 f |
  bes, bes bes bes |
  c a bes2 |

  d4 bes f' f8[ ees] |
  d4. ees8 d4 d |
  d d d d |
  f e f2 |

  f4. f8 f4 c |
  d4. d8 ees4 ees |
  g f ees d8[ ees] |
  f4. ees8 d2 \bar "|."
}

tenor = \relative c' {
  \global
  bes4 f f bes |
  bes4. a8 bes4 bes |
  g bes f g |
  f f f2 |

  bes4 bes f f |
  bes4. a8 g4 fis |
  g fis d' c8[ bes] |
  c4 bes a2 |

  a4. a8 c4 a |
  bes4. aes8 g4 bes |
  c f, g8[ a] bes4 |
  c c bes2 \bar "|."
}

bass = \relative c {
  \global
  bes4 bes d bes |
  f'4. ees8 d4 d |
  ees ees d g, |
  a f bes2 |

  bes'4 f d d8[ c] |
  bes4. c8 d4 d |
  g d bes g |
  c c f,2 |

  f'4. f8 f4 f |
  bes,4. bes8 ees4 ees |
  c d ees8[ f] g4 |
  f f, bes2 \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Angels, from the realms of glory,
  % wing your flight o’er all the earth.
  % As you sang creation’s story,
  % now proclaim Messiah’s birth;
  % come and worship, come and worship,
  % worship Christ, the newborn King.
  \l Des do -- mi -- nions glo -- ri -- eu -- ses, 
  \l vous les an -- ges, ap -- pro -- chez;
  \l a -- vec vos chan -- sons pi -- eu -- ses
  \l sa nai -- san -- ce pro -- cla -- mez;
  \l Ve -- nez lou -- er, ve -- nez lou -- er,
  \l lou -- ez le Roi nou -- veau -né!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Shepherds, in the field abiding,
  % watching o’er your flocks by night,
  % God with us is now residing,
  % yonder shines the infant light;
  % come and worship, come and worship,
  % worship Christ, the newborn King.
  \l Dans vos champs à mi -- nu -- it, _
  \l vous les ber -- gers, en -- ten -- dez:
  \l u -- ne Lu -- mi -- è -- re luit; _
  \l l'En -- fant di -- vin i -- ci naît; 
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Sages, leave your contemplations—
  % brighter visions beam aesar.
  % Seek the great desire of nations,
  % guided by his natal star;
  % come and worship, come and worship,
  % worship Christ, the newborn King.
  \l Vos pro -- fon -- des con -- tem -- pla -- tions,
  \l vous les sa -- ges, quit -- tez -les
  \l pour cher -- cher l'es -- poir des na -- tions,
  \l par son é -- toi -- le gui -- dés;
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Saints, before the altar bending,
  % watching long in hope and fear,
  % shall behold God's love unending:
  % Christ will once again appear;
  % come and worship, come and worship,
  % worship Christ, the newborn King.
  \l Vo -- tre es -- poir se com -- ble -- ra,
  \l vous les saints de -- vant l'au -- tel;
  \l Jé -- sus Christ ré -- ap -- pa -- raî -- tra
  \l ce der -- nier jour é -- ter -- nel;
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


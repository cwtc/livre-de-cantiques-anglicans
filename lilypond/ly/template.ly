\include "../common_liturgy.ly"

global = {
  \key bes \major
  \time 4/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  
  \bar "|."
} 

alto = \relative c' {
  \global
  
}

tenor = \relative c {
  \global
  
}

bass = \relative c {
  \global
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "

}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "

}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "

  
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "

}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "Chantez gloire au Seigneur,"
          "car grande est sa justice."
          "Aimez-vous comme des frères;"
          "trouvez la source en Christ."
          "Pour nous racheter de la mort,"
          "Dieu a donné son Fils."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}


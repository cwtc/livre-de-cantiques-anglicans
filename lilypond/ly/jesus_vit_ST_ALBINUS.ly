\include "../common_liturgy.ly"

global = {
  \key c \major
  \time 2/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  e2 c |  g'2. e4 |  a2 a |  g1 \bar "|"
  e'2 d |  c b |  a c |  c1 |  g \bar "|" \break
  c2 g |  a e |  g f |  e1 \bar "|" 
  e'2 b |  c g |  a c |  d1 |  c \bar "|" 
  c2 c |  f1 |  e \bar "||"
} 

alto = \relative c' {
  \global  
  e2 c | g'2. e4 | a2 a | s1 
  g2 g | e e | c c4( d) | e1 | e 
  e2 e | c c | c c4( b) | c1 
  g'2 g | e g | g f | f1 e 
  e2 e | a1 g 
}

tenor = \relative c {\clef bass 
  \global
  e2 c | g'2. e4 | a2 a | g1 
  c2 b | a g | a a4( b) | c1 | c1
  c2 c | a a | g g | g1
  e'2 d | c c | c c | b1 | c 
  c2 c | c1 | c 
}

bass = \relative c {\clef bass 
  \global
  e2 c | g'2. e4 | a2 a | s1 
  c2 g | a e | f f | c1 | c
  a2 c | f a | e g | c,1 
  c'2 g | a e | f a | g1 | c, 
  c'2 a | f1 | c 
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % "JESUS lives! thy terrors now"
  % "    Can, O Death, no more appal us;"
  % "Jesus lives! by this we know"
  % "    Thou, O grave, canst not enthral us."
  % "                                    Alleluya!"

  % TO ADAPT?
  \l Jé -- sus vit! Tu ne sau -- rais
  \l tombe ef -- fra -- yer da -- van -- ta -- ge.
  \l Jé -- sus vit! De tes ar -- rêts,
  \l ô mort, es dé -- truit l'ou -- tra -- ge.
  \l _ _ _ _
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % "Jesus lives! henceforth is death"
  % "    But the gate of life immortal;"
  % "This shall calm our trembling breath,"
  % "    When we pass its gloomy portal."
  % "                                    Alleluya!"

  % TO ADAPT?
  \l Jé -- sus vit! Et dé -- sor -- mais,
  \l la mort du ciel est l'en -- trée, __ _
  \l Au lieu du trou -- ble, la paix
  \l rem -- plit mon âme as -- sur -- rée. __ _
  \l Al -- lé -- lu -- ia!
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % Jesus lives! for us he died;"
  % "    Then, alone to Jesus living,"
  % "Pure in heart may we abide,"
  % "    Glory to our Saviour giving,"
  % "                                    Alleluya!"

  % TO ADAPT?
  \l Jé -- sus vit! Sur u -- ne croix
  \l pour nous il don -- na sa vi -- e.
  \l Vivre a -- vec lui, c'est mon choix,
  \l mon doux es -- poir, mon en -- vi -- e.
  \l _ _ _ _
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % "Jesus lives! our hearts know well"
  % "    Nought from us his love shall sever;"
  % "Life, nor death, nor powers of hell"
  % "    Tear us from his keeping ever."
  % "                                    Alleluya!"
  \l Jé -- sus vit! Tu ne pour -- ras
  \l nous ra -- vir à sa ten -- dres -- se;
  \l Fuis, Sa -- tan, car dans ces bras
  \l c'est son a -- mour qui nous pres -- se.
  \l _ _ _ _
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

% \markup {
%   \fill-line {
%     \hspace #0.1 % moves the column off the left margin;
%      % can be removed if space on the page is tight
%      \column {
%       \line { \bold "5 "
%         \column {
%           % "Jesus lives! to him the throne"
%           % "    Over all the world is given;"
%           % "May we go where he is gone,"
%           % "    Rest and reign with him in heaven."
%           % "                                    Alleluya!"
%         }
%       }
%     }
%     \hspace #0.1 % adds horizontal spacing between columns;
%   \hspace #0.1 % gives some extra space on the right margin;
%   % can be removed if page space is tight
%   }
% }

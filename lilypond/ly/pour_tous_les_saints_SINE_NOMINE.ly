\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  b4\rest d b a |
  g2. d4 |
  e g a d, |
  b'1 \bar "|" \break

  a2 a4 g4 |
  fis2 fis |
  g4 a fis e |
  \tieDashed d2.~ \bar "|" \break

  d4 |
  g2 \sd g4( g4) \ss 
  d'2. d4 |
  c d c8[ b] a[ g] |
  a2 \bar "|" \break

  d( |
  e4) d8[ c] d2 |
  g,2. a8[( b] |
  c4) b a2 |
  g1 \bar "|."
}

alto = \relative c'' {
  \global
  s4 g g fis |
  e2. d4 |
  c d c d |
  d1 \bar "|"

  d2 d4 e |
  a,2 d |
  d4 d d cis |
  \slurDotted d2.( \bar "|"

  a4) |
  d2 \slurSolid \shiftOff d4( c) |
  b2 g' |
  g4 a g d |
  fis2 \bar "|"

  g4( f |
  e) g g(fis!) |
  e2 g4( f |
  e) d c( d) |
  b1 \bar "|."
}

tenor = \relative c' {
  \global
  r4 d4 e c |
  b2. g4 |
  g g g fis |
  g1 \bar "|"

  a2 b4 cis |
  d2 a |
  b4 a b g |
  \tieDotted fis2.~ \bar "|"

  fis4 |
  g2 g2 
  g2 b |
  c4 a d d |
  d2 \bar "|"

  d2( |
  c4) e d2 |
  d4( c) b( a8[ g] |
  c4) g g( fis) |
  g1 \bar "|."
}

bass = \relative c {
  \global
  \teeny g4 \normalsize b c d |
  e2. b4 |
  c b a d |
  g,1 \bar "|"

  fis'2 fis4 e |
  d2 cis |
  b4 fis g a |
  \slurDotted d2.( \bar "|"

  c4) |
  \slurSolid b( c) b a |
  g2 g'4( fis) |
  e fis g b, |
  d( c) \bar "||"

  b2( |
  c4) a b2 |
  e d( |
  c4) d e( d) |
  g,1 \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % \l For all the Saints, who from their la -- bours \set ignoreMelismata = ##t  rest,
  % \unset ignoreMelismata
  % \l Who Thee by  faith be -- fore the world con --
  % \set ignoreMelismata = ##t
  % fest,
  % \l Thy
  % \unset ignoreMelismata
  % Name, O Je -- su, be for ev -- er blest.
  % \l Al -- le -- lu -- ia, Al -- le -- lu -- ia!
  \l Pour tous les Saints, en re -- pos des la -- beurs,
  \l qui ont con -- fes -- sé par foi le Seig -- neur:
  \l bé -- nis -- sons le Nom de Jé -- sus le _ Sau -- _ veur.
  
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % ADAPTED
  % \l Thou wast their Rock, their Fort -- ress and their Might;
  \l C'est toi qui fus leur rem -- part i -- ci -- bas;
  % \set ignoreMelismata = ##t
  % \l Thou, Lord,
  % \unset ignoreMelismata
  % their Cap -- tain in the well fought fight;
  \l leur Ca -- pi -- tai -- ne dans leurs durs com -- bats.
  % \l Et ton Es -- prit tou -- jours les é -- clai -- ra
  % \l Thou, "in the" dark -- ness drear, their one true Light.
  % \l Dans leurs souf -- fran -- ces et leurs durs com -- bats.
  _ \l Toi, aux té -- nè -- bres, tu les é _ -- clai _ -- ras.
  \l Al -- _ lé -- _ lu -- ia, al _ _ -- lé -- lu -- ia!
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % \l O may Thy sol -- diers, faith -- ful, true and bold,
  % \set ignoreMelismata = ##t
  % \l Fight as
  % \unset ignoreMelismata
  % the Saints who no -- bly fought of
  % \set ignoreMelismata = ##t
  % old,
  % \l And
  % \unset ignoreMelismata
  % win, with them, the vic -- tor’s crown of gold. 
  % Nous, leurs en -- fants,
  % Nous i -- rons en a -- vant,
  % Fiers de leurs noms et fi -- dè -- les sol -- dats,
  % Sous la ban -- niè -- re du Roi tri -- om -- phant.
  % Lame!
  \l Que tes sol -- dats fi -- dèles et au -- da -- cieux
  \l com -- bat -- tent com -- me tes Saints pré -- ci -- eux,
  \l qu'ils gag -- nent leur cou -- ron -- ne, vic -- tor -- _ i -- _ eux.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  \set ignoreMelismata = ##t
 %  \l O blest com -- mu -- nion! fel -- low -- ship div -- ine!
 % \set ignoreMelismata = ##t
 % \l We feeb -- ly strug -- gle, they in glo --
 %  \set ignoreMelismata = ##t
 %  ry shine;
 %  \unset ignoreMelismata
 %  \l Yet all are one in thee, for all are thine.
 \l Ô com -- mun -- ion, sain -- te fra -- ter -- ni -- té!
 \l Nous lut -- tons, fai -- bles; ils sont glo -- ri -- fiés: _
 \l En -- semble _ on est en toi u -- _ ni -- _ fié.
}

% verseFive = \lyricmode {
%   \set stanza = "5 "
%   \l And when the strife is fierce, the war -- fare long,
%   \l Steals on the ear the dis -- tant tri -- umph
%   \set ignoreMelismata = ##t
%   song,
%   \l And
%   \unset ignoreMelismata
%   hearts are brave a -- gain, and arms are strong.
% }

% verseSix = \lyricmode {
%   \set stanza = "6."
%   The gol -- den even -- ing brigh -- tens in the west;
%   Soon, soon to faith -- ful war -- riors com -- eth rest;
%   \set associatedVoice = alt
%   Sweet is
%   \set associatedVoice = sopranosHarm
%   the calm of Pa -- ra -- dise the blest.
% }

% verseSeven = \lyricmode {
%   \set stanza = "7 "
%   Un jour vien -- dra plus glo -- ri -- eux en -- core,
%   où tous les saints tri -- om -- phants de la mort
%   en tou -- re -- ront le Seig -- neur pour tou -- jours.
% }

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    % \new Lyrics  \lyricsto soprano \verseFive
    % \new Lyrics  \lyricsto soprano \verseSix
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % And when the strife is fierce, the warfare long,
          % Steals on the ear, the distant triumph-song,
          % And hearts are brave again, and arms are strong.
          "Le combat est feroce et épuisant,"
          "mais on entend de loin enfin le chant"
          "de sa victoire, nous refortifiant."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "6 "
        \column {
          % The golden evening brightens in the west;
          % Soon, soon to faithful warriors comes the rest;
          % Sweet is the calm of Paradise the bless'd.
          "Le crepuscule doré vient de l'Ouest;"
          "aux guerriers vient leur repos de richesse:"
          "Calme et doux, le Paradis céleste."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "7 "
        \column {
          % But lo! there breaks a yet more glorious day;
          % The Saints triumphant rise in bright array;
          % The King of Glory passes on His way.
          "Mais un jour plus glorieux encore s'en vient:"
          "La résurrection des triomphants Saints;"
          "le Roi de Gloire se fait son chemin."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "8 "
        \column {
          % From earth's wide bounds, from ocean's farthest coast,
          % Through gates of pearl streams in the countless host,
          % Singing to Father, Son, and Holy Ghost,
          "Et venant des horizons infinis,"
          "aux portes de perle, nombreux, ils arrivent,"
          "chantant au Père, au Fils, au Saint-Esprit."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/I-N/Immaculate%20Mary%20(LOURDES%20HYMN).ly

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  d4 |
  g g b |
  g g b |
  a a b8[ a] | 
  g2 \bar "" \break

  d4 |
  g g b |
  g g b|
  a a b8[ a] |
  g2. \bar "||" \break

  c2^\markup {\italic Refrain.} c4 |
  b2 b4 |
  a a a |
  d2 g,4 | \break
  c2 c4 |
  b b b |
  a2( b8[ a]) |
  g2 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  b4 |
  d d d |
  e e fis |
  g g fis |
  d2

  d4 |
  e e fis |
  e d g8[ fis] |
  e4 g fis |
  d2. 

  e4( g) a4 |
  g( a) g8[ fis] |
  e4 g fis |
  g( fis) d4 |
  e( g) fis8[ e] |
  dis8[ e] fis4 g8[ fis] |
  e4( g fis) |
  d2
}

tenor = \relative c' {
  \global
  \partial 4  
  g4 |
  b b b |
  b b d |
  e c d |
  b2

    a4 |
  b b d |
  c b d |
  c c d |
  b2. 

    c2 d4 |
  d2 d4 |
  c c d |
  d( c) b |
  c( b) a8[ g] |
  fis4 b b |
  c( e d8[ c]) |
  b2
}

bass = \relative c {
  \global
  \partial 4
  g4 |
  g' g fis |
  e e d |
  c e d |
  g2

    fis4 |
  e e b |
  c g' g |
  a a d, |
  g2. \bar "||"

  c,4( e) fis4 |
  g2 b,4 |
  c e d |
  b'( a) g |
  a2 a,4 |
  b dis e |
  c2( d4) |
  g2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Que l'a -- mour m'en -- flam -- me, Jé -- sus, mon Sau -- veur!
  \l Qu'il vienne en mon â -- me et brû -- le mon cœur!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Des -- cen -- dez, saints an -- ges, ve -- nez en ce lieu
  \l of -- frir mes lou -- an -- ges au Cœur de mon Dieu!

  \l A -- mour, a -- mour, au Cœur de Jé -- sus!
  \set ignoreMelismata = ##t
  \l A -- mour, a -- mour, au Cœur de Jé -- sus!
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Ô Cœur a -- do -- ra -- ble! Ac -- cep -- te nos chants,
  \l et sois fa -- vo -- ra -- ble à tes chers en -- fants!
  
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "

}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

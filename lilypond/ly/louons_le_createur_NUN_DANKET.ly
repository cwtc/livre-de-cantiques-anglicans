\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/I-N/Now%20Thank%20We%20All%20Our%20God%20(NUN%20DANKET%20ALLE%20GOTT).ly

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4
  c4 |
  c c d d |
  c2. c4 |
  bes a g a |
  g2 f4 \bar "|" \break

  c'4 |
  c c d d |
  c2. c4 |
  bes a g a |
  g2 f4 \bar "|" \break

  g4 |
  g g a c |
  g2. g4 |
  a8[ b] c4 d b |
  c2. \bar "|" \break

  c4 |
  d c bes a |
  bes2. a4 |
  g f f e |
  f2. \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  f4 |
  f f f d8[ e] |
  f2. f8[ ees] |
  d4 f e f 
  f( e) f 

  f4 |
  f f f f8[ e] |
  f2. f4 |
  f f8[ e] d4 e |
  d( e) f 

  e4 f e8[ d] c4 c |
  c2. c4 |
  c8[ d] e4 f e |
  e2. 

  e4 |
  d d d fis |
  g2. f8[ e] |
  d4 a8[ bes] c4 c |
  c2. 
}

tenor = \relative c' {
  \global
  \partial 4
  a4 |
  a c bes f8[ g] |
  a2. a4 |
  bes c c8[ bes] a4 |
  bes2 a4

  a4 |
  a  a bes bes |
  a2. a4 |
  bes c d4. cis8 |
  d4( c8[ bes]) a4 

  c4 |
  d4 c8[ b] a4 f |
  e2. c'4 |
  c g8[ c] c[ b] a[ gis] |
  a2. 

  a4 |
  a g8[ a] bes4 c |
  bes2. c4 |
  d f,8[ g] a4 g8[ bes] |
  a2. 
}

bass = \relative c {
  \global
  \partial 4
  f4 |
  f a bes bes, |
  f'2. f,4 |
  g a8[ bes] c4 f |
  c2 f4 

  f4 |
  f f8[ ees] d[ c] bes4 |
  f'2. f8[ e] |
  d4 c bes a |
  bes( c) f4 

  c4 |
  b c f,8[ g] a[ b] |
  c2. e4 |
  f e d e |
  a,2. 

  a'8[ g] |
  fis4 e8[ fis] g4 d |
  g,2. a4 |
  bes d c c |
  f,2. 
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Now thank we all our God,
  % with heart, and hands, and voices,
  % who wondrous things has done,
  % in whom this world rejoices;
  % who from our mothers' arms
  % has blessed us on our way
  % with countless gifts of love,
  % and still is ours today.

  \l Lou -- ons le Cré -- a -- teur;
  \l chan -- tons à Dieu lou -- an -- ges!
  \l Et joi -- gnons no -- tre voix
  \l au con -- cert de ses an -- ges!
  \l Dès les bras ma -- ter -- nels
  \l il nous a pro -- té -- gés
  \l et, jus -- qu'au der -- nier jour,
  \l il est no -- tre ber -- ger.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % O may this bounteous God
  % through all our life be near us,
  % with ever-joyful hearts
  % and blessed peace to cheer us,
  % and keep us strong in grace,
  % and guide us when perplexed,
  % and free us from all ills
  % in this world and the next. 

  \l Lou -- é soit no -- tre Dieu!
  \l Que no -- tre vie en -- tiè -- re
  \l tous nous vi -- vions joy -- eux
  \l sous le re -- gard du Pè -- re;
  \l qu'il nous tienne en sa grâce
  \l et nous gui -- de tou -- jours,
  \l nous gar -- de du mal -- heur
  \l par son u -- nique a -- mour.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % O may this bounteous God
  % through all our life be near us,
  % with ever-joyful hearts
  % and blessed peace to cheer us,
  % and keep us strong in grace,
  % and guide us when perplexed,
  % and free us from all ills
  % in this world and the next. 

  \l De ce Dieu trois fois saint
  \l qui rè -- gne dans la gloi -- re,
  \l chré -- tiens, em -- pres -- sons -nous
  \l de chan -- ter la vic -- toi -- re;
  \l c'est lui qui nous u -- nit
  \l et nous fait re -- trou -- ver
  \l le che -- min de l'a -- mour
  \l et de la li -- ber -- té.  
}

% verseesour = \lyricmode {
%   \set ignoreMelismata = ##t
%   \set stanza = "4 "

% }


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    % \new Lyrics  \lyricsto soprano \verseesour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

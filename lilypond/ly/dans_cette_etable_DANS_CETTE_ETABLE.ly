\include "../common_liturgy.ly"

global = {
  \key bes \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  bes2 d4 c4 | bes2 bes4 f4 | g4 g4 f4 ees4 | d4. c8 d4 ees4 |
  f2 f4 f4 | g4 bes4 a4 c4 | bes2. f4 |
  bes4 f4 g4 aes4 | g2. g4 | c4 g4 a4 bes4 | a2. bes4 |
  c4 d4 bes4 c4 | d2 bes4 d4 | c4 bes4 a4 g4 |
  f4 g4 a4 bes4 | f2 bes2
  \bar "|."
} 

alto = \relative c' {
  \global
  d2 f4 ees4 | d2 d4 bes4 | bes4 bes4 bes4 a4 | bes4. a8 bes4 c4 |
  d2 d4 bes4 | bes4 c4 c4 ees4 | d2. d4 |
  d4 d4 ees4 f4 | ees2. f4 | e4 e4 f4 g4 | f2. d4 |
  ees4 f4 d4 ees4 | f2 d4 f4 | f4 ees4 c4 c4 |
  bes4 bes4 ees4 d4 | c4( ees4) d2
}

tenor = \relative c {
  \global
  f2 f4 f4 | f2 f4 f4 | g4 g4 f4 f4 | f4. f8 f4 f4 |
  f2 f4 f4 | g4 g4 a4 a4 | f2. f4 |
  f4 f4 g4 f4 | bes2. bes4 | g4 c4 a4 g4 | c2. f,4 |
  f4 a4 bes4 a4 | f2 f4 f4 | f4 g4 f4 g4 |
  f4 f4 f4 f4 | a2 f2
}

bass = \relative c {
  bes2 bes4 bes4 | bes2 bes4 d4 | ees4 ees4 d4 c4 | bes4. bes8 bes4 bes4 |
  bes2 bes4 d4 | ees4 ees4 f4 f4 | bes,2. bes4 |
  bes4 bes4 bes4 bes4 | ees2. des4 | c4 c4 c4 c4 | f2. bes,4 |
  f'4 f4 f4 f4 | bes,2 bes4 bes4 | a4 g4 f4 ees'4 |
  d4 d4 c4 bes4 | f'2 bes,2
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Dans cette é -- ta -- ble,
  \l que Jé -- sus est char -- mant! \bar "|" \break
  \l Qu'il est ai -- ma -- ble
  \l dans son a -- bais -- se -- ment! \bar "|" \break
  \l Que d'at -- traits à la fois!
  \l Tous les pa -- lais des \l rois
  \l n'ont rien de com -- par -- a -- ble \bar "|" \break
  \l aux beau -- tés que je vois
  \l dans cette é -- ta -- ble.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Que sa puis -- san -- ce
  \l pa -- raît bien en ce jour,
  \l mal -- gré l'en -- fan -- ce
  \l où le ré -- duit l'a -- mour!
  \l Le mon -- de ra -- che -- té,
  \l en tout l'en -- fer domp -- \l té,
  \l font voir qu'à sa nai -- san -- ce,
  \l rien n'est si re -- dou -- té 
  \l que sa puis -- san -- ce.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Tou -- chant my -- stè -- re!
  \l Jé -- sus, souf -- frant pour nous,
  \l d'un Dieu sé -- vè -- re 
  \l a -- pai -- se le cour -- roux.
  \l Du tes -- ta -- ment nou -- veau
  \l il est le doux A -- \l gneau;
  \l il doit sau -- ver la ter -- re
  \l por -- tant no -- tre far -- deau:
  \l Tou -- chant my -- stè -- re.
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l S'il est sen -- si -- ble,
  \l ce n'est qu'à nos mal -- heurs;
  \l le froid pé -- ni -- ble
  \l ne cau -- se point ses pleurs.
  \l Mon cœur à tant d'at -- traits,
  \l à de si doux bien -- \l faits;
  \l à ce charme in -- vin -- ci -- ble
  \l doit cé -- der dé -- sor -- mais,
  \l s'il est sen -- si -- ble.
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  % TODO MARKING ADAPTED TUTOYER
  % \l Ah! Je vous ai -- me;
  \l Ah! Que je t'ai -- me,
  % \l vous vous ca -- chez en vain,
  \l tu te ca -- ches en vain,
  \l beau -- té su -- prê -- me,
  \l Jé -- sus, En -- fant di -- vin!
  % \l Vous êtes à mex yeux le puis -- sant Roi des cieux,
  \l Tu es donc a mes yeux
  \l le puis -- sant Roi des \l cieux,
  \l le Fils de Dieu lui -mê -- me,
  \l des -- cen -- du dans ces lieux:
  % "Ah! Je vous aime."
  \l Ah! Que je t'ai -- me.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

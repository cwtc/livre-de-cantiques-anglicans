\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d4 g g4. fis8 |
  g4 b b( a) |
  d d d4. c8 |
  b4 a b2 | \break
  
  d,4 g g4. fis8 |
  g4 b b( a) |
  d a a4. fis8 |
  fis4 e d2 |
  
  d'4 d d g, |
  c b b( a) |
  d d d g, |
  c b b( a) |
  
  e' e e d |
  c b c2 |
  a4 b8[ c] d4. g,8 |
  g4 a b2 | \break
  
  e4.^\markup { \italic "Refrain" } e8 e4 d |
  c b c2 |
  a4 b8[ c] d4. g,8 |
  g4 a g2 \bar "|."
}

alto = \relative c' {
  \global
  d4 d d4. d8 |
  d4 g g( fis) |
  g fis e a |
  g fis g2 |
  
  d4 d d4. d8 |
  b4 g' g2 |
  fis4 e fis4. d8 |
  d4 cis d2 |
  
  d4 d d g |
  a g g( fis) |
  d d d g |
  a g g( fis) |
  
  g g g gis |
  a gis a2 |
  fis4 fis g4. d8 |
  e4 fis g2 |
  
  c4 c c b |
  a gis a2 |
  d,4 fis g4. d8 |
  d4 fis d2 \bar "|."
}

tenor = \relative c' {
  \global
   b4 b b4. a8 |
  g4 d' d2 |
  d4 d e e |
  d d d2 |
  
  b4 b b4. a8 |
  g4 d' e2 |
  d4 e a,4. a8 |
  b4 g fis2 |
  
  d'4 d d d |
  d d d2 |
  d4 d d d |
  d d d2 |
  
  e4 e c d |
  e e e2 |
  d4 d d4. b8 |
  b4 d d2 |
  
  e4 e e d |
  c e e2 |
  d4 d d4. b8 |
  b4 c b2 \bar "|."
}

bass = \relative c' {
  \global
  g4 g g d |
  b g d'2 |
  b4 b c c |
  d d g,2 |
  
  g'4 g g d |
  e d cis4.( a8) |
  b4 cis d fis, |
  g a d2 |
  
  d'4 d d b |
  fis g d2 |
  d'4 d d b |
  fis g d2 |
  
  c4 c c b |
  a e' a2 |
  c4 c b g |
  e d g,2 | \bar "||"
  
  c'4 c c b |
  a e a( g) |
  fis c' b g |
  d d g2 \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % \l Hark! the her -- ald an -- gels sing, __ “Glo -- ry to the new -- born King!
  % \l É -- cou -- tez le chant des an -- ges: __ Gloire au di -- vin en -- fant Roi!
  \l É -- cou -- tez le chant du ciel: _ le Roi naît en Is-- ra-- ël!
  % % \unset associatedVoice
  % \l Peace on earth, and
  % \l Ap -- por -- tez lui vos lou -- an -- ges,
  \l Paix sur ter -- re par le Christ, _
  % % \set associatedVoice = "basses"
  % \l mer -- cy mild; __
  % % \unset associatedVoice
  % \l God and sin -- ners re -- con -- ciled.”
  \l Dieu et l'hom -- me ré -- u -- nis.
  % \l Vos hom -- ma -- ges, vo -- tre foi.
  % \l Joy -- ful all ye na -- tions, rise; __
  % \l Le -- vez -vous gaî -- ment, ô frè -- res;
  \l Tous les peu -- ples et les an -- ges,
  % \l Join the tri -- umph of the skies;
  % \l au -- jour -- d'hui naît la lu -- miè -- re,
  \l Chan -- tez en -- semble en tri -- om -- phe:
  % \l With th’an -- gel -- ic hosts pro -- claim,
  \l A -- vec les cieux pro -- cla -- mez:
  % \l “Christ is born in Beth -- le -- hem.”
  \l à Beth -- lé -- em, le Christ est né.
  
  % % \dropLyricsV
  % % \set associatedVoice = "basses"
  % \l Hark the her -- ald
  % an -- gels sing,
  % % \unset associatedVoice
  % \l Glo -- ry to the new -- born King.
  % \l É -- cou -- tez le chant du ciel,
  % \l Le Roi _ naît en Is -- ra -- ël.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % \set associatedVoice = "altos"
  % \l Christ, by high -- est heav’n a -- dored; Christ, the ev -- er -- last -- ing Lord;
  % % \unset associatedVoice\
  % \l Ô Christ que le ciel ré -- vè -- re, é -- ter -- nel -- le -- ment bé -- ni,
  \l Christ, a -- do -- ré par les cieux, _
  \l Christ, per -- pet -- u -- el Seig -- neur,
  % \l Late in time be -- hold
  % % \set associatedVoice = "basses"
  % \l Him come, __
  % \l te voi -- ci qui viens sur ter -- re,
  \l De la vier -- ge tu es né, _
  % % \unset associatedVoice
  % \l Off -- spring of the Vir -- gin’s womb.
  % \l d'u -- ne vier -- ge tu na -- quis.
  \l Sur terre en -- fin ar -- ri -- vé!
  % \l Veil’d in flesh the God -- head see; __
  % \l Toi qui nais par -- mi les hom -- mes,
  \l Voi -- ci ha -- bil -- lé en chair le
  % \l Hail th’In -- car -- nate De -- i -- ty, __
  % \l pour tes frè -- res tu te don -- nes,
  \l Fils con -- sub -- stan -- tiel au Pè -- re!
  % \l Pleased as Man with man to dwell, Je -- sus, our Em -- man -- u -- el!
  % \l et vers nous des -- cends du ciel,
  % \l Jé -- sus, notre Em -- ma -- nu -- el.
  \l Ver -- be vi -- vant par -- mi nous,
  \l Notre Em -- _ man -- u -- el, Jé -- sus!

  \l É -- cou -- tez le chant du ciel,
  \l Le Roi _ naît en Is -- ra -- ël.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  \set ignoreMelismata = ##t
  % \l Mild He lays His glo -- ry by, __
  % \l Sa -- lut, Prin -- ce de jus -- ti -- ce!
  \l Il ad -- op -- ta no -- tre sort, _
  % \l Born that man no more may die,
  % \l Sa -- lut, Prin -- ce de la paix!
  \l Né pour con -- qué -- rir la mort,
  % % \unset associatedVoice
  % \l Born to raise the
  % % \set associatedVoice = "basses"
  % sons of earth, __
  % \l Te ser -- vir est mon dé -- li -- ce;
  \l Né pour nous res -- sus -- ci -- ter, _
  % % \unset associatedVoice
  % \l tu me bé -- nis à ja -- mais.
  % \l Born to give them sec -- ond birth.
  \l Né que l'on soit deux fois né.
  % \l Ris’n with heal -- ing in His wings,
  % \l Tu re -- non -- ças à la gloi -- re,
  \l Le So -- leil de la jus -- ti -- ce,
  % \l Light and life to all He brings,
  % \l mais nous chan -- tons ta vic -- toi -- re,
  \l sous ses ai -- les il gué -- rit, _ __
  % \l Hail, the Sun of Right -- eous -- ness!
  % \l car tu as vain -- cu la mort
  % \l Hail, the heav’n born Prince of Peace!
  % \l et tu chan -- ges no -- tre sort.
  \l Ad -- mi -- ra -- ble Con -- seil -- ler!
  \l Dieu et _ Prin -- ce de la Paix!
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

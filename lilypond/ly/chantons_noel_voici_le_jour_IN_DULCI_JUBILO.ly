\include "../common_liturgy.ly"

% https://github.com/bbloomf/christmas-carols/blob/master/ly/8.5garamond/073-Good%20Christian%20Men%2C%20Rejoice.ly

global = {
  \key f \major
  \time 6/8
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 8 f8 |
  f4 f8 a4 bes8 |
  c4( d8 c4) c8 |
  f,4 f8 a4 bes8 |
  
  c4( d8 c4) bes8\rest | \break
  c4 d8 c4 bes8 |
  a4 g8 f4. |
%  f f |
  g4 g8 a4 g8 |
  
  f4 g8 a4 bes8\rest | \break
  c4 d8 c4 bes8 |
  a4 g8 f4 f8 |
  g4 g8 a4 g8 |
  
  f4 g8 a4 bes8\rest | \break
  d,4 d8 e4 e8 |
  f4.( c') |
  \partial 8*5 a4 a8 g4 g8 f4. bes4\rest \bar "|."
} 

alto = \relative c' {
  \global
  c8 |
  d4 c8 f8[ e] d |
  c4( f8 c4) f8 |
  d4 c8 f[ e] d |
  
  c4( f8 e4) s8 |
  f4 f8 e4 e8 |
  f4 e8 d4. |
%  f f |
  f4 f8 f4 e8 |
  
  f4 f8 f4 s8 |
  f4 f8 e4 g8 |
  f4 e8 d4 f8 |
  f4 f8 f4 e8 |
  
  f4 f8 f4 s8 |
  d4 d8 d4 cis8 |
  d4.( e) | 
  f4 f8 f4 e8 
  f4. s4 \bar "|."
}

tenor = \relative c' {
  \global
  a8 |
  bes4 a8 c4 bes8 |
  a4( bes8 g4) a8 |
  bes4 a8 c4 bes8 |
  
  a4( bes8 g4) s8 |
  c4 bes8 g4 c8 |
  c4 bes8 a4. |
%  f a |
  d4 d8 c4 bes8 |
  
  a4 bes8 c4 s8 |
  c4 bes8 g4 c8 |
  c4 bes8 a4 c8 |
  d4 d8 c[ d] bes |
  
  a4 bes8 c4 s8 |
  a4 a8 g4 g8 |
  a4( bes8 g4.) |
  f4 a8 d4 c8 
  a4. s4 \bar "|."
}

bass = \relative c {
  \global
  f8 |
  f4 f8 f4 f8 |
  f4.( e4) f8 |
  f4 f8 f4 f8 |
  
  f4.( c4) d8\rest |
  a4 bes8 c4 c8 |
  f4 c8 d4. |
%  f d |
  bes4 bes8 c4 c8 |
  
  f4 f8 f4 d8\rest |
  a4 bes8 c4 e8 |
  f4 c8 d4 a8 |
  bes4 bes8 c4 c8 |
  
  f4 f8 f4 d8\rest |
  f4 f8 e4 e8 |
  d4.( c) |
  f4 d8 bes4 c8 
  f4. d4\rest \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Good Christian men, rejoice,
  % With heart, and soul, and voice;
  % Give ye heed to what we say:
  % Jesus Christ is born today;
  % Ox and ass before him bow,
  % And he is in the manger now.
  % Christ is born today!
  % Christ is born today!

  % In dulci jubilo,
  % Let us our homage show,
  % Our heart’s joy reclineth
  % In praesepio,
  % And like a bright star shineth,
  % Matris in gremio.
  % Alpha es et O. 
  \l Chan -- tons No -- ël, voi -- ci le jour,
  \l au front la joie, au cœur l'a -- mour!
  \l Là dans u -- ne crè -- che fait homme un Dieu re -- pose en -- fant;
  \l Sur la pail -- le fraî -- che, il _ nous re -- garde en sou -- ri ant:
  \l c'est de là qu'il prê -- che
  \l au pe -- tit, au grand.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Good Christian men, rejoice,
  % With heart, and soul, and voice;
  % Now ye hear of endless bliss:
  % Jesus Christ was born for this!
  % He hath oped the heavenly door,
  % And man is blessèd evermore.
  % Christ was born for this!
  % Christ was born for this!

  % O Jesu parvule!
  % I yearn for thee always!
  % Hear me, I beseech thee,
  % O Puer optime!
  % My pray’r let it reach thee,
  % O Princeps gloriae;
  % Trahe me post te.

  \l Les An -- ges chan -- tent dans les airs
  \l la paix du ciel à l'u -- ni -- vers:
  \l Dou -- ce mé -- lo -- di -- e! Ve -- nez, ber -- gers, pres -- sez le pas;
  \l C'est le doux Mes -- si -- e, qui _ vous ap -- pelle et tend les bras:
  \l Sa grâce in -- fi -- ni -- e
  \l vous at -- tend là -bas.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Good Christian men, rejoice,
  % With heart, and soul, and voice;
  % Now ye need not fear the grave:
  % Jesus Christ was born to save!
  % Calls you one and calls you all,
  % To gain his everlasting hall:
  % Christ was born to save!
  % Christ was born to save!

  % O Patris caritas!
  % O Nati lenitas!
  % Deeply were we stained,
  % Per nostra crimina;
  % But thou hast for us gained
  % Coelorum gaudia!
  % O that we were there! 
  
  \l Ve -- nez, ô Sa -- ges d'O -- ri -- ent,
  \l voir pauvre un Dieu pe -- tit en -- fant!
  \l Ren -- dre vos hom -- ma -- ges, of -- frir vos cœurs et vos ca -- deaux;
  \l c'est le Roi des Sa -- ges, le _ Roi des rois des temps nou -- veaux!
  \l Vous se -- rez, Rois Ma -- ges,
  \l les pre -- miers hé -- raults.

}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "

  % Ubi sunt gaudia,
  % Where, if they be not there?
  % There are angels singing
  % Nova cantica!
  % And there the bells are ringing
  % In regis curia
  % O that we were there,
  % In dulci jubilo!

  \l A no -- tre tour, al -- lons jo -- yeux
  \l don -- ner nos cœurs au Roi des Cieux!
  \l Dans son humble é -- ta -- ble, il nous ap -- prend la pau -- vre -- té;
  \l Roi vain -- queur ai -- ma -- ble, a _ l'Homme il rend la li -- ber -- té:
  \l Grâce in -- com -- pa -- ra -- ble!
  \l Gloire à sa bon -- té!
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "

  % Ubi sunt gaudia,
  % Where, if they be not there?
  % There are angels singing
  % Nova cantica!
  % And there the bells are ringing
  % In regis curia
  % O that we were there,
  % In dulci jubilo!

  \l À toi d'al -- ler aus -- si, pé -- cheur,
  \l à cet en -- fant, grand gué -- ris -- seur.
  \l Mé -- de -- cin ha -- bi -- le, il rend à l'â -- me la san -- té,
  \l donne au cœur dé -- bi -- le la _ force a -- vec la fer -- me -- té.
  \l Mon -- tre -toi do -- ci -- le: Bon -- ne vo -- lon -- té!
}



\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}



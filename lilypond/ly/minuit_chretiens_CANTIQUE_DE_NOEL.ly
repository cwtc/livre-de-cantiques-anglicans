\include "../common_liturgy.ly"
% https://hymnary.org/hymn/DLSH1913/page/8

global = {
  \key c \major
  \time 12/8
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 2. e4. e4 e8 | g4.~ g4 g8 a4 a8 f4 a8 |
  c2. g4 g8 e4 d8 | c4. e4 f8 g4. f4 d8 | c1. |

  \partial 2. e4. e4 e8 | g4.~ g4 g8 a4 a8 f4 a8 |
  c2. g4 g8 fis4 e8 | b'4. g4 a8 b4. c4 b8 | e,1( e4.) \bar "||" \break

  g8 | g4. a4. d,4. g4. | a4 g8 c4 e,8 a4. g4 \bar "|" \break g8 |
  g4. a4. d,4. g4. | a4 g8 c4 e,8 g2. \bar "||" \break

  c2.~ c4. b4 a8 | b2.~ b4. b4\rest b8 |
  d4.~ d4.~ d4 a8 a4 a8 | c2. c4. \bar "|" \break b4\rest c8 |
  e2. d4.~ d4 g,8 | c2.~ c4. b4 a8 |
  g2.~ g4 g8 a4 g8 | g2.~ g4. \bar "|" \break b4\rest c8 |
  d2.~ d4.~ d4 g,8 | e'2.~ e4. d4. |
  c2. b4. c4 d8 | c1.
  \bar "|."
} 

alto = \relative c' {
  \global
  \partial 2. c4. c4 c8 | c4.~ c4 c8 c4 c8 c4 c8 |
  e2. e4 e8 c4 c8 | c4. c4 c8 b4. b4 b8 | c1. |

  \partial 2. c4. c4 c8 | c4.~ c4 c8 c4 c8 c4 c8 |
  e2. e4 e8 e4 e8 | e4. e4 e8 dis4. dis4 dis8 | e1( e4.) \bar "||" %\break

  e8 | f4. f4. d4. d4. | e4 e8 e4 e8 e4. e4 e8 |
  f4. f4. d4. d4. | e4 e8 e4 e8 e2. \bar "||"

  e2.~ e4. e4 e8 | e2.~ e4. s4 e8 |
  f4.~ f4.~ f4 f8 f4 f8 | e2. e4. s4 e8 |
  g2. g4.~ g4 g8 | g2.~ g4. g4 f8 |
  e2.~ e4 e8 f4 f8 | e2.~ e4. s4 e8 |
  g2.~ g4.~ g4 g8 | g2.~ g4. f4. |
  e2. d4. e4 f8 | e1.
}

tenor = \relative c' {
  \global
  \partial 2. g4. g4 g8 | g4.~ g4 g8 f4 f8 a4 f8 |
  g2. c4 c8 g4 f8 | e4. g4 g8 g4. g4 f8 | e1. |

  \partial 2. g4. g4 g8 | g4.~ g4 g8 f4 f8 a4 a8 |
  g2. c4 g8 a4 g8 | g4. g4 g8 fis4. a4 a8 | g1( g4.) \bar "||" %\break

  b8 | b4. b4. b4. b4. | c4 c8 c4 c8 g4. g4 g8 |
  b4. b4. b4. b4. | c4 c8 c4 c8 g2. \bar "||"

  a2.~ a4. a4 a8 | g2.~ g4. d4\rest g8 |
  a4.~ a4.~ a4 d8 d4 d8 | a2. a4. d,4\rest a'8 |
  c2. b4.~ b4 b8 | c2.~ c4. c4. |
  c2.~ c4 c8 b4 b8 | c2.~ c4. d,4\rest c'8 |
  b2.~ b4.~ b4 b8 | c2.~ c4. a4. |
  g2. g4. g4 g8 | g1.
  \bar "|."
}

bass = \relative c {
  \global
  \partial 2. c4. c4 c8 | e4.~ e4 e8 f4 f8 f4 f8 |
  c2. c4 c8 c4 c8 | c4. c4 c8 g4. g4 g8 | c1. |

  \partial 2. c4. c4 c8 | e4.~ e4 e8 f4 f8 f4 f8 |
  c2. c4 c8 c4 c8 | b4. b4 b8 b4. b4 b8 | e1( e4.) \bar "||" %\break

  e8 | d4. d4. g4. g4. | c,4 c8 c4 c8 c4. c4 c8 |
  d4. d4. g4. g4. | c,4 c8 c4 c8 c2. \bar "||"

  a2.~ a4. c4 c8 | e2.~ e4. s4 e8 |
  d4.~ d4.~ d4 d8 d4 d8 | a'2. a4. s4 a8 |
  a2. g4.~ g4 f8 | e2.~ e4. f4. |
  g2.~ g4 g8 g4 g8 | c,2.~ c4. s4 c8 |
  g'2.~ g4.~ g4 g8 | c,2.~ c4. f4. |
  g2. g4. g,4 g8 | c1.
  \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Mi -- nuit, chré -- tiens! _
  \l C'est l'heu -- re so -- len -- nel -- le,
  \l où l'hom -- me Dieu des -- cen -- dit jus -- qu'à nous
  \l pour ef -- fa -- cer _ la tache or -- i -- gi -- nel -- le,
  \l et de son Père ar -- rê -- ter le cour -- roux. __ _

  \l Le monde en -- tier tres -- sai -- le d'es -- pé -- ran -- ce,
  \l à cet -- te nuit qui lui donne un Sau -- veur.

  \l Peu --  _ "ple, à" ge -- noux, __ _
  \l at -- tends __ _ _ ta dé -- liv -- ran -- ce!
  \l No -- ël, __ _ _  No -- ël! __ _
  \l Voi -- _ ci __ _  le Ré -- demp -- teur! __ _
  \l No -- ël, __ _ _ No -- ël! __ _ 
  \l Voi -- ci le Ré -- demp -- teur!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l De no -- tre foi _
  \l que la lu -- mière ar -- den -- te
  \l nous gui -- de tous au ber -- ceau de l'En -- fant,
  \l comme au -- tre -- fois _ une é -- toi -- le bril -- lan -- te
  \l y con -- dui -- sit les chefs de l'O -- ri -- ent. __ _

  \l Le Roi des rois naît dans une hum -- ble crè -- che;
  \l puis -- sants du jour, fiers de vo -- tre gran -- deur:

  \l À _ votre or -- geuil, __ _
  \l c'est de __ _ _ là que Dieu prê -- che.
  \l Cour -- bez __ _ _ vos fronts __ _
  \l de -- _ vant __ _  le Ré -- demp -- teur! __ _
  \l Cour -- bez __ _ _ vos fronts __ _
  \l de -- vant le Ré -- demp -- teur!
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Le Ré -- demp -- teur _
  \l a bri -- sé toute en -- tra -- ve;
  \l la Terre est libre et le Ciel est ou -- vert.
  \l Il voit un frère _ où n'é -- tait qu'un es -- cla -- ve;
  \l l'a -- mour u -- nit ceux qu'en -- chaî -- nait le fer. __ _

  \l Qui lui di -- ra no -- tre re -- con -- nais -- san -- ce?
  \l C'est pour nous tous qu'il naît, qu'il souffre et meurt.

  \l Peu -- _ ple, de -- bout! __ _ 
  \l Chan -- tons __ _ _ ta dé -- li -- vran -- ce!
  \l No -- ël, __ _ _ No -- ël! __ _ 
  \l Chan -- _ tons __ _ le Ré -- demp -- teur! __ _ 
  \l No -- ël, __ _ _ No -- ël! __ _
  \l Chan -- tons le Ré -- demp -- teur!
  
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

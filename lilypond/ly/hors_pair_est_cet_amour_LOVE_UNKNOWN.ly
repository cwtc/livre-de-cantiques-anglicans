\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/I-N/My%20song%20is%20love%20unknown%20(LOVE%20UNKNOWN).ly

global = {
  \key d \major
  \time 3/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 2
  fis2 |
  a4 b fis2 e |
  \time 2/2 d2. 
  
  e4 |
  fis g a fis | \break
  b2. \bar "||" %\break

  b4 |
  cis b a a |
  d 
  cis b a | \break
  a fis2 gis4 |
  a2. \bar "|" %\break

  a4 |
  \time 3/2 c b g2. 
  g4 |
  b a d,2. 
  e4 |
  \time 2/2 fis g a fis |
  fis2 e |
  d \bar "|."  
} 

alto = \relative c' {
  \global
  \partial 2
  d2 |
  d4 b cis2 cis |
  b2. a4 |
  d d cis d |
  d2. d4 |
  g g cis, fis |
  d e fis cis |
  e2. e4 |
  e( d cis) d |
  e d b2 c |
  d4 c b2. a4 |
  a d d d |
  cis2 cis |
  d \bar "|."
}

tenor = \relative c' {
  \global
  \partial 2
  a2 |
  a4 fis a2. g4 |
  g2( fis4) e4 |
  a a a b |
  g2. g4 |
  g g a cis |
  b a fis a |
  b2.b 4 |
  a2. fis4 |
  g fis g2 e |
  g4 fis a2( g4) e |
  a g fis b |
  a2. g4 |
  fis2 \bar "|." 
}

bass = \relative c {
  \global
  \partial 2
  d2 |
  fis4 d a'2 a, |
  b2. cis4 |
  d e fis d |
  g2 fis4~ fis4 |
  e4 e fis fis |
  b, cis d fis |
  e2 d |
  cis4( b a) d |
  c d e2 c |
  g4 a b2. cis4 |
  d b fis g |
  a2 a |
  d \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % My song is love unknown,
  % My Savior’s love to me,
  % Love to the loveless shown,
  % That they might lovely be.
  % Oh, who am I, that for my sake
  % My Lord should take frail flesh and die?

  % \l Je chante l’a -- mour hors pair
  \l Hors pair est cet a -- mour
  \l de mon Sau -- veur pour \l moi:
  \l l’a -- mour aux sans -a -- mour,
  \l pour qu’il a -- \l do -- ra -- ble soit.
  \l Mais qui suis -je, que \l pour mes peines,
  \l mon Sei -- gneur pren -- ne chair et meure?
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % He came from His blest throne
  % Salvation to bestow;
  % But men made strange, and none
  % The longed-for Christ would know.
  % But, oh, my friend, my friend indeed,
  % Who at my need his life did spend!

  \l Il vint du trô -- ne saint
  \l pour don -- ner le sa -- \l lut;
  \l son a -- mour l'Homme a craint;
  \l le Christ rest -- \l ant in -- con -- nu.
  \l Mais mon A -- mi, en \l vé -- ri -- té,
  \l il a don -- né pour moi sa vie!
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Sometimes they strew His way
  % And his sweet praises sing;
  % Resounding all the day
  % Hosannas to their King.
  % Then “Crucify!” is all their breath,
  % And for his death they thirst and cry.

  % \l Ils dé -- ga -- gèrent sa voie
  \l On dé -- ga -- gait sa voie
  \l en chan -- tant tous sa \l gloire;
  % \l et ils pous -- sèrent au Roi
  \l et on pous -- sait au Roi
  \l des “Ho -- san -- \l na!” pleins de joie.
  % \l Puis ils im -- plo -- rent, Cru -- ci -- fie!
  \l Puis on im -- plo -- rait, \l “Cru -- ci -- fie!”
  \l et leur cri ré -- cla -- mait sa mort.
  
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Why, what hath my Lord done?
  % What makes this rage and spite?
  % He made the lame to run,
  % He gave the blind their sight.
  % Sweet injuries! Yet they at these
  % Themselves displease, and 'gainst him rise.

  \l Qu’a -t-il fait, mon Sei -- gneur?
  \l D’où ces rages et co -- \l lères?
  \l Le boi -- teux peut cou -- rir,
  \l et l'a -- veu -- \l gle voit en -- core.
  % \l Bles -- su -- res douces! Ils les dé -- testent;
  % \l ils ma -- ni -- festent et le re -- poussent.
  \l Bles -- su -- res douces! On \l les dé -- teste;
  \l on ma -- ni -- feste et le re -- pousse.
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  % They rise, and needs will have
  % my dear Lord made away;
  % a murderer they save,
  % the Prince of Life they slay.
  % Yet cheerful He to suffering goes,
  % that He His foes from thence might free.
  \l Mon cher Sei -- gneur vo -- lé!
  \l On se tire a -- vec \l lui;
  \l on sauve un meur -- tri -- er,
  \l tu -- ant le \l Prin -- ce de Vie.
  \l Ses ad -- ver -- saires il \l ai -- me tant,
  \l souf -- frant a -- fin qu’il les li -- bère.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % In life, no house, no home
          % my Lord on earth might have;
          % in death, no friendly tomb
          % but what a stranger gave.
          % What may I say? Heav'n was His home,
          % but mine the tomb wherein He lay.
          "Il vivait sans maison"
          "sur Terre, mon Seigneur;"
          % "Dans la mort, pas de tombe"
          "et il mourrait sans tombe"
          "hors le don d’un étranger."
          % "Son vrai palais, c’est bien très haut,"
          "Le lieu du Roi, c'est bien très haut,"
          "et son tombeau était pour moi."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Here might I stay and sing,
          % no story so divine;
          % never was love, dear King,
          % never was grief like Thine.
          % This is my Friend, in whose sweet praise
          % I all my days could gladly spend.
          % "Je chante ce compte divin;"
          "Ô quel compte divin!"
          "Le monde n’a jamais vu"
          "un chagrin comme le tien,"
          "ni ton amour reconnu."
          "C’est mon Ami, que je louerai"
          "pour chaque journée de ma vie."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}


\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  c4 |
  f f a a |
  g f g a |
  g f a g |
  f2. \bar "|" \break
  
  g4 |
  a g f a |
  c8[ bes] a[ g] a4 c |
  c2 c |
  d c4( b) |
  c2. \bar "|" \break
  
  a4 |
  c a f a |
  g8[ f] g[ a] g4 f |
  c'2 bes |
  a4.( bes8 g4) g |
  f2. \bar "|."
}

alto = \relative c' {
  \global
  \partial 4
  c4 |
  a c c f |
  e f e f |
  e f f e |
  f2. c4 |
  f c a f' |
  a8[ g] f[ e] f4 e|
  f2 g |
  f e4( d) |
  e2. f4 |
  c f c f |
  e8[ d] e[ f] e4 f |
  f2 f |
  f2. e4 |
  f2.
}

tenor = \relative c {
  \global
  \partial 4
  c4 |
  f a c c |
  c a c c |
  bes a c bes |
  a2. g4 |
  a g f a |
  c8[ bes] a[ g] a4 g |
  a2 g |
  a g |
  g2. f4 |
  a c c c |
  c g c a |
  c2 d |
  c2. bes4 |
  a2.
}

bass = \relative c {
  \global
  \partial 4
  c4 |
  f4 f f f |
  c d c a |
  c d c c |
  f2. c4 |
  f c a f' |
  a8[ g] f[ e] f4 c |
  f2 e |
  d g, |
  c2. f4 |
  f f a f |
  c c c d |
  a2 bes |
  c2. c4 |
  f2.
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % ADAPTED
  % \l All hail the pow'r of Je -- sus' Name!
  \l Sa -- lut, le Nom du Ré -- demp -- teur!
  % \l Let an -- gels pros -- trate fall;
  % \l Jé -- sus, puis -- sant Sau -- veur!
  \l Le ciel lou -- e leur Roi;
  % \l Bring forth the roy -- al di -- a -- dem,
  % \l Nous pros -- ter -- nant tous de -- vant toi,
  \l La di -- a -- dè -- me, c'est à toi;
  % \l And crown him Lord of all!
  \l _ _ _ _ _ _
  % \l Nous pros -- ter -- nant tous de -- vant toi,
  \l La di -- a -- dè -- me, c'est à toi;
  \l _ _ _ _ _ _
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % NEW
  % \l Crown him, ye mar -- tyrs of our God, 
  % \l A -- vec les an -- ges dans les cieux,
  \l Le sang des mar -- tyrs de l'au -- tel
  % \l Who from his al -- tar call:
  % \l les mar -- tyrs glo -- ri -- eux,
  \l t'ap -- pel -- le par leur foi;
  % \l Praise him whose way of pain ye trod
  % \l qui ja -- dis ont souf -- fert pour toi,
  \l Ils t'ont sui -- vi dans leur dou -- leur;
  % \l And crown him Lord of all!
  \l nous te cour -- on -- nons Roi!
  % \l Praise him whose way of pain ye trod
  \l Ils t'ont sui -- vi dans leur dou -- leur;
  % \l And crown him Lord of all!
  \l nous te cour -- on -- nons Roi!
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % NEW
  % \l Hail him, the Heir of Da -- vid's line,
  % \l Whom Da -- vid Lord did call,
  % \l The God in -- car -- nate, Man di -- vine,
  % \l And crown him Lord of all!
  % \l The God in -- car -- nate, Man di -- vine,
  % \l And crown him Lord of all!
  \l Sa -- lut, hé -- ri -- tier de Da -- vid
  \l que Dav -- id a -- per -- çoit;
  \l Il t'ap -- pel -- la Sei -- gneur di -- vin; 
  \l _ _ _ _ _ _
  \l Il t'ap -- pel -- la Sei -- gneur di -- vin; 
  \l _ _ _ _ _ _
}

% verseFour = \lyricmode {
%   \set stanza = "4 "
%   % \l Ye seed of Is -- rael's cho -- sen race,
%   \l Ra -- che -- tés au prix de ton sang,
%   % \l Ye ran -- somed of the fall,
%   \l Ô Sau -- veur tout -puis -- sant,
%   % \l Hail him who saves you by his grace,
%   \l sau -- vés par grâ -- ce, par la foi,
%   % \l And crown him Lord of all!
%   \l nous te cour -- ron -- nons Roi!
%   % \l Hail him who saves you by his grace,
%   \l Sau -- vés par grâ -- ce, par la foi,
%   % \l And crown him Lord of all!
%   \l nous te cour -- ron -- nons Roi!
% }

verseFour = \lyricmode {
  \set stanza = "4 "
  % NEW
  % Ye chosen seed of Israel's race,
  % A remnant weak and small;
  % Hail him who saves you by his grace,
  % And crown him Lord of all.
  \l Le res -- te fai -- ble d'Is -- ra -- ël,
  \l li -- bé -- ré de la Loi,
  \l C'est de ta grâ -- ce, leur sa -- lut;
  \l _ _ _ _ _ _
  \l C'est de ta grâ -- ce, leur sa -- lut; 
  \l _ _ _ _ _ _
}

% verseFive = \lyricmode {
%   \set stanza = "5 "
%   \l Sin -- ners, whose love can ne'er for -- get
%   \l The worm -- wood and the gall,
%   \l Go, spread your tro -- phies at his feet,
%   \l And crown him Lord of all!
%   \l Go, spread your tro -- phies at his feet,
%   \l And crown him Lord of all!
% }


% verseSix = \lyricmode {
%   \set stanza = "6 "
%   % Let ev -- 'ry kind -- red, ev -- 'ry tribe,
%   \l Bien -- tôt nous te ver -- rons au ciel
%   % On this ter -- res -- trial ball,
%   \l sur ton trône é -- ter -- nel,
%   % To him all ma -- jes -- ty as -- cribe,
%   \l mais ne vi -- vant dé -- jà qu'en toi,
%   % And crown him Lord of all!
%   \l nous te cour -- ron -- nons Roi!
%   % To him all ma -- jes -- ty as -- cribe,
%   \l mais ne vi -- vant dé -- jà qu'en toi,
%   % And crown him Lord of all!
%   \l nous te cour -- ron -- nons Roi!
% }


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    % \new Lyrics  \lyricsto soprano \verseFive
    % \new Lyrics  \lyricsto soprano \verseSix
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % Ye Gentile sinners, ne'er forget
          % The wormwood and the gall;
          % Go—spread your trophies at his feet,
          % And crown him Lord of all.
          "Le Gentil greffé à l'absinthe"
          "et le poison d'effroi,"
          "Il met son hommage à tes pieds;"
          "Nous te couronnons Roi!"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "6 "
        \column {
          % Babes, men, and sires, who know his live,
          % Who feel your sin and thrall,
          % Now joy with all the hosts above,
          % And crown him Lord of all.
          "Âgés et jeunes, nous pêcheurs,"
          "Méritant l'affreux croix,"
          "Nous fêtons avec ceux au cieux;"
          "Nous te couronnons Roi!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "7 "
        \column {
          % Let every kindred, ev'ry tribe,
          % On this terrestrial ball,
          % To him all majesty ascribe,
          % And crown him Lord of all.
          "Toute famille, toute nation"
          "claironne leur joie;"
          "Nous proclamons ta majesté;"
          "Nous te couronnons Roi!"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "8 "
        \column {
          % O that with yonder sacred throng,
          % We at his feet may fall;
          % We'll join the everlasting song,
          % And crown him Lord of all.
          "Nous voyons les archanges se"
          "prosternant devant toi!"
          "Nous nous unissons avec eux;"
          "Nous te couronnons Roi!"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g2 b4( d) |
  g,( fis) e( d) |
  e4.( fis8 g4) fis8[ e] |
  d4.( c8) b2 \bar "|" \break

  d2. d4 |
  g2 a |
  b4( d) c( b) |
  b2( a2) \bar "|" \break

  %repeat
  g2 b4( d) |
  g,( fis) e( d) |
  e4.( fis8 g4) fis8[ e] |
  d4.( c8) b2 \bar "|" \break

  d2. d4 |
  g2 a |
  b4( d) c( b) |
  b2( a2) \bar "|" \break


  a4.( b8 a4) b |
  c2 b |
  g4.( a8 g4) c4 |
  b( a) g2 |
  b4.( c8 b4) \bar "|" \break d |
  c( b) a2 

  g2. a8[ b] |
  d,2 c' |
  b a |
  g1 \bar "|."
} 

alto = \relative c' {
  \global
  d2 d |
  b4( d) c( d) |
  c4.( d8 e4) d8[ c] |
  a2 g \bar "|"

  a2. d4 |
  d2 e4( fis) |
  g( b) a( g) |
  g2( fis2) \bar "|"

  %repeat
  d2 d |
  b4( d) c( d) |
  c4.( d8 e4) d8[ c] |
  a2 g \bar "|"

  a2. d4 |
  d2 e4( fis) |
  g( b) a( g) |
  g2( fis2) \bar "|"


  fis4.( g8 fis4) d |
  e( fis) g2 |
  d( e4) e |
  g( fis) e2 |
  e2. d4 |
  fis( g) g( fis) \bar "|"

  d2. d4 |
  d2 g |
  g fis |
  s1 \bar "|."
}

tenor = \relative c' {
  \global
  b2 b4( a) |
  g( b) g8[( a] b4) |
  g2. g4 |
  fis2 g \bar "|"

  fis4( g fis) fis |
  g2 c |
  b e |
  d1 \bar "|"

  %repeat
  b2 b4( a) |
  g( b) g8[( a] b4) |
  g2. g4 |
  fis2 g \bar "|"

  fis4( g fis) fis |
  g2 c |
  b e |
  d1 \bar "|"


  d2. d4 |
  c2 d |
  b4.( a8 b4) c |
  d4.( c8) b2 |
  g4.( a8 g4) g |
  a4( b8[ c]) d2 \bar "|"

  b2. b4 |
  a2 g4( a) |
  b( c) d( c) |
  b1 \bar "|."
}

bass = \relative c' {
  \global
  g2 g4( fis) |
  e( b) c( g) |
  c2. c4 |
  d2 g, \bar "|"

  d'4( e d) c |
  b2 a |
  g c |
  d1 \bar "|"

  %repeat
  g2 g4( fis) |
  e( b) c( g) |
  c2. c4 |
  d2 g, \bar "|"

  d'4( e d) c |
  b2 a |
  g c |
  d1 \bar "|"
 

  d2. b4 |
  a2 g |
  g'4.( fis8 e4) a, |
  b8[( c] d4) e2 |
  e2. b4 |
  a( g) d'2 \bar "|"

  g2. g4 |
  fis2 e |
  d d |
  g,1 \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Lo! He comes with clouds descending,
  % Once for favour'd sinners slain!
  % Thousand, thousand saints attending,
  % Swell the triumph of his train:
  % Hallelujah,
  % God appears, on earth to reign !

  \l Voi -- ci! _ Il _ des -- _ cend par -- mi les nu -- a -- _ ges,
  \l au -- tre -- fois pour le sa -- lut oc -- cis; _
  \l mil -- liers de mil -- _ liers de saints lui rendent hom -- _ ma -- _ ge;
  \l en vic -- toire ils s'am -- _ pli -- _ fi -- ent:
  \l Al -- _ _ lé -- lu -- ia!
  \l Al -- _ _ lé -- lu -- _ ia!
  \l Al -- _ _ \l lé -- lu -- _ ia!
  \l Il re -- vient, le Sei -- gneur, le Christ.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Every eye shall now behold Him
  % Rob'd in dreadful majesty,
  % Those who set at nought and sold Him,
  % Pierc'd, and nail'd Him to the tree,
  % Deeply wailing Shall the true Messiah see.
  \l Tout œil _ doit _ main -- te -- nant _ _ le re -- gar -- _ der,
  \l re -- vê -- tu d'un gran -- _ deur ter -- ri -- ble;
  \l tous ceux _ qui _ l'ont _ à _ la croix _ clou -- _ é
  \l a -- près qu'ils _ le _ ven -- _ dir -- ent:
  \l Quel -- les la -- men -- ta -- tions!
  \l Quel -- les la -- men -- ta -- _ tions!
  \l Quel -- les la -- \l men -- ta -- _ tions!
  \l Ils ver -- _ ront le vrai Mes -- sie.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % The dear tokens of his passion
  % Still His dazling body bears,
  % Cause of endless exultation
  % To his ransom'd worshippers;
  % With what rapture
  % Gaze we on those glorious scars!
  \l Ces mar -- ques chè -- res qu'il ga -- gna pen -- dant sa _ Pas -- _ sion,
  \l par son corps é -- blo -- uis -- sant por -- tées, _
  \l sont tou -- _ jours _ sour -- ce d'ex -- _ _ ul -- _ ta -- _ tion
  \l pour cha -- cun des cro -- _ yants sau -- vés: _
  \l Quelle _ ec -- sta -- si -- e!
  \l Quelle _ ec -- sta -- si -- _ e!
  \l Quelle _ ec -- \l sta -- si -- _ e!
  \l Bel -- les ba -- la -- fres, lou -- ons -les!
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Yea, Amen! let all adore Thee,
  % High on Thine eternal throne;
  % Savior, take the power and glory,
  % Claim the kingdom for Thine own;
  % O come quickly! O come quickly!
  % O come quickly!
  % Everlasting God, come down!
  \l A -- men! _ Que _ tout _ hom -- _ me t'a -- _ do -- _ re!
  \l Viens et à ton trône _ ac -- _ cè -- de;
  \l Sau -- veur, _ prends _ le pou -- voir _ et la _ gloi -- _ re,
  \l car le Roy -- aume est _ le _ tien: _
  % Alléluia! Alléluia!
  % Alléluia!
  \l Ô, _ _ viens vi -- te!
  \l Ô, _ _ viens vi -- _ te!
  \l Ô, _ _ \l viens vi -- _ te!
  \l C'est à toi qu'ap -- par -- tient le règne.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


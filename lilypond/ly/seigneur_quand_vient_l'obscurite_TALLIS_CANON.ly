\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/2
  \partial 4*2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g2 | 
  g fis g g |
  a a b \bar "|" \break g | 
  c c b b |
  a a g \bar "|" \break d' |
  c a b b |
  a a g \bar "|" \break d |
  e fis g b |
  a a g1 \bar "|."
}

alto = \relative c' {
  \global
  d2 |
  d d b d |
  e d d g |
  e fis g d |
  e d d g |
  e d d d |
  e fis g g, |
  e' d b d |
  c a b1 |
}

tenor = \relative c' {
  \global
  b2 |
  a a g g |
  g fis g g |
  a a b g |
  c c b b |
  a a g d' |
  c a b b |
  a a g d |
  e fis g1 \bar "|."
}

bass = \relative c' {
  \global
  g2 |
  d d e b |
  c d g, b |
  a a g g' |
  g fis g g |
  a fis g g, |
  c d g, b |
  c d e b |
  c d g,1 |
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % \l All praise to thee, my God, this night
  \l Seig -- neur, quand vient l'ob -- scu -- ri -- té,
  % \l For all the bless -- ings of the light:
  \l bé -- ni sois -tu pour ta clar -- té,
  % \l Keep me, O keep me, King of Kings,
  \l et quand sur nous la nuit des -- cend,
  % \l Be -- neath thine own al -- might -- y wings.
  \l dans tes bras gar -- de tes en -- fants.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % \l For -- give me, Lord, for thy dear Son,
  \l Par -- don -- né -moi mon vil pé -- ché
  % \l The ill that I this day have done;
  \l au nom de ton Fils bien -ai -- mé,
  % \l That with the world, my -- self, and thee,
  \l a -- fin que je m'en -- dorme en paix
  % \l I, ere I sleep, at peace may be.
  \l sous ton re -- gard, ô Dieu par -- fait.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % % Teach me to live, that I may dread
  % \l Fais que je vi -- ve sain -- te -- ment,
  % % The grave as little as my bed.
  % \l a -- fin qu'à mes der -- niers mo -- ments
  % % Teach me to die that so I may
  % \l la mort me soit un gain bé -- ni,
  % % Rise glorious at the awe-full Day.
  % \l et prends -moi dans ton pa -- ra -- dis.
  \l Tu m'ap -- prends, et, Sei -- gneur, je vis:
  \l la tom -- be n'est qu'un au -- tre lit.
  \l Tu m'ap -- pel -- les, Sei -- gneur; je meurs:
  \l j'at -- ten -- drai donc mon Ré -- veil -- leur.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % \l O may my soul on thee re -- pose,
  \l Fais qu'en toi je re -- pose, ô Dieu,
  % \l And with sweet sleep mine eye -- lids close;
  \l et quand je fer -- me -- rai les yeux,
  % \l Sleep that shall me more vig -- 'rous make
  \l res -- tau -- re -moi par le som -- meil
  % \l To serve my God when I a -- wake.
  \l pour mieux te ser -- vir au ré -- veil.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "Si parfois le sommeil me fuit,"
          "en toi je cherche mon appui."
          "Seigneur, protège mon repos"
          "et garde-moi de tous les maux."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
        "Chantez à Dieu, vous ses enfants,"
        "et vous ses anges triomphants,"
        "Gloire à celui qui nous bénit,"
        "au Père, au Fils, au Saint Esprit."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

\include "../common_liturgy.ly"

% https://hymnary.org/hymn/HTLG2017/page/17

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  a2 fis4 g4 | fis4( e4) d2 | 
  fis4. g8 a4 a4 | b2 a2 | \bar "|" \break
  a2 b4 cis4 | d2 a2 |
  d4. cis8 b4 cis4 | b2 a2 | \bar "|" \break
  d2 a4 b4 | a4( g4) fis2 |
  g4. fis8 g4 a4 | \bar "|" \break
  g4( fis4) e2 |
  a2 d,4 g4 | fis2( e2) | d1 
  \bar "|."
} 

alto = \relative c' {
  \global
  d2 d4 d4 | cis2 d2 |
  d4 d4 e4 fis4 | g2 fis2 |
  e2 e4 e4 | d2 d2 |
  d4 d4 fis4 fis4 | b,4 e4 cis2 |
  d2 cis4 b4 | e2. d4 |
  d4 d4 d4 d4 | e4( d4) cis2 |
  d2 d4 e4 | e4( d2 cis4) | a1
}

tenor = \relative c {
  \global
  fis2 a4 b4 | a2 fis2 |
  b4 b4 cis4 d4 | d2 d2 |
  cis2 b4 a4 | a4 g4 fis2 |
  fis4 fis4 b4 a4 | a4 gis4 a2 |
  a2 g4 fis4 | a2 a2 |
  g4 g4 d4 d4 | b'4 a4 a2 |
  a2 b4 b4 | a2. g4 | fis1
}

bass = \relative c {
  \global
  d2 d4 g,4 | a2 d4 cis4 |
  b4 b4 a4 d4 | g2 d2 |
  a'2 g4 g4 | fis4 e4 d4 cis4 |
  b4 b4 d4 d4 | e2 a4 g4 |
  fis2 e4 d4 | cis2 d2 |
  b4 b4 b4 fis4 | g4 d'4 a4 g4 |
  fis2 b4 e,4 | fis4. g8 a2 | d1
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Father, we praise Thee, now the night is over,
  % Active and watchful, stand we all before Thee;
  % Singing, we offer prayer and meditation:
  % Thus we adore Thee.

  \l Père, on te lou -- _ e; \l la nuit se ter -- mi -- ne;
  \l nous s'as -- sem -- blons tous \l de -- vant toi, les veil -- leurs;
  \l nous of -- frons par le chant \l tou -- tes nos pri -- è -- _ res:
  \l ain -- si on t’a -- do -- re.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Monarch of all things, fit us for Thy mansions;
  % Banish our weakness, health and wholeness sending;
  % Bring us to heaven, where Thy saints united
  % Joy without ending.

  \l Roi de la cré -- a -- tion, \l qu’on soit prêt d’y en -- trer;
  \l chas -- se nos fau -- tes; \l don -- ne -nous la san -- té;
  \l ra -- mè -- ne -nous au ciel, \l où sont tes saints u -- _ nis
  \l en joie é -- ter -- nel -- le.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % All holy Father, Son and Holy Spirit,
  % Trinity blessed, send us Thy salvation;
  % Thine is the glory, gleaming and resounding
  % Through all creation.
  \l Au Père, au Fils, _ et \l à l’Es -- prit en -- sem -- ble:
  \l Tri -- ni -- té, don -- ne \l -nous la bé -- né -- dic -- tion;
  \l à toi la gloi -- _ re, \l bril -- lant et é -- cla -- _ tant
  \l en tou -- te cré -- a -- tion.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseOne
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseTwo
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


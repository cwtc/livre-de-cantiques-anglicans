\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d4 d g g |
  a a b g  \bar "|"

  d'4 d e c |
  a d b2 \bar "|"

  b4 b a b |
  g a g fis \bar "|"

  g e d g |
  g fis g2 \bar "|."
} 

alto = \relative c' {
  \global
  d4 d d d |
  fis fis g g |

  a b g g |
  g fis g2 |

  g4 g fis fis |
  e e d d |

  d c b d |
  d d d2
}

tenor = \relative c {
  \global
  d4 d b' b |
  d d d b |

  d d c e |
  d d d2 |

  e4 e c b |
  b a a a |

  g g g b |
  a a b2
}

bass = \relative c {
  \global
  d4 d b g |
  d' d g g |

  fis g c, c |
  d d g,2 |

  e'4 e e dis |
  e cis d d |

  b c g g |
  d' d g,2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Come, thou long expected Jesus,
  % Born to set thy people free;
  % From our Fears and Sins release us,
  % Let us find our rest in thee;
  \l Viens, ô Jé -- sus très at -- ten -- du,
  \l né pour af -- fran -- chir les tiens;
  \l de nos pé -- chés li -- bè -- re -nous;
  \l qu'on soit por -- té dans ton sein.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Israel's strength and consolation,
  % Hope of all the saints thou art;
  % Dear desire of ev'ry nation,
  % Joy of every longing heart.
  \l D'Is -- ra -- ël la con -- so -- la -- tion,
  \l des saints le fer -- vent es -- poir,
  \l cher dé -- sir de tou -- te na -- tion,
  \l joie ar -- den -- te de tout cœur:
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Born thy people to deliver,
  % Born a child, and yet a king;
  % Born to rain in us for ever,
  % Now thy gracious kingdom bring:
  \l Né pour dé -- li -- vrer ton peu -- ple,
  \l né en -- fant et pour -- tant roi,
  \l né pour gou -- ver -- ner pour tou -- jours:
  \l viens, Jé -- sus; ne tar -- de pas!
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % By thine own eternal spirit,
  % Rule in all our hearts alone;
  % By thine all-sufficient merit,
  % Raise us to thy glorious throne.
  \l Par ton es -- prit sé -- cu -- ri -- sant,
  \l sois no -- tre Roi et E -- poux;
  \l par ton mé -- ri -- te suf -- fi -- sant,
  \l vers ton trô -- ne lè -- ve -nous.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

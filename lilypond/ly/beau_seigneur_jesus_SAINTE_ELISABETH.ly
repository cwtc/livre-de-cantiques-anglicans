\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 f4 f |
  g( e) f2 |
  a4 a a a |
  bes( g) a2 |
  c2 f4 d |
  c2 bes4( a) |
  bes2 a |
  g1 | \break

  c2 d4 c |
  c( a) bes2 |
  bes2 c4 bes |
  bes( g) a a | \break
  a a c bes |
  a2 g2 |
  f1 \bar "|."
} 

alto = \relative c' {
  \global
  c2 d4 d |
  d( c) c2 |
  f4 f f fis |
  g( e) f2 |
  f2 f4 f |
  f2 g4( f) |
  g2 f |
  e1 |

  f2 f4 f |
  fis2 g |
  d2 d4 d |
  e2 f4 f |
  g f f f |
  f2 e |
  c1 \bar "|."
}

tenor = \relative c' {
  \global
  a2 a4 a |
  bes( g) a2 |
  c4 c d d |
  d( c) c2 |
  a2 d4 bes |
  a2 c |
  c c |
  c1 |

  c2 bes4 a |
  a( d) d2 |
  g,2 fis4 g |
  g( c) c c |
  cis d ees d |
  c2 c4( bes) |
  a1 \bar "|."
}

bass = \relative c {
  \global
  f2 d4 d |
  bes( c) f2 |
  f4 e d c |
  bes( c) f2 |
  f2 bes,4 d4 |
  f2 f |
  e f |
  c1 |

  a2 bes4 c |
  d2 g |
  g,2 a4 bes |
  c2 f4 f4 |
  e d a bes |
  c2 c |
  f1 \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Fairest Lord Jesus,
  % Ruler of all nature,
  % O Thou of God and man the Son,
  % Thee will I cherish,
  % Thee will I honor,
  % Thou, my soul's Glory, Joy, and Crown.
  \l Beau Sei -- gneur Jé -- sus,
  \l Roi de la na -- tu -- re,
  \l toi, Fils de l'Homme et Fils de Dieu:
  \l je te ché -- ri -- rai;
  \l ô, je t'ho -- no -- rai,
  \l toi, la pru -- nel -- le de mes yeux.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Fair are the meadows,
  % Fairer still the woodlands,
  % Robed in the blooming garb of spring:
  % Jesus is fairer,
  % Jesus is purer,
  % Who makes the woeful heart to sing.
  \l Beaux sont les grands prés;
  \l beaux sont les lieux boi -- sés
  \l de nou -- veau en fleur au prin -- temps;
  \l Plus bel est Jé -- sus;
  \l plus pur est Jé -- sus,
  \l qui re -- donne aux cœurs leur doux chant. 
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Fair is the sunshine,
  % Fairer still the moonlight,
  % And all the twinkling, starry host:
  % Jesus shines brighter,
  % Jesus shines purer,
  % Than all the angels heaven can boast.
  \l Bel est le so -- leil;
  \l plus belle est la lu -- ne
  \l et l'ar -- mée é -- toil -- ée des cieux;
  \l Jé -- sus ray -- on -- ne;
  \l Jé -- sus, il bril -- le
  \l plus que tous les anges en ce lieu.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

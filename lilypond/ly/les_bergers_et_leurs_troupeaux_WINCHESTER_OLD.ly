\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  f4 |
  a4. a8 g4 f |
  bes bes a g |
  a c c b c2. \bar "|" \break

  a4 |
  d4. c8 bes4 a |
  g f e a |
  g f f e |
  f2. \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  c4 |
  f4. f8 e4 d |
  d f f e |
  f e g g |
  e2.

  f4 |
  f4. f8 f4 f |
  e d cis4 c |
  e d d c |
  c2.
}

tenor = \relative c' {
  \global
  \partial 4
  a4 |
  c4. c8 c4 a |
  bes d c c |
  c c d d |
  c2.

  c4 |
  bes4. a8 bes4 c |
  c a a a |
  c a bes g |
  a2.
}

bass = \relative c {
  \global
  \partial 4
  f4 |
  f4. f8 c4 d |
  bes bes f' c4 |
  f a g g |
  c,2.

  f4 |
  bes4. f8 d4 f |
  c d a f' |
  c d bes c |
  f2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % While shepherds watched their flocks by night,
  % All seated on the ground,
  % The angel of the Lord came down,
  % And glory shone around.
  \l Les ber -- gers et leurs trou -- peaux é -- taient 
  \l aux grands champs as -- sis
  \l quand, tout res -- plen -- dis -- sant, l'an -- ge
  \l du Sei -- gneur des -- cen -- dit.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % "Fear not," said he (for mighty dread
  % Had seized their troubled mind),
  % "Glad tidings of great joy I bring
  % To you and all mankind. 
  \l “Ne crai -- gnez point, dit -il, car ils
  \l fu -- rent sai -- si d'ef -- froi;
  \l J'an -- nonce u -- ne bon -- ne nou -- vel -- le,
  \l d'u -- ne gran -- de joie.”
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % "To you, in David's town, this day,
  % Is born of David's line
  % The Savior, who is Christ, the Lord,
  % And this shall be the sign: 
  \l “C'est dans la vil -- le de Da -- vid
  \l qu'il est né un Sau -- veur;
  \l c'est au -- jour -- d'hui qu'il ap -- pa -- raît,
  \l le Christ et le Sei -- gneur.”
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % "The heavenly Babe you there shall find
  % To human view displayed,
  % All simply wrapped in swathing bands,
  % And in a manger laid." 
  \l “Et voi -- ci donc à quel si -- gne
  \l vous le re -- con -- naî -- trez:
  \l c'est à la crè -- che qu'est l'En -- fant,
  \l des langes em -- mail -- lo -- té.”
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % Thus spake the seraphs; and forthwith
          % Appeared a shining throng
          % Of angels, praising God, and thus
          % Addressed their joyful song:
          "Et soudain l'ange se joignit"
          "au bataillon brillant"
          "de toute l'armée céleste,"
          "et voici leur doux chant:"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % "All glory be to God on high,
          % And to the earth be peace;
          % Good-will, henceforth, from heaven to men
          % Begin, and never cease!" 
          "“Soit gloire à Dieu dans les hauts lieux;"
          % "et sur la terre paix;"
          "sur terre soit la paix;"
          "que cette bienveillance aux hommes"
          "ne cesse jamais!”"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

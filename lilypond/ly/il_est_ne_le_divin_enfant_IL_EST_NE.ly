\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 2/4
  \autoBeamOff
  \set Score.voltaSpannerDuration = #(ly:make-moment 4 4)
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
    d4^\markup { \italic "Refrain" } g g b8[ g] |
    d4 g g2 |
    g4 g8[ a] b4 c8[ b] |
    a4 g a a \break |
    d, g g b8[ g] |
    d4 g g2 | \noBreak
    
    g4 a b c8[ b] |
    a4 d g,2 \bar "||" | \break
  
      b4 c d c8[ b] |
      c4 e d2 |
      b4 c d e8[ d] |
      c4 b b a |
      
      b4 c d c8[ b] |
      c4 e d2 |
      b4 c d e8[ d] |
      c4 b a2 \bar "|."|
  }

alto = \relative c' {
  \global
    b4 g' e2 |
    b4 g' e2 |
    g4 g8[ a] g2 |
    e2 d2 |
    b4 g' e2 |
    b4 g' e2 |

    g4 a4 g2 |
    g4 fis4 g2
  
      g4 a b a8[ g] |
      a4 c b2 |
      g4 g g8[ fis] e4 |
      a4 g g fis4 |
      
      g4 a b a8[ g] |
      a4 c b2 |
      g4 g g8[ fis] e4 |
      e4 e g4( fis) |
    
  
}

tenor = \relative c' {
  \global
  g4 g4 c2 | b2 c2 |
  b2 b4 g4 | a2 fis2 |

  g4 g4 c2 | g2 c2 |
  b2 b4 a4 | a4 d8[ c] b2

  d2 d2 e4 e8[ fis] g2 |
  d4 c b g e' e d2 
  d2 d2 e4 e8[ fis] g2 |
  d4 c a g a a a2

}

bass = \relative c {
  \global
    d2 e2 | d2 e2 |
    g4 fis e2 | c d |
    d2 e2 | b2 e2 |
    
    g4 fis e c |
    d2 g2 |

    g2 g2 | g4 g4 g2 |
    g4 e4 b4 c4 |
    a4 cis4 d2 |
    g2 g2 | g4 g4 g2 |
    g4 e4 b4 c8[ b] |
    a[ b] c[ cis] d2 |
    
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \l Il est né le di -- vin En -- fant!
  \l Jou -- ez haut -- bois, ré -- son -- nez mu -- set -- tes!
  \l Il est né le di -- vin En -- fant!
  \l Chan -- tons tous son a -- vè -- ne -- ment.
  \break
  % \dropLyricsV
  \set stanza = "1 "
  \l De -- puis plus de qua -- tre mille ans,
  \l nous le pro -- met -- taient les pro -- phè -- tes:
  \l De -- puis plus de qua -- tre mille ans,
  \l nous at -- ten -- dions cet heu -- reux temps.
}

verseTwo = \lyricmode {
  % \dropLyricsV
  \repeat unfold 29 " "
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Ah! qu'il est beau, qu'il est char -- mant!
  \l Ah! que ses grâ -- ces sont par -- fai -- tes!
  \l Ah! qu'il est beau, qu'il est char -- mant!
  \l qu'il est doux, ce di -- vin En -- fant!
}

verseThree = \lyricmode {
  % \dropLyricsV
  \repeat unfold 29 " "
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Une é -- table est son lo -- ge -- ment,
  \l un peu de paille est sa cou -- chet -- te:
  \l Une é -- table est son lo -- ge -- ment,
  \l pour un Dieu, quel a -- bais -- se -- ment!
}

verseFour = \lyricmode {
  % \dropLyricsV
  \repeat unfold 29 ""
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Par -- tez, grands rois de l'O -- ri -- ent!
  \l Ve -- nez vous u -- nir à nos fê -- tes!
  \l Par -- tez, grands rois de l'O -- ri -- ent!
  \l Ve -- nez a -- do -- rer cet en -- fant!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "Il veut nos cœurs, il les attend:"
          "Il est là pour faire leur conquête."
          "Il veut nos cœurs, il les attend:"
          "Donnons-les lui donc promptement!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          "Ô Jésus! Ô Roi tout-puissant,"
          "Tout petit enfant que vous êtes,"
          "Ô Jésus! Ô Roi tout-puissant,"
          "Régnez sur nous entièrement!"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

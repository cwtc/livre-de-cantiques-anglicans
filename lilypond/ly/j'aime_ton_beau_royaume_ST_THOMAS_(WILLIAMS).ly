\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  c4 |
  f f a g8[ f] |
  g2. 

  a8[ bes]  | \break 
  c4 bes8[ a] bes4 a | 
  g2.

  c4 |
  a f g c, | \break
  f a8.[ bes16] c4 

  c4 |
  d c8[ bes] a4 g |
  f2. \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  c4 |
  c d c c8[ b] |
  c2.

  c4 |
  f e8[ f] d[ e] f4 |
  e2.

  g4 |
  f f e c |
  c d e

  ees |
  d f f e |
  f2. \bar "|."
}

tenor = \relative c' {
  \global
  \partial 4
  c4 |
  f, f f g |
  e2.

  c'4 |
  c c bes c |
  c2.

  c4 |
  c c c c8[ bes] |
  a4 f g

  f |
  f c'8[ d] c4. bes8 |
  a2. \bar "|."
}

bass = \relative c {
  \global
  \partial 4
  c4 |
  a bes f8[ f'] e[ d] |
  c2.

  f8[ g]  |
  a4 g8[ f] g4 f |
  c2.

  e4 |
  f a c8[ bes] a[ g]  |
  f4 d c

  a |
  bes a8[ bes] c4 c |
  f2. \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % I love Thy kingdom, Lord;
  % The house of Thine abode,
  % The Church our blest Redeemer saved
  % With His own precious blood.
  \l J'ai -- me ton beau roy -- aume
  \l et ta mai -- son, Sei -- gneur,
  \l L'E -- gli -- se sau -- vée par le sang
  \l de no -- tre Ré -- demp -- teur.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % I love Thy Church, O God!
  % Her walls before Thee stand,
  % Dear as the apple of Thine eye,
  % And graven on Thy hand.
  \l J'ai -- me l'E -- glise, ô Dieu!
  \l Ses murs sont forts et saints.
  \l C'est la pru -- nel -- le de tes yeux,
  \l tout gra -- vé sur ta main.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % For her my tears shall fall,
  % For her my prayers ascend;
  % To her my cares and toils be given,
  % Till toils and cares shall end.
  \l Je pleu -- re tout son deuil;
  \l je pri -- e pour son bien;
  \l tout mon pos -- si -- ble, je lui fais
  \l et ce jus -- qu'à la fin.
} 

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Beyond my highest joy
  % I prize her heavenly ways,
  % Her sweet communion, solemn vows,
  % Her hymns of love and praise.
  \l Plus que ma pro -- pre joie
  \l je tiens fort à ses mœurs,
  \l ses chants, sa dou -- ce com -- mun -- ion,
  \l et ses so -- lon -- nels vœux.
}
 

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          %  Jesus, Thou Friend divine,
          % Our Savior and our King,
          % Thy hand from every snare and foe
          % Shall great deliverance bring.
          "Jésus Sauveur et Roi,"
          "notre Ami divin,"
          "délivre-nous de l'ennemi"
          "et chasse nos chagrins."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Sure as Thy truth shall last,
          % To Zion shall be given
          % The brightest glories earth can yield,
          % And brighter bliss of heaven.
          "Proclamons l'Evangile:"
          "A ta sainte cité"
          "la terre entière et tous les cieux"
          "seront renouvelés."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

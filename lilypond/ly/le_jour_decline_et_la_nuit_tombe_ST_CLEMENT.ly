\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 d4 | b'4( c4) b4 | d4( b4) a4 | g4( a4) e4 | g4 fis4 \bar "|" \break
  e4 | d2 d4 | g4( a4) b4 | a2 g4 | fis2 \bar "|" \break
  d4 | b'4( c4) b4 | d4( b4) a4 | g4( a4) e4 | g4 fis4 \bar "|" \break
  e4 | d4( e4) fis4 | g4( b4) a4 | e4( g4) fis4 | g2.
  \bar "|."
} 

alto = \relative c' {
  \global
  d4 | d4( e4) d4 | fis2 fis4 | e2 e4 | c4 c4
  c4 | c4( b4) d4 | e2 d4 | d4( cis4) cis4 | d2
  d4 | d4( e4) d4 | fis2 fis4 | e2 e4 | c4 c4
  a4 | d2 c4 | b4( d4) e4 | e2 e4 | d2.
}

tenor = \relative c {
  \global
  d4 | g2 g4 | a4( d4) c4 | b4( c4) a4 | a4 a4 g4 |
  a4( b4) a4 | b2 g4 | e2 a4 | a2 a4|
  g2 g4 | a4( d4) c4 | b4( c4) a4 | a4 a4 fis4 |
  g2 a4 | g2 c4 | c2 a4 | b2.
}

bass = \relative c {
  \global
  d4 | g,2 g'4 | d2 d4 | e4( a,4) c4 | d4 d4 e4 |
  fis4( g4) fis4 | e2 e4 | a,2 a4 | d2 fis4 |
  g2 g4 | d2 d4 | e4( a,4) c4 | d4 d4 c4 |
  b2 d4 | e4( b4) c4 | c4( a4) d4 | g,2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % The day Thou gavest, Lord, is ended,
  % The darkness falls at Thy behest;
  % To Thee our morning hymns ascended,
  % Thy praise shall hallow now our rest.

  % \l Le don de ta jour -- née, Sei -- gneur, se ter -- mi -- ne,
  % \l Le don de ce beau jour, Sei -- gneur, se ter -- mi -- ne; 
  % \l Le don de ce jour, Sei -- gneur, se ter -- mi -- ne;
  \l Le jour dé -- cline, et la nuit tom -- be
  % \l la nuit tom -- be se -- lon ta vo -- _ lon -- té;
  \l se -- lon ta vo -- lon -- té, Sei -- gneur;
  % \l vers toi sont mon -- té ce ma -- tin _ nos hym -- nes;
  % \l Le ma -- tin chan -- tions nous tes hym -- nes;
  \l Au ma -- tin chan -- tons tes lou -- an -- ges;
  % \l tes lou -- an -- ges sont un re -- pos san -- cti -- fié.
  \l au soir, re -- po -- sons -nous sans peur.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % We thank Thee that Thy Church unsleeping,
  % While earth rolls onward into light,
  % Through all the world her watch is keeping,
  % And rests not now by day or night.

  % \l On te rend grâ -- ce pour ton é -- glise é -- veil -- lée
  % \l quand la Ter -- re vers la lu -- mi -- è -- re tourne;
  % \l a -- vec vi -- gi -- lance, el -- le fait tou -- jours le guet;
  % \l ni par jour, ni par nuit, _ el -- le ne dort.

  \l Nous ren -- dons grâce a -- vec l'é -- gli -- se
  \l par -- tout sur la Ter -- re tour -- nante; 
  \l El -- le fait la veille, in -- sou -- mi -- se,
  \l en pous -- sant quel -- que part son chant.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % As o'er each continent and island
  % The dawn leads on another day,
  % The voice of prayer is never silent,
  % Nor dies the strain of praise away.

  % % \l Comme au _ -des -- sus _ des îles et con -- ti -- nents
  % \l Comme au -des -- sus des î -- les et des con -- ti -- nents,
  % % \l nous mè -- ne l’au -- ro -- _ re jus -- _ qu’au jour,
  % \l l'au -- ro -- re nous mè -- ne jus -- qu'au nou -- veau jour, 
  % % \l la voix des pri -- ères n’est ja -- mais en si -- len -- ce;
  % \l ain -- si est la voix de tous nos sup -- pli -- ca -- tions:
  % \l Tes lou -- an -- ges ne ver -- ront ja -- mais la mort.

\l Comme à chaque î -- le de la Ter -- re
\l la nou -- velle au -- be vient tou -- jours,
\l ain -- si se pous -- sent ses pri -- è -- res:
\l son los ne voit ja -- mais la mort.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % The sun that bids us rest is waking
  % Our brethren 'neath the western sky,
  % And hour by hour fresh lips are making
  % Thy wondrous doings heard on high.
  \l Le so -- leil part en -- fin ré -- veil -- ler 
  \l nos frè -- res vi -- vant dans l'Ou -- est, 
  \l heure a -- près heu -- re, pour re -- lay -- er
  \l ta bon -- té dans les lieux cé -- lestes.

}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  % So be it, Lord; Thy throne shall never,
  % Like earth's proud empires, pass away;
  % Thy Kingdom stands, and grows forever
  % Till all Thy creatures own Thy sway.

  % \l Qu’ain -- si soit -il, Sei -- gneur, que ton trô -- ne du -- re,
  % \l quand les em -- pi -- res ter -- res -- tres pas -- ser -- ont:
  % \l ton roy -- au -- me res -- te et croî -- tra pour tou -- jours,
  % \l jus -- qu’à la ré -- demp -- tion de ta cré -- a -- tion.

  \l Ton ro -- yaume é -- ter -- nel grand -- i -- ra;
  \l les rois ter -- res -- tres pas -- ser -- ront:
  \l Ta sai -- nte gloi -- re  re -- ten -- ti -- ra,
  \l chan -- té -- e par ta cré -- a -- tion.

}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

% \markup { \vspace #0.5 } 

% \markup {
%   \fill-line {
%     \hspace #0.1 % moves the column off the left margin;
%      % can be removed if space on the page is tight
%      \column {
%       \line { \bold "5 "
%         \column {
%           % So be it, Lord; Thy throne shall never,
%           % Like earth's proud empires, pass away;
%           % Thy Kingdom stands, and grows forever
%           % Till all Thy creatures own Thy sway.
%         }
%       }
%     }
%     \hspace #0.1 % adds horizontal spacing between columns;
%   \hspace #0.1 % gives some extra space on the right margin;
%   % can be removed if page space is tight
%   }
% }


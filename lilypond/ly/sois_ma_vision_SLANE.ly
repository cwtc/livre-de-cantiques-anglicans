\include "../common_liturgy.ly"

global = {
  \key ees \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  ees4 ees f8[ ees] |
  c4 bes bes8[ c] |
  ees4 ees f |
  g2. | \break

  f4 f f |
  f g bes |
  c bes g |
  bes2. | \break

  c4 c8[ d] ees[ d] |
  c4 bes g |
  bes ees, d |
  c2( bes4) | \break
  
  ees4 g bes |
  c8[ bes] g4 ees8[ g] |
  f4 ees ees |
  ees2. \bar "|."
} 

alto = \relative c' {
  \global
  bes4 bes c |
  g bes bes8[ c] |
  c4 c ees |
  ees2. |

  bes4 ees d |
  c d ees |
  ees f ees |
  ees( d g) |

  aes4 aes g |
  g2 d4 |
  d c bes |
  c2( bes4) |

  c4 ees d |
  ees bes4 bes |
  c c c |
  bes2.
}

tenor = \relative c' {
  \global
  g4 g aes |
  ees f g |
  g g c |
  bes2. |
  
  f4 a bes |
  f bes g |
  g f bes |
  bes2( d4) |
  
  c4 c c |
  g d' bes |
  g g f |
  g( aes f) |
  
  g ees f |
  ees ees ees |
  aes g f |
  g2.
}

bass = \relative c {
  \global
  ees4 ees aes, |
  c d ees |
  c bes aes |
  ees'2.
  
  d4 c bes |
  a g ees' |
  c d ees |
  bes2( bes'4) |
  
  aes4 f c8[ d] |
  ees4 g g |
  bes, c d |
  ees( f d) |
  
  c4 c bes8[ aes] |
  g4 ees g |
  aes8[ bes] c4 aes |
  ees'2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Be thou my vision, O Lord of my heart;
  % naught be all else to me, save that thou art.
  % Thou my best thought, by day or by night,
  % waking or sleeping, thy presence my light.
  % \l Qu'en toi je vive, Seigneur bien-aimé,
  % \l Ô source vive de félicité,
  % \l Qu'à toi je pense, le jour et la nuit,
  % \l car ta clémence toujours me bénit.
  \l Sois ma vi -- sion, ô Sei -- gneur de mon cœur;
  \l car mon gain est per -- du sans mon Sau -- veur.
  \l Qu'à toi je pen -- se, le jour et la nuit;
  \l ta pré -- sence est ma Lu -- miè -- re qui luit.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Be thou my wisdom, be thou my true word;
  % I ever with thee, and thou with me, Lord.
  % Born of thy love, thy child may I be,
  % thou in me dwelling and I one with thee.
  \l Sois ma sa -- ges -- se, mes mots sur mes lèvres,
  \l pré -- sent quand je dors et quand je me lève;
  \l Fais -moi ton en -- fant, né de ton a -- mour;
  \l fais ton ta -- ber -- nacle en moi tous les jours.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Be thou my buckler, my sword for the fight.
  % Be thou my dignity, thou my delight,
  % thou my soul’s shelter, thou my high tow’r.
  % Raise thou me heav’nward, O Pow’r of my pow’r.
  \l Sois ma cui -- ras -- se, sois mon bou -- cli -- er;
  \l sois ma dé -- li -- ce; sois ma di -- gni -- té:
  \l ma for -- te -- res -- se, l'a -- bri de mon âme!
  \l Que ton Saint -Es -- prit pour tou -- jours m'en -- flamme.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Riches I heed not, nor vain empty praise;
  % thou mine inheritance, now and always.
  % Thou and thou only, first in my heart,
  % Ruler of heaven, my treasure thou art.
  \l Tou -- te ri -- ches -- se ne com -- pte pour rien:
  \l mon seul hé -- ri -- tage est l'a -- mour di -- vin!
  \l Roi de mon cœur est le Sei -- gneur, mon Dieu;
  \l mon tré -- sor qui rè -- gne sur tous les cieux.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  % True Light of heaven, when vict’ry is won
  % may I reach heaven’s joys, O bright heav’n’s Sun!
  % Heart of my heart, whatever befall,
  % still be my vision, O Ruler of all.
  \l Lu -- mière é -- clai -- rant tou -- tes les na -- tions,
  \l So -- leil de jus -- ti -- ce, ta com -- pas -- sion
  \l tous les jours de ma vi -- e me sui -- vra:
  \l Sois donc ma vi -- sion, mon Sei -- gneur, mon Roi.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\include "../common_liturgy.ly"

% https://github.com/bbloomf/christmas-carols/blob/master/ly/8.5garamond/012-Adeste%20Fideles.ly

global = {
  \key a \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4 \teeny a4 | \normalsize
  a2 e4 a |
  b2 e, |
  cis'4 b cis d |
  \tieDashed\slurDashed cis4.~ cis8 b4 \bar "|" \break
  \teeny a | \normalsize
  a2 gis4( fis) |
  
  \tieSolid\slurSolid
  gis( a) \slurDashed b( cis) |
  \slurSolid gis2( fis4.) e8 |
  e2. b'4\rest |
  e2 d4 cis |
  \slurDashed d2( cis) |
  b4 cis a b |
  \slurSolid \partial 4*3 gis4.( fis8) e4 | \break
  
  \partial 4 a4 |
  a gis a b |
  a2 e4 cis'4 |
  cis b cis d |
  cis2 b4 \bar"|"\break cis |
  
  d cis b a |
  gis2 a4( d) |
  cis2( b4.) a8 |
  \partial 4*3 a2. \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4 \teeny e4 | \normalsize
  e2 e4 e |
  e2 e |
  e4 e e fis |
  \tieDashed\slurDashed e4.~ e8 e4 \teeny cis | \normalsize
  \slurSolid cis( dis) \slurDashed e( dis) |
  
  \tieSolid\slurSolid e( dis8[) cis] \slurDashed b4( e) |
  \slurSolid e2( dis4.) e8 |
  e2. s4 |
  e2 fis8[ gis] a4 |
  \slurSolid a( \slurDashed gis)( a2) |
  e4 e fis fis |
  \slurSolid \partial 4*3 e2 e4 |
  
  e4 |
  e1~ |
  e2 e4 e |
  e e e e |
  e2 e4 a |
  
  gis a e e8[ dis] |
  e2 e4( fis) |
  e2( d4.) cis8 |
  \partial 4*3 cis2. \bar "|."
}

tenor = \relative c' {
  \global
  \partial 4 \teeny cis4 | \normalsize
  cis2 cis4 cis |
  b2 b |
  a4 b a a |
  \tieDashed\slurDashed a4.~ a8 gis4 \teeny a | \normalsize
  a2 b4~ b |
  
  \tieSolid\slurSolid b( a) \slurDashed e'( cis) |
  \slurSolid b2( a4.) gis8 |
  gis2. d4\rest |
  cis'2 d4 e |
  \slurDashed e2( e2) |
  e4 a, cis d |
  \slurSolid \partial 4*3 b4.( a8) gis4 |
  
  \partial 4 cis4 |
  cis b cis d |
  cis2. a4 |
  a gis a b |
  a2 gis4 e' |
  
  e e b b |
  b2 a |
  a( gis4.) a8 |
  \partial 4*3 a2. \bar "|."
}

bass = \relative c' {
  \global
  \partial 4 \teeny a4 | \normalsize
  a2 a4 a |
  gis2 gis |
  a4 gis a d, |
  \tieDashed\slurDashed e4.~ e8 e4 \teeny fis | \normalsize
  fis2 e4( b) |
  
  \tieSolid\slurSolid e( cis) \slurDashed gis( a) |
  b2~ b4. e8 |
  \tieSolid e2. s4 |
  cis'2 b4 a |
  b2( a2) |
  gis4 a fis d |
  \partial 4*3 e2 e4 |
  
  \slurSolid \partial 4 d\rest |
  g,1\rest |
  g1\rest |
  g1\rest |
  g2\rest f4\rest a'4 |
  
  b a gis fis |
  e( d) cis( d) |
  e2~e4. a,8 |
  \partial 4*3 a2. \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O come, all ye faithful,
  % joyful and triumphant,
  % O come ye, O come ye, to Bethlehem;
  % come and behold him,
  % born the King of angels;
  % O come, let us adore him,
  % O come, let us adore him,
  % O come, let us adore him,
  % Christ the Lord.

  \l Ô peu -- ple fi -- dè -- le,
  % \l Jésus vous appelle!
  \l joy -- eux et tri -- om -- _ phant,
  \l Ve -- nez à Beth -- lé -- em, ve -- nez en _ ces lieux!
  \l Peu -- ple fi -- dè -- le,
  % \l Ve -- nez le re -- gar -- der,
  \l ve -- nez voir le Roi des cieux!
  % \l lui né Roi des an -- ges!
  \l Que votre a -- mour l'im -- plo -- re,
  \l que vo -- tre foi l'a -- do -- re,
  \l et qu'el -- le chante en -- co -- re
  \l ce don pré -- ci -- eux!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % God from God,
  % Light from Light eternal,
  % lo! he abhors not the Virgin's womb;
  % only begotten Son of the Father;
  % O come, let us adore him,
  % O come, let us adore him,
  % O come, let us adore him,
  % Christ the Lord.
  \l " " \l Dieu de __ _ Dieu,
  \l Lu -- miè -- re de Lu -- miè -- _ re,
  \l _ il n'ab -- _ hor -- re pas le sein de la Vierge.
  \l Seul Fils de Dieu, né a -- vant tous les siè -- _ cles:
  _ _ _ _ _ _ _
  _ _ _ _ _ _ _
  \l et qu'ils _ chantent en -- co -- re,
  \l le ciel et la terre!
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Sing, choirs of angels,
  % sing in exultation;
  % sing, all ye citizens of heaven above;
  % glory to God, glory in the highest;
  % O come, let us adore him,
  % O come, let us adore him,
  % O come, let us adore him,
  % Christ the Lord.
  \l " "  \l Chan -- tez, les an -- ges,
  \l vo -- tre ex -- ul -- ta -- _ tion,
  \l _ chan -- tez, ô chan -- tez, vous ci -- toy -- ens cé -- lestes:
  \l Gloire _ à Dieu, _ au plus haut des cieux! _ _
  _ _ _ _ _ _ _
  _ _ _ _ _ _ _
  \l et qu'el -- le chante en -- co -- re
  \l nos joies __ _ ter -- restres!
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
    % See how the shepherds,
    % summoned to his cradle,
    % leaving their flocks, draw nigh to gaze;
    % we too will thither bend our joyful footsteps;
    % O come, let us adore him,
    % O come, let us adore him,
    % O come, let us adore him,
    % Christ the Lord.

    \l Dans une humble é -- ta -- ble
    \l froide et mi -- sé -- ra -- _ ble,
    \l des ber -- gers rem -- plis d’a -- mour lui for -- ment sa cour!
    \l Dans cette é -- ta -- ble, ac -- cou -- rez à vo -- tre tour!
    _ _ _ _ _ _ _
    _ _ _ _ _ _ _
    et qu’el -- le chante en -- co -- re
    sa gloire en ce jour!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % Child, for us sinners
          % poor and in the manger,
          % we would embrace thee, with love and awe;
          % who would not love thee, loving us so dearly?
          % O come, let us adore him,
          % O come, let us adore him,
          % O come, let us adore him,
          % Christ the Lord.
          "Troublant de mystère,"
          "dans la crèche austère"
          "est l’enfant qui reçoit notre adoration."
          "Nous l'aimons parce qu'il nous aima le premier!"
          "Que votre amour l'implore,"
          "que votre foi l'adore,"
          "et qu’elle chante encore"
          "les jours annoncés!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Yea, Lord, we greet thee,
          % born this happy morning,
          % Jesus, to thee be glory given;
          % Word of the Father, now in flesh appearing;
          % O come, let us adore him,
          % O come, let us adore him,
          % O come, let us adore him,
          % Christ the Lord.
          "Saluons le Seigneur,"
          "né ce matin joyeux;"
          "Jésus, le Christ, que nous te glorifions."
          "Verbe de Dieu, Parole du Père!"
          "Que votre amour l’implore,"
          "que votre foi l’adore,"
          "et qu’elle chante encore"
          "qu’il vienne en chair!"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

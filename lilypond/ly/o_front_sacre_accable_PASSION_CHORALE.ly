\include "../common_liturgy.ly"

% TO CONSIDER:
% http://www.hymntime.com/tch/non/fr/r/c/o/u/rcouvert.htm

global = {
  \key a \minor
  \time 4/2
  \partial 2*1
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  e2 |
  a g f e |
  d1 e2 b' |
  c c b4( a) b2 |
  \slurDotted
  a1( a2) \bar "|" \break % end line 1
  \slurSolid
  e2 \bar "|" |
  a g f e |
  d1 e2 b' |
  c c b4( a) b2 |
  \slurDotted
  a1( a2) \bar "|" \break % end line 2
  \slurSolid
  c2 \bar "|"|
  b4( a) g2 a b |
  c1 c2 g |
  a g f f |
  \slurDotted
  e1( e2) \bar "|" \break% end line 3
  c'!2 \bar "|"|
  \slurSolid
  b4( c) d2 c b |
  a1 b2 e, |
  f e d g |
  \slurDotted
  e1( e2) \bar "|."
}

alto = \relative c' {
  \global
  c2 |
  c c c4( d) d( c) |
  c2( b) c d |
  c4( d) e2 e2. d4 |
  \slurDotted
  c1( c2) % end line 1
  \slurSolid
  c2 |
  c c c4( d) d( c) |
  c2( b) c d |
  c4( d) e2 e2. d4 |
  \slurDotted
  c1( c2) % end line 2
  \slurSolid
  e2 |
  d4( f) e( d) c2 f |
  f2( e4 d) e2 e |
  f e e d |
  \slurDotted
  cis1( cis2) % end line 3
  d2 |
  \slurSolid
  d d e4( fis) g2 |
  g( fis) g c,4( b) |
  a( b) c2 c4( a) b2 |
  \slurDotted
  c1( c2) \bar "|."
}

tenor = \relative c' {
  \global
  g2 |
  f2 g a4( g) g2 |
  a( g) g gis |
  a a a gis |
  \slurDotted
  a1( a2) % end line 1
  \slurSolid
  g2 |
  f2 g a4( g) g2 |
  a( g) g gis |
  a a a gis |
  \slurDotted
  a1( a2) % end line 2
  \slurSolid
  a4( g) |
  f( a) c( b) a( g) f( g) |
  a2( g4 f) g2 c |
  c bes a a |
  \slurDotted
  a1( a2) % end line 3
  a=2 |
  \slurSolid
  g a g4( a) b( a) |
  e'2( d) d g, |
  f g g g |
  \slurDotted
  g1( g2) \bar "|."
}

bass = \relative c {
  \global
  c2 |
  f e a,4( b) c2 |
  f,( g) c b |
  a4( b) c( d) e2 e, |
  \slurDotted
  a1( a2) % end line 1
  \slurSolid
  c2 |
  f e a,4( b) c2 |
  f,( g) c b |
  a4( b) c( d) e2 e, |
  \slurDotted
  s1( a2) % end line 2
  \slurSolid
  a2 |
  d e f4( e) d2 |
  c1 c2 c |
  f4( e) d(cis) d( e) f( g) |
  \slurDotted
  s1( a2) % end line 3
  fis2 |
  g fis e d |
  \slurSolid
 cis2( d) g, c= |
  d e4( f) g2 g, |
  \slurDotted
  c1( c2) \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Theology...

  % % \l O sa -- cred head, sore wound -- ed,
  % \l Chef cou -- vert de bles -- sur -- es,
  % % \l De -- filed and put to scorn,
  % CL: tout meur -- tri, tout san -- glant!
  % % \l mour -- ant pour nous, pé -- cheurs,
  % % \l O king -- ly head, sur -- round -- ed
  % CL: Chef ac -- ca -- blé d'in -- ju -- res
  % % \l Tu souf -- fres sans mur -- mur -- e 
  % % \l Ton front royal, 
  % % \l With mock -- ing crown of thorn;
  % % CL: d'op -- pro -- bre, de tour -- ment!
  % % \l la honte et la dou -- leur.
  % % \l What sor -- row mars thy grand -- eur?
  % CL: Toi, des splen -- deurs di -- vi -- nes 
  % % \l De la splen -- deur di -- vi -- ne
  % % \l Can death thy bloom de -- flow'r?
  % CL: au -- tre -- fois cour -- ron -- né,
  % % \l plus rien ne reste en toi,
  % % \l O coun -- te -- nance whose splen -- dor
  % CL: c'est main -- te -- nant d'é -- pi -- nes
  % % \l Roi cour -- on -- né d'é -- pi -- nes,
  % % \l The hosts of heav'n a -- dore!
  % CL: que ton front est or -- né.
  % % \l clou -- é sur u -- ne croix!
  \l Ô front sac-- ré, ac -- ca -- blé
  \l d'in -- jure et d'in -- _ jus -- ti -- ce!
  \l Ô front roy -- al en -- tou -- ré
  \l d'u -- ne cou -- ron -- ne d'é -- pi -- ne!
  \l Pré -- ser -- _ vée est ta gran -- deur,
  \l mal -- gré cet -- te souf -- fran -- ce,
  \l Ô tê -- _ te dont la splen -- deur
  \l char -- me l'ar -- mée des an -- ges!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  % Revise this theology!

  \set stanza = "2 "
  % % \l Thy beau -- ty, long de -- sir --ed,
  % \l Tu dois quit -- ter la vi -- e,
  % % \l Hath van -- ished from our sight;
  % \l de tous a -- ban -- don -- né;
  % % \l Thy pow'r is all ex -- pir -- ed,
  % \l Quand vient ton a -- go -- ni -- é,
  % % \l And quench'd the light of light;
  % \l Dieu sem -- ble s'é -- loig -- ner;
  % % \l Ah me! for whom thou di -- est,
  % \l Mais au mo -- ment su -- prê -- me,
  % % \l Hide not so far thy grace:
  % \l vers lui je -- tant un cri,
  % % \l Show me, O Love most high -- est,
  % \l tu veux, dans ses mains mê -- mes,
  % % \l The bright -- ness of thy face.
  % \l re -- met -- tre ton e -- sprit.
  \l Ta beau -- té très dé -- si -- ré
  \l a quit -- té no -- _ tre vu -- e.
  \l Ton pou -- voir a ex -- pi -- ré,
  \l Lu -- miè -- re dis -- _ pa -- ru -- e.
  \l Ne ca -- _ che pas ta grâ -- ce
  \l de moi pour qui tu meurs! __ _ 
  \l Montre en _ a -- mour ta fa -- ce,
  \l que je voie sa lu -- eur. __ _
}

verseThree = \lyricmode {
  % Needs rewrite 

  \set stanza = "3 "
  \set ignoreMelismata = ##t
  % % In thy most bitter passion
  % \l Dois -tu vrai -- ment con -- naî -- tre
  % % My heart to share doth cry.
  % \l le fond de la dou -- leur,
  % % With thee for my salvation
  % \l toi qui ve -- nais pour ê -- tre
  % % Upon the cross to die.
  % \l le grand li -- bér -- a -- teur?
  % % Ah, keep my heart thus moved
  % \l Tu veux por -- ter nos pein -- es,
  % % To stand thy cross beneath,
  % \l souf -- frir jusqu' -- à la mort,
  % % To mourn thee, well-beloved,
  % \l ré -- pon -- "dre à" tant de hai -- ne
  % % Yet thank thee for thy death.
  % \l par plus d'a -- mour en -- core.
  \l De ta Pas -- sion a -- mè -- re 
  \l mon cœur veut par --  _ ta -- ger. __ _
  \l A -- vec toi il es -- pè -- re
  \l être à la croix _ clou -- é. __ _
  \l Que ce _ cœur se di -- spo -- se
  \l à res -- ter à tes pieds __ _
  \l En la -- _ men -- tant, mais j'o -- se
  \l aus -- si te re -- mer -- cier. __ _
}

verseFour = \lyricmode {
  % Needs rewrite

  \set stanza = "4 "
  \set ignoreMelismata = ##t
  % % What language shall I borrow
  % \l Mais ton cru -- el mar -- ty -- re
  % % To thank thee, dearest friend,
  % \l pré -- pare un jour nou -- veau:
  % % For this thy dying sorrow,
  % \l ta gloire et ton em -- pi -- re
  % % Thy pity without end?
  % \l au -- près du Dieu très -haut.
  % % Oh, make me thine forever!
  % \l Vers toi, dans la lu -- miè -- re,
  % % And should I fainting be,
  % \l tu veux nous at -- ti -- rer.
  % % Lord, let me never, never
  % \l Tu veux con -- duire au Pè -- re
  % % Outlive my love for thee.
  % \l tous ceux qu'il t'a don -- nés.
  \l Donc main -- ten -- ant je chan -- te
  \l ce mer -- ci, mon _ a -- mi, __ _
  \l Pour ta dou -- leur mou -- ran -- te,
  \l Ton a -- mour in -- _ fi -- ni! __ _
  \l Fais -- moi _ le tien pour tou -- jours!
  \l Et si je suis ten -- té, __ _
  \l Seig -- neur, _ que, par ton a -- mour, 
  \l je n'y quit -- te ja -- mais. __ _
}

verseFive = \lyricmode {
  % Needs rewrite

  \set stanza = "5 "
  \set ignoreMelismata = ##t
  % My days are few, O fail not,
  %   With thine immortal pow'r,
  %   To hold me that I quail not
  %   In death's most fearful hour:
  %   That I may fight befriended,
  %   And see in my last strife
  %   To me thine arms extended
  %   Upon the cross of life.
  \l Mes jours sont courts et comp -- tés,
  \l donc viens à mon _ se -- cours, __ _
  \l que mon deuil soit sur -- mon -- té
  \l cet af -- freux mor -- _ tel jour. __ _
  % \l Qu'à ma bataille dernière mes renforts soient en toi;"
  \l Qu'à ma _ der -- niè -- re guer -- re, 
  \l mes ren -- forts soient en toi; __ _
  \l que tes _ bras soient ou -- verts, vers 
  \l moi de ta sain -- te croix. __ _
}



\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}



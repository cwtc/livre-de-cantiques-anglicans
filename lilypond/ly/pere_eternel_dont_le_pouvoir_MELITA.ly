\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/D-H/Eternal%20Father,%20Strong%20to%20Save%20(MELITA).ly

global = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  c4 |
  e4. e8 g4 g4 |
  a4 a g \bar "|" %\break

  g |
  c d b g |
  g fis g \bar "|" \break

  d |
  f4. f8 e4 e |
  g4. g8 fis4 \bar "|" %\break
  
    b |
  g fis e a |
  g fis e \bar "|" %\break
  
    e |
  e4. e8 f4 f |
  fis4. fis8 g4 \bar "|"
  
    g |
  g a g e |
  d4. c8 c4 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  c4 |
  c4. c8 e4 e |
  f f e
  
    g |
  g4. fis8 g4. e8 |
  d4 d d 
  
    b |
  d4. d8 c4 c |
  e4. e8 d4 
  
    fis |
  e4. dis8 e4. fis8 |
  e4 dis e 
  
    b |
  as4. as8 a4 c |
  c4. c8 b4 
  
    f'! |
  e d e c |
  b4. c8 c4 \bar "|."
}

tenor = \relative c {
  \global
  \partial 4
  e4 |
  g4. g8 c4. b8 |
  a8[ b] c[ d] e4

  d |
  c a b d8[ c] |
  a4 a b 
  
    g |
  g4. g8 g4 g |
  a4. a8 a4 
  
    b |
  b a b c |
  b b g 
  
    g |
  g4. g8 f4 a |
  a4. a8 g4 
  
    d' |
  c c c g |
  f4. e8 e4 \bar "|."
}

bass = \relative c {
  \global
  \partial 4
  c4 |
  c4. c8 c4 c4 |
  f8[ g] a[ b] c4 
    
    b |
  a d, g b,8[ c] |
  d4 d g 
    
    g |
  b,4. b8 c4 c |
  cis4. cis8 d4 
  
    dis |
  e fis g a |
  b b e, 
  
    e |
  c4. c8 f4 f |
  d4. d8 g4 
  
    b, |
  c fis, g g |
  g4. c8 c4 \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Père é -- ter -- nel, dont le pou -- voir
  \l se plaît à sau -- ver, tu fais voir
  \l leurs li -- mi -- tes aux vas -- tes mers
  \l et mets un frein aux flots a -- mers:
  \l Vois nos pleurs, en -- tends nos san -- glots
  \l pour ceux en pé -- ril sur les flots. 
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Ô Christ, dont les eaux au -- tre -- fois
  \l ou -- ï -- rent la puis -- san -- te voix,
  \l qui sur les on -- des as mar -- ché,
  \l dans l'ou -- ra -- gan res -- tas cou -- ché:
  \l Vois nos pleurs, en -- tends nos san -- glots
  \l pour ceux en pé -- ril sur les flots.  
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Toi qui, dans le temps du cha -- os,
  \l en pla -- nant au -des -- sus des eaux,
  \l Es -- prit Saint, cou -- vas l'u -- ni -- vers
  \l et peu -- plas la terre et les mers:
  \l Vois nos pleurs, en -- tends nos san -- glots
  \l pour ceux en pé -- ril sur les flots. 
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Ô Tri -- ni -- té, puis -- sance, a -- mour,
  \l gar -- de nos frè -- res nuit et jour;
  \l et si du feu, des en -- ne -- mis
  \l loin du dan -- ger tu les as mis:
  \l Des lou -- an -- ges, non des san -- glots
  \l mon -- tent de la terre et des flots.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

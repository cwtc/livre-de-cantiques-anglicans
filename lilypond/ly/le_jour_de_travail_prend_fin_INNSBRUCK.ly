\include "../common_liturgy.ly"

% http://www.geoffhorton.com/hymns/indexbytitle.html

global = {
  \key f \major
  \time 4/2
  \partial 2*1
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  a2 |
  f g a4( bes) c2 |
  bes1 a2 \bar "||" a4( bes) |
  c2 c g a | \break
  f1 e2 \bar "||" c |
  f g a a |
  g1. \bar "||" a2 |
  f g a4( bes) c2 | \bar "|" \break
  bes1 a2 \bar "||" a4( bes) |
  c2 c g a |
  f1 e2 | \bar "||" \break c |
  f g a bes |
  a g f1 \bar "|."
} 

alto = \relative c' {
  \global
  f2 |
  d e f4( g) a2 |
  a( g) f f |
  f4( e) f( d) e( d) c2 |
  c( b) c g |
  c bes a4( bes) c2 |
  c1. e2 |
  d c c d |
  d4( c d e) fis2 f |
  g f4( e) d2 e |
  e( d) cis c |
  c4( d) d( bes) a( c) d( e) |
  f2 e2 c1
}

tenor = \relative c' {
  \global
  c2 |
  bes bes c4( bes) a( g) |
  f2( c') c d |
  c c c e, |
  f4( e) f2 g e4( d) |
  c2 d4( e) f2 f |
  e1. a2 |
  a4( bes) a( g) f( g) a2 |
  g4( a bes c) d2 d |
  c4( bes) a2 b e, |
  a1 a2 a |
  a4( bes) a( g) f2 f |
  d' g,4( a8[ bes]) a1 \bar "|."
}

bass = \relative c {
  \global
  f2 |
  bes4( a) g2 f2. e4 |
  d2( e) f d |
  a4( g) a( bes) c( bes) a2 |
  d1 c2 bes |
  a g f4( g) a( bes) |
  c1. cis2 |
  d e f fis |
  g( g,) d' d |
  e f f4( e) d( cis) |
  d( e f g) a2 a, |
  d e f4( e) d( c) |
  bes2 c f1
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % The duteous day now closeth,
  % Each flower and tree reposeth,
  % Shade creeps o’er wild and wood:
  % Let us, as night is falling,
  % On God our Maker calling,
  % Give thanks to him, the Giver good.

  \l Le jour de tra -- _ vail prend fin,
  \l s'en -- _ dor -- mant jus -- qu’à \l de -- main;
  \l l’om -- bre voit le ter -- rain:
  \l Pen -- dant la tom -- bée de \l la nuit,
  \l que _ l'on fasse ap -- pel sur lui; 
  \l lou -- ons le Cré -- a -- teur di -- vin.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Now all the heavenly splendor
  % Breaks forth in starlight tender
  % From myriad worlds unknown;
  % And man, the marvel seeing,
  % Forgets his selfish being,
  % For joy of beauty not his own.

  \l La glo -- ri -- eu -- _ se splen -- deur,
  % \l bril _ -- lent des é -- toi -- les ten -- dres
  \l bril -- _ lante é -- toi -- le \l ten -- dre,
  \l ve -- nant du ciel qui luit;
  \l et l’Hom -- me, voy -- ant la \l mer -- veille,
  \l ou -- bli -- e son é -- tat tom -- bé
  \l dû à la beau -- té pas à lui.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % His care he drowneth yonder,
  % Lost in the abyss of wonder;
  % To heaven his soul doth steal:
  % This life he disesteemeth,
  % The day it is that dreameth,
  % That doth from truth his vision seal.
  
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Awhile his mortal blindness
  % May miss God’s loving-kindness,
  % And grope in faithless strife:
  % But when life’s day is over
  % Shall death’s fair night discover
  % The fields of everlasting life.

  \l Bien que no -- tre _ cé -- ci -- té
  \l ait _ l’a -- mour de Dieu \l man -- qué,
  \l donc on est en que -- relle;
  \l quand no -- tre jour -- née fi -- \l ni -- ra,
  \l la _ nuit mor -- tel -- le ver -- ra
  \l les champs de la vie é -- ter -- nelle.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

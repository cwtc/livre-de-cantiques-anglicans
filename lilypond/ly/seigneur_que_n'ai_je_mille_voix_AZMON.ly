\include "../common_liturgy.ly"
% https://topmusic.topchretien.com/chant/charles-wesley-seigneur-que-nai-je-mille-voix/
% https://hymnary.org/hymn/GH1924/page/21

% TO ADAPT (fill)

global = {
  \key a \major
  \time 3/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 2 e2 | a4 a4 b2 b2 | cis4 b4 a2 b2 |
  cis4 cis4 d2 cis2 | b1 \bar "|" \break
  e2 | cis4 cis4 cis2 a2 | a4 fis4 fis2 a4( fis4) |
  e4 a4 a2 b2 | a1.
  \bar "|." 
} 

alto = \relative c' {
  \global
  e2 | e4 cis4 e2 e2 | e4 d4 cis2 e2 | e4 e4 e2 e2 | e1
  gis2 | a4 e4 e2 e2 | fis4 d4 d2 (fis4 d4) | 
  cis4 cis4 cis2 d2 | cis2.
}

tenor = \relative c {
  \global
  e2 | e4 a4 gis2 gis2 | a4 gis4 a2 gis2 | a4 a4 b2 a2 | gis1
  b2 | cis4 a4 a2 a2 | a4 a4 a2 a2 | 
  a4 a4 a2 gis2 | a1.
}

bass = \relative c {
  \global
  e2 | cis4 a4 d2 d2 | a4 e4 fis2 e2 | a4 a4 gis2 a2 | e1
  e2 | a4 a4 a2 cis2 | d4 d4 d2 d2 | e4 e4 d2 d2 | a1.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O for a thousand Tongues to sing,
  % My great Redeemer's Praise;
  % The Glories of my God and King,
  % The Triumphs of his Grace!
  \l Sei -- gneur, que n'ai -je mil -- le voix
  \l pour chan -- ter tes lou -- anges,
  \l et faire mon -- ter jus -- qu'aux an -- ges
  \l les gloi -- res de ta croix!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % My gracious Master and my God,
  % Assist me to proclaim,
  % To spread thro' all the Earth abroad,
  % The Honours of thy Name.
  \l Jé -- sus, mon Sei -- gneur et mon Dieu,
  \l que ton souf -- fle m'a -- nime,
  \l pour que par toi, ton nom su -- blime
  \l re -- ten -- tisse en tout lieu!
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Jesus, the Name that charms our Fears,
  % That bids our Sorrows cease;
  % 'Tis Musick in the Sinner's Ears,
  % 'Tis Life, and Health, and Peace.
  \l Doux Nom qui fait ta -- rir nos pleurs,
  \l in -- ef -- fable har -- mo -- nie,
  \l tu ré -- pands la joie et la vie
  \l et la pa -- ix dans nos cœurs!
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % He breaks the Power of cancel'd Sin,
  % He sets the Pris'ners free;
  % His Blood can make the Foulest clean;
  % His Blood avail'd for me.
  \l Dé -- sor -- mais, je n'ai plus d'ef -- froi;
  \l au -- cun mal ne m'ac -- cable;
  \l ton sang rend pur le plus cou -- pable,
  \l ton sang cou -- la pour moi!
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  % He speaks, and, list'ning to his Voice,
  % New Life the Dead receive;
  % The mournful broken Hearts rejoice,
  % The humble Poor believe.
  \l Il parle, et, en -- ten -- dant sa voix,
  \l se ra -- vi -- vent les morts;
  \l le pauvre et hum -- ble hom -- me croit;
  \l l'af -- fli -- gé de -- vient fort.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "6 "
        \column {
          % Hear him, ye Deaf; his Praise ye Dumb,
          % Your loosen'd Tongues employ;
          % Ye Blind your Saviour's come,
          % And leap, ye Lame for Joy. 
          "Le sourd l'entend; le muet parle"
          "en chantant à son Roi!"
          "L'aveugle voit son cher Sauveur;"
          "Le boiteux saute en joie!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "7 "
        \column {
          % Glory to God, and praise, and love,
          % Be ever, ever given;
          % By saints below and saints above,
          % The Church in earth and heaven.
          "Chantons maintenant, comme il faut:"
          "Soit grande gloire à Dieu!"
          "Des saints en haut, des saints en bas,"
          "la Terre comme les Cieux."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

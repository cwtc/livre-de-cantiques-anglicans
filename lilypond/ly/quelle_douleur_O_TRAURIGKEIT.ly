\include "../common_liturgy.ly"

% https://hymnary.org/tune/o_traurigkeit
% https://books.google.ca/books?id=k44ZjxB0oeAC&pg=PA96&lpg=PA96&dq=%22quelle+douleur%22+o+traurigkeit&source=bl&ots=75zrog2kD0&sig=ACfU3U3UntpWEgaiZ0NO5fLBdTMPV9sxoQ&hl=en&sa=X&ved=2ahUKEwj6itzj_b_zAhUKrHIEHVTUDksQ6AF6BAgCEAM#v=onepage&q=%22quelle%20douleur%22%20o%20traurigkeit&f=false

global = {
  \key bes \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4 d4 | bes4 g4 a2 |
  b4\rest 
  fis4 g4 g4 | fis2 b4\rest \bar %"|" \break
  d4 | d4 d4 ees4 d4 | c2 bes2 | \break
  a4 bes4 c4 d4 | c4 bes4 a2 |
  g4 a4 bes4 c4 | a2 g2 
  \bar "|."
} 

alto = \relative c'' {
  \global
  a4 | g4 g4 fis2 | 
  s4 d4 bes4 c4 | d2 s4 
  f4 | f4 f4 g4 f4 | f2 d2 |
  fis4 g4 a4 bes4 | a4 g4 fis2 |
  g4 fis4 g4 g4 | g4( fis4) g2
}

tenor = \relative c' {
  \global
  d4 | d4 c4 a2 | s4 
  a4 g4 g4 | a2 s4 
  bes4 | bes4 bes4 bes4 bes4 | bes( a4) bes2 |
  d4 d4 f4 f4 | f4 d4 d2 |
  bes4 d4 d4 ees4 | d2 bes2
}

bass = \relative c {
  \global
  fis4 | g4 ees4 d2 | d4\rest d4 ees4 ees4 | d2 d4\rest 
  bes4 | bes4 bes4 ees4 bes4 | f2 bes2 |
  d4 g4 f4 bes,4 | f'4 g4 d2 |
  ees4 d4 g4 c,4 | d2 g,2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Quel -- le dou -- leur
  \l sai -- sit mon cœur!
  \l A peu près j'y suc -- com -- be.
  \l Le Fils de Dieu, mon Sau -- veur,
  \l est mis dans la Tom -- be.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l É -- tran -- ge sort!
  \l Dieu même est mort;
  \l il laisse en croix la vi -- e.
  \l Mais par ce su -- prême ef -- fort,
  \l ils nous vi -- vi -- fi -- e.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Hom -- me pé -- cheur,
  \l le sé -- duc -- teur 
  \l t'en -- traî -- nait dans l'a -- bî -- me:
  \l Pour t'en ti -- rer, le Sau -- veur
  \l ex -- pire en vic -- ti -- me.
  % Whoa: original says
  % "ton méchant coeur te plongait dans l'abîme,"
  % "pour t'en tirer le Sauveur devient TA victime."
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Ce tendre É -- poux, % Cet Agneau doux
  \l ver -- sant pour nous
  \l tout le sang de ses vei -- nes,
  \l cal -- me le ciel en cour -- roux % du ciel les courroux
  \l et fi -- nit nos pei -- nes.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Con -- tem -- plons tous
  \l cet A -- gneau doux
  \l of -- fert en sa -- cri -- fi -- ce:
  \l Voy -- ons im -- mo -- lé pour nous
  \l le Sau -- veur pro -- pi -- ce.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

% \markup { \vspace #0.5 } 

% \markup {
%   \fill-line {
%     \hspace #0.1 % moves the column off the left margin;
%      % can be removed if space on the page is tight
%      \column {
%       \line { \bold "5 "
%         \column {
%           "Contemplons tous cet Agneau doux"
%           "offert en sacrifice:"
%           "Voyons immolé pour nous"
%           "le Sauveur propice."
%         }
%       }
%     }
%     \hspace #0.1 % adds horizontal spacing between columns;
%   \hspace #0.1 % gives some extra space on the right margin;
%   % can be removed if page space is tight
%   }
% }

% % Cantiques spirituels Strasbourg 1758

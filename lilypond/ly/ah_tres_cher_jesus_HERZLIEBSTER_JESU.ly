\include "../common_liturgy.ly"

global = {
  \key g \minor
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g2 g4 g4 | 
  f2 d2 |
  g4 a bes bes |
  c2 a \bar "|" \break
  
  a bes4 c |
  d2 bes | 
  ees4 ees d d |
  c2 bes2 \bar "|" \break
  
  bes a4 g |
  f2 d4.( ees8) |
  f4 f g f \bar "|" \break 
  ees2 d \bar "|" 
   
  d' c4 bes |
  a2 g \bar "|."
}

alto = \relative c' {
  \global
  d2 ees4 ees |
  c2 d2 |
  d4 d g f |
  ees2 d |
  
  d d4 f |
  f2 g2 |
  g4 g f f |
  f2 d |
  
  g f4 ees |
  d2 bes |
  d4 c d d |
  c4( bes) a2 |
  
  d ees4 d |
  d4.( c8) bes2 |
}

tenor = \relative c' {
  \global
  bes2 bes4 c |
  a2 f |
  bes4 a g bes |
  g2 fis |
  
  fis g4 a |
  bes2 g2 |
  c4 g8[ a] bes4 bes |
  bes( a) bes2 |
  
  d d4 bes |
  bes2 f |
  f4 a bes bes |
  g2 fis |
  
  f4( bes) g g |
  g( fis) g2 \bar "|."
}

bass = \relative c' {
  \global
  g2 ees4 c |
  f2 bes, |
  g'4 f ees d |
  c2 d |
  
  d g4 f |
  bes,2 ees |
  c4 c d4. ees8 |
  f2 bes, |
  
  g d'4 ees |
  bes2 bes |
  bes4 a g bes |
  c2 d |
  
  bes c4 g |
  d'2 g, |
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % % \l Ah, ho -- ly Je -- sus, how hast Thou of -- fen -- ded,
  % \l Pour quel pé -- ché, Jé -- sus, pour quelle of -- fen -- se
  % % \l That man to judge Thee hath in hate pre -- ten -- ded?
  % \l a -t-on sur toi pro -- non -- cé la sen -- ten -- ce?
  % % \l By foes de -- rid -- ed, by Thine own re -- jec -- ted
  % \l Ah! qu'as -tu fait, in -- no -- cen -- te vic -- tim -- e?
  % % \l O most af -- flic -- ted.
  % \l Quel est ton cri -- me?
  \l Ah, très cher Jé -- sus, quelle est ta trans -- gres -- sion,
  \l que l'on te pu -- nit a -- vec tant d'op -- pres -- sion?
  \l Où est le tort? Quel -- les fautes as -tu com -- mi -- ses
  \l qu'ils in -- ter -- dis -- ent?
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % % \l Who was the guil -- ty? Who brought this up -- on Thee?
  % \l Qui peut t'a -- voir at -- ti -- ré le sup -- pli -- ce?
  % % \l A -- las, my trea -- son, Je -- sus, hath un -- done Thee.
  % \l C'est moi, Seig -- neur! Oui, c'est mon in -- ju -- sti -- ce!
  % % \l ’Twas I, Lord, Je -- sus, I it was de -- nied Thee!
  % \l De ces tour -- ments où ton a -- mour t'ex -- po -- se,
  % % \l I cru -- ci -- fied Thee.
  % \l je suis la cau -- se.
  \l Qui est cou -- pa -- ble? Quelle est donc la rai -- son?
  \l Ah, très cher Jé -- sus, c'é -- tait ma tra -- hi -- son!
  \l Moi, mon Seig -- neur, c'é -- _ tait moi qui t'ai ni -- é,
  \l et cru -- ci -- fi -- é.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  \set ignoreMelismata = ##t
  % \l Lo, the Good Shep -- herd for the sheep is of -- fered;
  % \l The slave hath sin -- ned, and the Son hath suf -- fered;
  % \l For man’s a -- tone -- ment, while he noth -- ing heed -- eth,
  % \l God in -- ter -- ced -- eth.
  \l Qu'il est né -- an -- moins é -- tran -- ge, ce con -- flit:
  \l Le Bon Pas -- teur, il souf -- fre pour ses bre -- bis.
  \l La det -- te pay -- ée, ex -- pi -- é par le Seig -- neur
  \l pour ses ser -- vi -- teurs!
}

verseFour = \lyricmode {
  \set stanza = "4 "
  \set ignoreMelismata = ##t
  % \l For me, kind Je -- sus, was Thy in -- car -- na -- tion,
  % \l Thy mor -- tal sor -- row, and Thy life’s ob -- la -- tion;
  % \l Thy death of an -- guish and Thy bit -- ter pas -- sion,
  % \l For my sal -- va -- tion.
  \l L'a -- mour qui dé -- pas -- se la com -- pré -- hen -- sion,
  \l il t'a me -- né sur ce che -- min de Pas -- sion.
  \l C'é -- tait pour mon sa -- _ lut, ton in -- car -- na -- tion
  \l et ton o -- bla -- tion.
}

verseFive = \lyricmode {
  \set stanza = "5 "
  % \set ignoreMelismata = ##t
  % \l There -- fore, kind Je -- sus, since I can -- not pay Thee,
  % \l I do a -- dore Thee, and will ev -- er pray Thee,
  % \l Think on Thy pi -- ty and Thy love un -- swerv -- ing,
  % \l Not my de -- serv -- ing.
  \l Donc, très cher Jé -- sus, tou -- jours je te pri -- e;
  \l je te vé -- nère et je te re -- mer -- ci -- e
  \l pour ta mi -- sé -- ri -- corde in -- dé -- fec -- ti -- ble:
  \l J'en suis in -- di -- gne.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseOne
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseTwo
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseThree
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseFour
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


% \markup { \vspace #0.5 } 

% \markup {
%   \fill-line {
%     \hspace #0.1 % moves the column off the left margin;
%      % can be removed if space on the page is tight
%      \column {
%       \line { \bold "5 "
%         \column {
%           %   \l There -- fore, kind Je -- sus, since I can -- not pay Thee,
%           %   \l I do a -- dore Thee, and will ev -- er pray Thee,
%           %   \l Think on Thy pi -- ty and Thy love un -- swerv -- ing,
%           %   \l Not my de -- serv -- ing.
%           "Donc, très cher Jésus, toujours je te prie."
%           "Je te vénère et je te remercie"
%           "Pour ta miséricorde indéfectible:"
%           "J'en suis indigne."
%         }
%       }
%     }
%     \hspace #0.1 % adds horizontal spacing between columns;
%   \hspace #0.1 % gives some extra space on the right margin;
%   % can be removed if page space is tight
%   }
% }


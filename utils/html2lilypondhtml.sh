#!/usr/bin/env bash

in_html_dir='lilypond/html'
out_html_dir='lilypond/out_html'

mkdir -p ${out_html_dir}

for filename in ${in_html_dir}/*.html; do
	lilypond-book --pdf --output ${out_html_dir} ${filename}
done

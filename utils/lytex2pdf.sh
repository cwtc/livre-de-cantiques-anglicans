#!/usr/bin/env bash

in_lytex_dir='lilypond/web_lytex'
out_pdf_dir='lilypond/web_pdf'

start_dir=$PWD

mkdir -p ${out_pdf_dir}

cp lilypond/Baskerville.ttf ${in_lytex_dir}/Baskerville.ttf
# cp lilypond/Baskerville.ttf ${in_lytex_dir}/Baskerville.ttf
# cp lilypond/Baskerville.ttf ${in_lytex_dir}/Baskerville.ttf
# cp lilypond/Baskerville.ttf ${in_lytex_dir}/Baskerville.ttf

for filename in ${in_lytex_dir}/*.lytex; do
	lilypond-book --output ${out_pdf_dir} ${filename}
	tex="$(basename ${filename} | sed 's/.lytex/.tex/g')"
	# sleep 2
	# echo "Compiling with xelatex..."
	cd ${out_pdf_dir}
	echo $PWD
	echo ${tex}
	# # echo ${out_pdf_dir}/${tex}
	xelatex ${tex}
	# cd ${start_dir}
done

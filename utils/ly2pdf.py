import glob
import os
import pdfCropMargins
import shutil

if __name__ == '__main__':
    in_ly_dir = os.path.join('lilypond', 'ly')
    out_lytex_dir = os.path.join('lilypond', 'web_lytex')
    out_pdf_dir = os.path.join('lilypond', 'web_pdf')

    if not os.path.exists(out_lytex_dir):
        os.makedirs(out_lytex_dir)
    if not os.path.exists(out_pdf_dir):
        os.makedirs(out_pdf_dir)

    shutil.copyfile(os.path.join('lilypond', 'Baskerville.ttf'), os.path.join(out_lytex_dir, 'Baskerville.ttf'))

    # For every input ly, write an output lytex
    for ly_path in glob.glob(os.path.join(in_ly_dir, '*.ly')):
        lytex_lines = []
        lytex_lines.append('\\documentclass{book}')
        # lytex_lines.append('\\def\\preLilyPondExample{{\\hspace*{{-3mm}}}}')
        # lytex_lines.append('\\newcommand{{\\betweenLilyPondSystem}[1]{{\\vspace{{1mm}}\\linebreak}}')
        # lytex_lines.append('\\renewcommand{{\\betweenLilyPondSystem}[1]{{\\linebreak\\hspace*{{-3mm}}}}')
        lytex_lines.append('\\usepackage{geometry}')
        lytex_lines.append('\\geometry{paperheight=6in,paperwidth=6in,margin=0.5in}')
        # lytex_lines.append('\\usepackage[no-math]{fontspec}')
        # lytex_lines.append('\\setmainfont[Path=../, UprightFont=Baskerville.ttf, BoldFont=Baskerville-Bold.ttf,  ItalicFont=Baskerville-Italic.ttf, Ligatures=TeX]{Baskerville}')
        lytex_lines.append('\\begin{document}')
        lytex_lines.append('\\pagenumbering{gobble}')
        lytex_lines.append('\\lilypondfile[staffsize=16,line-width=4.83\\in]{{{0}}}'.format(ly_path))
        lytex_lines.append('\\end{document}')
        basename = os.path.basename(ly_path).replace('.ly', '.lytex')
        lytex_path = os.path.join(out_lytex_dir, basename)

        with open(lytex_path, 'w') as fp:
            for line in lytex_lines:
                fp.write('{0}\n'.format(line))
        # break

    # For every lytex, render a pdf
    start_dir = os.getcwd()
    for lytex_path in glob.glob(os.path.join(out_lytex_dir, '*.lytex')):
    # for lytex_path in glob.glob(os.path.join(out_lytex_dir, '*CHRISTE_SANCTORUM.lytex')):
        # Pass through lilypond-book
        os.system('lilypond-book --output {0} {1}'.format(out_pdf_dir, lytex_path.replace('\\', '/')))

        # Render output tex to get pdf
        os.chdir(out_pdf_dir)
        tex_path = os.path.basename(lytex_path).replace('.lytex', '.tex')
        os.system('xelatex {0}'.format(tex_path))
        os.chdir(start_dir)

        # Copy pdf to site assets
        pdf_path = os.path.join(out_pdf_dir, tex_path.replace('.tex', '.pdf'))
        copy_pdf_path = os.path.join('docs', 'assets', 'pdf', os.path.basename(pdf_path))
        shutil.copyfile(pdf_path, os.path.join('docs', 'assets', 'pdf', os.path.basename(pdf_path)))

        # Crop
        pdfCropMargins.crop(['-v', '-s', '-u', '-o', copy_pdf_path.replace('.pdf', '.cropped.pdf'), copy_pdf_path])
        shutil.move(copy_pdf_path.replace('.pdf', '.cropped.pdf'), copy_pdf_path)


---
tags:
  - Dedication d'une église · Dedication of a church
---

# J'aime ton beau royaume



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/j'aime_ton_beau_royaume_ST_THOMAS_(WILLIAMS).pdf"}},
            	metaData:{fileName: "j'aime_ton_beau_royaume_ST_THOMAS_(WILLIAMS).pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> J'aime ton beau royaume<br>et ta maison, Seigneur,<br>L'Eglise sauvée par le sang<br>de notre Rédempteur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> J'aime l'Eglise, ô Dieu!<br>Ses murs sont forts et saints.<br>C'est la prunelle de tes yeux,<br>tout gravé sur ta main. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Je pleure tout son deuil;<br>je prie pour son bien;<br>tout mon possible, je lui fais<br>et ce jusqu'à la fin. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Plus que ma propre joie<br>je tiens fort à ses mœurs,<br>ses chants, sa douce communion,<br>et ses solonnels vœux. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Jésus Sauveur et Roi,<br>notre Ami divin,<br>délivre-nous de l'ennemi<br>et chasse nos chagrins. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Proclamons l'Evangile:<br>A ta sainte cité<br>la terre entière et tous les cieux<br>seront renouvelés. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | I love thy kingdom, Lord |
| Texte · Text  | Timothy Dwight (1800)<br/>tr. Arlie Coles (2021) |
| Musique · Music | *St Thomas (Williams)* <br/> Aaron Williams (1763) |
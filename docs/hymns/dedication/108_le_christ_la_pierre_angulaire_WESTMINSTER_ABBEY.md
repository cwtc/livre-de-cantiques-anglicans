---
tags:
  - Dedication d'une église · Dedication of a church
---

# Le Christ, la pierre angulaire



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/le_christ_la_pierre_angulaire_WESTMINSTER_ABBEY.pdf"}},
            	metaData:{fileName: "le_christ_la_pierre_angulaire_WESTMINSTER_ABBEY.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Le Christ, la pierre angulaire,<br>il est le ferme soutien<br>des murs précieux qu'on vénère<br>de Sion, ce lieu très saint;<br>lui, l'inébranlable asile<br>du fidèle chrétien. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Cette cité, dans son zèle,<br>a consacré tous ses soins<br>à la gloire immortelle<br>du vrai Dieu qui la défend;<br>elle chante en allégresse<br>de son pouvoir triomphant. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Toi, objet de nos louanges,<br>viens et descends donc du ciel<br>régner parmi tous tes anges<br>sur chaque sacré autel;<br>toi, le flambeau salutaire,<br>viens luire aux faibles mortels. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Que l'on, par nos pures offrandes,<br>puisse attirer les effets,<br>sur nos très humbles demandes.<br>de ton immense bonté;<br>qu'on voie en nous l'héritage<br>de chacun de tes bienfaits. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Angularis fundamentum |
| Texte · Text  | Latin, VI siècle<br/>tr. en français J. Racine (1659), *Bréviaire de Le Tourneux*<br/>ad. Arlie Coles (2021) |
| Musique · Music | *Westminster Abbey* <br/> Henry Purcell (1680) |
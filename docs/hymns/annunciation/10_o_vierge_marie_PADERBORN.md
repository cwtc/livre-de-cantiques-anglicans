---
tags:
  - Annonciation · Annunciation
  - Vierge Marie · Virgin Mary
---

# O Vièrge Marie



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/o_vierge_marie_PADERBORN.pdf"}},
            	metaData:{fileName: "o_vierge_marie_PADERBORN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ô Vierge Marie, nos voix et nos cœurs<br>voudraient de ta vie chanter les grandeurs.<br>À toute la terre, ta nativité<br>présage une autre ère de félicité. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Emue et ravie de vant Gabriel,<br>accepte, ô Marie, les ordres du Ciel.<br>Pour sauver la terre du joug de Satan,<br>oui, tu seras Mère du Dieu ToutPuissant. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Bientôt dans l'étable naît sur tes genoux<br>le Verbe adorable fait Homme pour nous.<br>Oh! Qui pourra dire l'amour de ton Fils?<br>À ton doux empire un Dieu s'est soumis. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Jésus te couronne de sa majesté;<br>prends place à son trône pour l'éternité.<br>Et nous de ta vie, chantant les grandeurs,<br>t'offrons, ô Marie, nos voix et nos cœurs. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | *Psallam Deo*, Schola Cantorum de Montréal (1922) |
| Musique · Music | *Paderborn* <br/> *Paderborn Gesangbuch* (1765) |
---
tags:
  - Louange · Praise
---

# Qu'au saint Nom de Jésus



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/qu'au_saint_nom_de_jesus_KINGS_WESTON.pdf"}},
            	metaData:{fileName: "qu'au_saint_nom_de_jesus_KINGS_WESTON.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Qu'au saint Nom de Jésus,<br>tout genou fléchisse,<br>toute langue entonne<br>qu'il est bien le Christ:<br>Il est Roi de gloire,<br>notre vrai Seigneur;<br>l'éternel Verbe<br>du Père créateur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> De cette Parole<br>tout était créé:<br>les séraphins brillants,<br>la céleste armée,<br>les Trônes, Autorités,<br>Pouvoirs ordonnés,<br>les étoiles qui sont<br>par sa main rangées. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Devenant serviteur,<br>il s'est dépouillé;<br>comme simple homme il<br>s'est humilié:<br>Le Christ s'est rendu pour<br>nous obéissant,<br>jusqu'à la mort, même<br>la mort sur la croix. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Donc Dieu l'a élevé,<br>le victorieux,<br>son corps et son saint Nom<br>même jusqu'aux cieux:<br>au-dessus des anges,<br>au plus grande hauteur,<br>assis à la droite<br>du Père en splendeur. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Que l'on l'intronise Roi de tous nos cœurs,<br>afin qu'il maîtrise nos désirs pécheurs:<br>Notre Capitaine à notre tentation!<br>Que sa volonté soit la libération! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Et ce Seigneur Jésus, il retournera:<br>dans la gloire immense, il nous jugera;<br>et son règne parfait n'aura pas de fin:<br>C'est le Roi de gloire; louons son Nom saint! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | At the name of Jesus |
| Texte · Text  | Caroline M. Noel (1870);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *King's Weston* <br/> Ralph Vaughan Williams (1925) |
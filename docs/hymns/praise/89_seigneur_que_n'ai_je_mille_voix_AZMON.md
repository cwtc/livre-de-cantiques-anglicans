---
tags:
  - Louange · Praise
---

# Seigneur, que n'ai-je mille voix



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/seigneur_que_n'ai_je_mille_voix_AZMON.pdf"}},
            	metaData:{fileName: "seigneur_que_n'ai_je_mille_voix_AZMON.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Seigneur, que n'ai-je mille voix<br>pour chanter tes louanges,<br>et faire monter jusqu'aux anges<br>les gloires de ta croix! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Jésus, mon Seigneur et mon Dieu,<br>que ton souffle m'anime,<br>pour que par toi, ton nom sublime<br>retentisse en tout lieu! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Doux Nom qui fait tarir nos pleurs,<br>ineffable harmonie,<br>tu répands la joie et la vie<br>et la paix dans nos cœurs! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Désormais, je n'ai plus d'effroi;<br>aucun mal ne m'accable;<br>ton sang rend pur le plus coupable,<br>ton sang coula pour moi! </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Le sourd l'entend; le muet parle<br>en chantant à son Roi!<br>L'aveugle voit son cher Sauveur;<br>Le boiteux saute en joie! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Chantons maintenant, comme il faut:<br>Soit grande gloire à Dieu!<br>Des saints en haut, des saints en bas,<br>la Terre comme les Cieux. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O for a thousand tongues to sing |
| Texte · Text  | Charles Wesley (1739);<br/>tr. Ruben Sailens (1855-1942), str. 1-4<br/>tr. Arlie Coles (2022), str. 5-6 |
| Musique · Music | *Azmon* <br/> C. G. Gläser (1928);<br/>ad. Lowell Mason (1792-1872) |
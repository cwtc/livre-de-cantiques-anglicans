---
tags:
  - Louange · Praise
---

# O Dieu, notre aide aux temps anciens



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/o_dieu_notre_aide_aux_temps_anciens_SAINT_ANNE.pdf"}},
            	metaData:{fileName: "o_dieu_notre_aide_aux_temps_anciens_SAINT_ANNE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ô Dieu, notre aide aux temps anciens,<br>espoir des temps nouveaux,<br>Aux jours mauvais, puissant soutien,<br>notre éternel repos. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> À l'ombre de ton trône, assis,<br>tes saints ont habité.<br>Ton bras vailant nous garde aussi<br>en toute sûreté. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Quand les collines et les flots<br>se confondaient mêlés,<br>Seul tu régnais sur le chaos<br>de toute éternité. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Mille ans, Seigneur, sont à tes yeux<br>plus brefs qu'un soir enfui,<br>Plus brefs que l'aube dans les cieux<br>lorsque prend fin la nuit. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Le temps comme un torrent fougueux<br>emporte ses enfants.<br>Ils passent, comme un rêve heureux<br>s'envoie au jour naissant. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Ô Dieu notre aide aux temps anciens,<br>espoir des temps nouveaux,<br>De toujours notre gardien,<br>notre éternel repos. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O God, our help in ages past |
| Texte · Text  | Isaac Watts (1674-1748);<br/>tr. Pauline Martin (1950), ad. Arlie Coles (2021), str. 6 |
| Musique · Music | *Saint Anne* <br/> William Croft (1678-1727) |
---
tags:
  - Louange · Praise
---

# Louons le Dieu puissant



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/louons_le_dieu_puissant_LOBE_DEN_HERREN.pdf"}},
            	metaData:{fileName: "louons_le_dieu_puissant_LOBE_DEN_HERREN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Louez le Dieu puissant, vrai Seigneur, vrai Roi du monde!<br>C'est Dieu qui fit le ciel radieux, la terre et l'onde.<br>Orgues, chantez!<br>Cors et trompettes, sonnez:<br>Et que les anges répondent! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Louez le Dieu qui sur toute la création règne!<br>Qu'il nous cache à l'ombre de ses ailes et nous soutienne!<br>N'as-tu pas vu<br>que tes désirs sont rendus<br>complétés s'il intervienne? </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Louez le Dieu qui vient nous bénir et nous défendre,<br>qui fait sur nous ses dons, ses faveur sans fin descendre!<br>Et songez tous<br>à ce que Dieu fait pour vous!<br>Sourds qui passez sans l'entendre! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Honneur et gloire à toi, Roi du Ciel; le ciel t'adore!<br>Et l'univers entier, prosterné, t'acclame encore!<br>L'astre qui luit<br>chante ton Nom à la nuit;<br>la nuit le dit à l'aurore! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Lobe den Herren, den mächtigen König der Ehren |
| Texte · Text  | Joachim Neander (1680);<br/>tr. en anglais Catherine Winkworth (1863);<br/>tr. en français Augustin Mahot (date inconnue), str. 1-2, 4;<br/>tr. en français Arlie Coles (2021), str. 3 |
| Musique · Music | *Lobe den Herren* <br/> air allemand (1665) |
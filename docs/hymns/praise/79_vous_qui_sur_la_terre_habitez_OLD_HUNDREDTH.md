---
tags:
  - Louange · Praise
---

# Vous qui sur la terre habitez



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/vous_qui_sur_la_terre_habitez_OLD_HUNDREDTH.pdf"}},
            	metaData:{fileName: "vous_qui_sur_la_terre_habitez_OLD_HUNDREDTH.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Vous, qui sur la terre habitez,<br>Chantez à haute voix, chantez;<br>Réjouissezvous au Seigneur,<br>Par un saint hymne à son honneur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Sachez qu'il est le Souverain,<br>Qu'il nous a formés de sa main,<br>Nous, le peuple qu'il veut chérir,<br>Et le troupeau qu'il veut nourrir. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Entrez dans son temple aujourd'hui;<br>Prosternezvous tous devant Lui;<br>Et, de concert avec les cieux,<br>Célébrez son Nom glorieux. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> C'est un Dieu rempli de bonté,<br>D'une éternelle vérité,<br>Toujours propice à nos souhaits,<br>Et sa grâce dure à jamais. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td>  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Théodore de Bèze (1619-1605) |
| Musique · Music | *Old Hundredth* <br/> Louis Bourgeois (1551) |
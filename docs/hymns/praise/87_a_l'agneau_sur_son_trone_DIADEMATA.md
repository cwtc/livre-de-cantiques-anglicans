---
tags:
  - Louange · Praise
---

# A l'Agneau sur son trône



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/a_l'agneau_sur_son_trone_DIADEMATA.pdf"}},
            	metaData:{fileName: "a_l'agneau_sur_son_trone_DIADEMATA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> À l'Agneau sur son trône,<br>apportons la couronne!<br>Il l'a conquise sur la croix;<br>il est le Roi des rois!<br>Eveille toi, mon âme!<br>Bénis, adore, acclame,<br>avec tous les anges du ciel,<br>Jésus, Emmanuel! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> À l'Agneau sur son trône,<br>l'encens et la couronne!<br>Car il est le Verbe incarné;<br>d'une vierge, il est né!<br>Ô Sagesse profonde!<br>Le Créateur du monde,<br>pour vaincre le mal triomphant<br>s'est fait petit enfant! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Il eut la croix pour trône,<br>l'épine pour couronne.<br>Mais le Père a glorifié<br>son Fils crucifié!<br>Au Prince de la Vie,<br>la mort est asservie!<br>Hors de la tombe il est monté:<br>Christ est ressucité! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> À l'Agneau sur son trône,<br>la palme et la couronne!<br>Car il est le Prince de Paix;<br>il règne désormais!<br>Les fureurs de la guerre<br>s'éteindront sur la terre,<br>où renaîtront, comme jadis,<br>les fleurs du Paradis! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Crown him with many crowns |
| Texte · Text  | Matthew Bridges (1851);<br/>tr. Ruben Sailens (1855-1942) |
| Musique · Music | *Diademata* <br/> George J. Elvey (1868) |
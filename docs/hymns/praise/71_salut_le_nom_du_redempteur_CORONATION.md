---
tags:
  - Louange · Praise
---

# Salut, le Nom du Redempteur



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/salut_le_nom_du_redempteur_CORONATION.pdf"}},
            	metaData:{fileName: "salut_le_nom_du_redempteur_CORONATION.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Salut, le Nom du Rédempteur!<br>Le ciel loue leur Roi;<br>La diadème, c'est à toi;<br>La diadème, c'est à toi; </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Le sang des martyrs de l'autel<br>t'appelle par leur foi;<br>Ils t'ont suivi dans leur douleur;<br>nous te couronnons Roi!<br>Ils t'ont suivi dans leur douleur;<br>nous te couronnons Roi! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Salut, héritier de David<br>que David aperçoit;<br>Il t'appella Seigneur divin;<br>Il t'appella Seigneur divin; </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Le reste faible d'Israël,<br>libéré de la Loi,<br>C'est de ta grâce, leur salut;<br>C'est de ta grâce, leur salut; </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Le Gentil greffé à l'absinthe<br>et le poison d'effroi,<br>Il met son hommage à tes pieds;<br>Nous te couronnons Roi! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Âgés et jeunes, nous pêcheurs,<br>Méritant l'affreux croix,<br>Nous fêtons avec ceux au cieux;<br>Nous te couronnons Roi! </td>
	</tr>
	<tr>
	<td> 7 </td>
	<td> Toute famille, toute nation<br>claironne leur joie;<br>Nous proclamons ta majesté;<br>Nous te couronnons Roi! </td>
	</tr>
	<tr>
	<td> 8 </td>
	<td> Nous voyons les archanges se<br>prosternant devant toi!<br>Nous nous unissons avec eux;<br>Nous te couronnons Roi! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | All hail the power of Jesus' Name |
| Texte · Text  | Edward Perronet (1726-1792);<br/>tr. Charles Rochedieu (1857-1928), ad. Arlie Coles (2021), str. 1;<br/>tr. Arlie Coles (2021), str. 2-8 |
| Musique · Music | *Coronation* <br/> Olivier Holden (1765-1844) |
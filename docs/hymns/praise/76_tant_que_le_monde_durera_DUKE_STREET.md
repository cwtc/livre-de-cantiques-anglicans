---
tags:
  - Louange · Praise
---

# Tant que le monde durera



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/tant_que_le_monde_durera_DUKE_STREET.pdf"}},
            	metaData:{fileName: "tant_que_le_monde_durera_DUKE_STREET.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Tant que le monde durera,<br>Christ notre Maître régnera;<br>Tant que les astres glorieux<br>Rayonneront du haut des cieux. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Les peuples chanteront toujours<br>les grands bienfaits de son amour;<br>Les voix d'enfants proclameront<br>la majesté de son saint Nom. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Il a soumis le monde entier,<br>Rompu les liens des prisonniers,<br>Donné la paix aux malheureux,<br>et béni tous les miséreux. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Jesus shall reign wherever the sun |
| Texte · Text  | Isaac Watts (1674-1748);<br/>tr. Flossette du Pasquier (née 1922) |
| Musique · Music | *Duke Street* <br/> attr. John C. Hatton (mort 1793) |
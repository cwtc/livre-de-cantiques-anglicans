---
tags:
  - Louange · Praise
---

# Sois ma vision



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/sois_ma_vision_SLANE.pdf"}},
            	metaData:{fileName: "sois_ma_vision_SLANE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Sois ma vision, ô Seigneur de mon cœur;<br>car mon gain est perdu sans mon Sauveur.<br>Qu'à toi je pense, le jour et la nuit;<br>ta présence est ma Lumière qui luit. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Sois ma sagesse, mes mots sur mes lèvres,<br>présent quand je dors et quand je me lève;<br>Fais-moi ton enfant, né de ton amour;<br>fais ton tabernacle en moi tous les jours. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Sois ma cuirasse, sois mon bouclier;<br>sois ma délice; sois ma dignité:<br>ma forteresse, l'abri de mon âme!<br>Que ton Saint-Esprit pour toujours m'enflamme. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Toute richesse ne compte pour rien:<br>mon seul héritage est l'amour divin!<br>Roi de mon cœur est le Seigneur, mon Dieu;<br>mon trésor qui règne sur tous les cieux. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Be thou my vision |
| Texte · Text  | Ancien irlandais, VIIIe siècle;<br/>tr. en anglais Mary E. Byrne (1905);<br/>ad. Eleanor H. Hull (1912);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Slane* <br/> Air irlandais (date inconnue) |
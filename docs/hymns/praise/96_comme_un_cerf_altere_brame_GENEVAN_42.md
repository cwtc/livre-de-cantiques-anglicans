---
tags:
  - Louange · Praise
---

# Comme un cerf altéré brâme



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/comme_un_cerf_altere_brame_GENEVAN_42.pdf"}},
            	metaData:{fileName: "comme_un_cerf_altere_brame_GENEVAN_42.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Comme un cerf altéré brâme<br>après le courant des eaux,<br>ainsi souspire mon âme,<br>Seigneur, après tes ruisseaux.<br>Elle a soif du Dieu vivant,<br>et s'écrie en le suivant:<br>ô mon Dieu, quand donc sera-ce<br>que mes yeux verront ta face? </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Pour pain je n'ai que mes larmes,<br>et nuit et jour en tout lieu,<br>lorsqu'en mes dures alarmes,<br>on me dit: Que fait ton Dieu?<br>Je regrette la saison<br>où j'allais en ta maison<br>chantant avec les fidèles<br>tes louanges immortelles. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Mais quel chagrin te dévore?<br>Mon âme, rassure-toi:<br>Espère en Dieu, car encore<br>il sera loué de moi,<br>quand, d'un regard seulement,<br>il guérira mon tourment.<br>Mon Dieu, je sens que mon âme<br>d'un ardent désir se pâme. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Je me souviens à toute heure<br>du temps que, vers le Jourdain,<br>j'avais pour triste demeure<br>Hermon, où j'errais en vain.<br>Et Mishar, et tous ces lieux,<br>où j'étais loin des tes yeux!<br>Partout mes maux me poursuivent,<br>comme des flots qui me suivent. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Dieu, ma force et ma puissance,<br>dirai-je, as-tu donc permis<br>qu’une si longue souffrance<br>m’expose à mes ennemis?<br>Leurs fiers, leurs malins propos<br>me pénètrent jusqu’aux os,<br>quand ils disent à toute heure:<br>Où fait ton Dieu sa demeure? </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Mais pourquoi, mon âme, encore<br>t’abattre avec tant d’effroi?<br>Espère au Dieu que j’adore;<br>il sera loué de moi.<br>Un regard dans sa fureur,<br>me dit qu’il est mon Sauveur;<br>et c’est aussi lui, mon âme,<br>qu’en tous mes maux je réclame. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Théodore de Bèze (1619-1605) |
| Musique · Music | *Genevan 42* <br/> Louis Bourgeois (1551) |
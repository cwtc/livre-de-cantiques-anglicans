---
tags:
  - Louange · Praise
---

# Protège l'enfance, Jésus, Bon Pasteur



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/protege_l'enfance_jesus_bon_pasteur_HANOVER.pdf"}},
            	metaData:{fileName: "protege_l'enfance_jesus_bon_pasteur_HANOVER.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Protège l'enfance, Jésus, bon Pasteur;<br>de son innocence, conserve la fleur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Lorsque, vagabonde, la brebis s'enfuit,<br>à travers le monde, ton amour la suit. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Trop longtemps rebelle au divin Pasteur,<br>brebis infidèle, reviens sur son cœur.<br>De la dent cruelle des loups ravissants,<br>ô Gardien fidèle, Toi seul nous défends. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Vois comme il s'empresse pour te recevoir,<br>lui dont la tendresse est ton seul espoir. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> O Pasteur suprême! Soumis à ta loi,<br>Pour toujours je t'aime et me donne à toi. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Au gras paturages, conduis tes brebis<br>sous les frais ombrages de ton paradis. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | *Chants Sacrés*, Messager canadien du Sacré-Coeur (1930) |
| Musique · Music | *Hanover* <br/> William Croft (1708) |
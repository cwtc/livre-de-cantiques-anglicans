---
tags:
  - Louange · Praise
---

# Dans ton amour, divin Sauveur



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/dans_ton_amour_divin_sauveur_SAGINA.pdf"}},
            	metaData:{fileName: "dans_ton_amour_divin_sauveur_SAGINA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Dans ton amour, divin Sauveur,<br>toi, les délices de ton Père,<br>te dépouillant de ta splendeur,<br>tu vins t'unir à ma misère!<br>Amour divin, le Roi des rois<br>pour moi se donne sur la croix!<br>Amour divin, le Roi des rois<br>pour moi se donne sur la croix! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> En vain, mon cœur, de ton amour,<br>voudrait sonder le grand mystère;<br>Savoir pourquoi tu fus, un jour,<br>frappé pour moi sur le Calvaire!<br>Sublime amour, don merveilleux,<br>plus grand que l'infini des cieux! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Le créateur, l'être immortel,<br>l'auteur, la source de la vie;<br>Le Fils de Dieu, le Roi du ciel,<br>grâce ineffable, infinie,<br>voulant sauver l'Homme pécheur<br>Souffre à sa place et pour lui meurt! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Mais, ô bonheur, tu as vaincu!<br>la mort a dû lâcher sa proie,<br>Et ta victoire est mon salut,<br>oui, mon bonheur, ma paix, ma joie!<br>Aussi je veux, tout à nouveau,<br>bénir ton Nom, ô saint Agneau! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | And can it be that I should gain |
| Texte · Text  | Charles Wesley (1738);<br/>tr. en français Hector Arnéra (1890-1972) |
| Musique · Music | *Sagina* <br/> Thomas Campbell (1825) |
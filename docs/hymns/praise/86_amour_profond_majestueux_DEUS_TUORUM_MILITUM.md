---
tags:
  - Louange · Praise
---

# Amour profond majestueux



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/amour_profond_majestueux_DEUS_TUORUM_MILITUM.pdf"}},
            	metaData:{fileName: "amour_profond_majestueux_DEUS_TUORUM_MILITUM.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Amour profond majestueux:<br>ô grand mystère merveilleux,<br>que pour nous, Dieu, par Dieu le Fils<br>la faible chair mortelle prit! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Un ange il n'a pas envoyé<br>pour que nous soyons rabaissé:<br>Ce Dieu lui-même est venu<br>de la robe humaine vêtu. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Pour nous il s'est fait baptisé!<br>Lui qui nourrit, il a jeûné;<br>il a la tentation connu<br>et a le tentateur battu. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Pour nous il parla et pria,<br>chaque labeur il travailla;<br>chaque bienfait cherchait le bien<br>de ses brébis, et non le sien. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Pour nous trahi par le péché,<br>flagellé, des plaies paré;<br>pour nous il la dure croix prit<br>et il y remettra l'esprit. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Pour nous il est ressuscité;<br>pour nous il monta pour régner;<br>pour nous il envoya l'Esprit<br>qui guide et qui refortifie. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O amor quam ecstaticus |
| Texte · Text  | Thomas a Kempis (1370-1471);<br/>tr. en anglais Benjamin Webb (1852);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Deus tuorum militum* <br/> *Antiphoner*, Grenoble (1753) |
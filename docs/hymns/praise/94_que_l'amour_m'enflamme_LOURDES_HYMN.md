---
tags:
  - Louange · Praise
---

# Que l'amour m'enflamme



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/que_l'amour_m'enflamme_LOURDES_HYMN.pdf"}},
            	metaData:{fileName: "que_l'amour_m'enflamme_LOURDES_HYMN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Que l'amour m'enflamme, Jésus, mon Sauveur!<br>Qu'il vienne en mon âme et brûle mon cœur! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Descendez, saints anges, venez en ce lieu<br>offrir mes louanges au Cœur de mon Dieu!<br>Amour, amour, au Cœur de Jésus!<br>Amour, amour, au Cœur de Jésus! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ô Cœur adorable! Accepte nos chants,<br>et sois favorable à tes chers enfants! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td>  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | *Vieux cantiques et hymnes religieuses* [sic]}, Union Saint-Jean-Baptiste d'Amérique (1931) |
| Musique · Music | *Lourdes Hymn* <br/> Air des Pyrénées;<br/>Grenoble (1882) |
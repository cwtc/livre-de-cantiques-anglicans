---
tags:
  - Louange · Praise
---

# Louons le Créateur



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/louons_le_createur_NUN_DANKET.pdf"}},
            	metaData:{fileName: "louons_le_createur_NUN_DANKET.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Louons le Créateur;<br>chantons à Dieu louanges!<br>Et joignons notre voix<br>au concert de ses anges!<br>Dès les bras maternels<br>il nous a protégés<br>et, jusqu'au dernier jour,<br>il est notre berger. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Loué soit notre Dieu!<br>Que notre vie entière<br>tous nous vivions joyeux<br>sous le regard du Père;<br>qu'il nous tienne en sa grâce<br>et nous guide toujours,<br>nous garde du malheur<br>par son unique amour. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> De ce Dieu trois fois saint<br>qui règne dans la gloire,<br>chrétiens, empressons-nous<br>de chanter la victoire;<br>c'est lui qui nous unit<br>et nous fait retrouver<br>le chemin de l'amour<br>et de la liberté. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Nun danket alle Gott |
| Texte · Text  | Martin Rinckart (1636);<br/>tr. en anglais Catherine Winkworth (1858);<br/>tr. en français Flossette du Pasquier (née 1922) |
| Musique · Music | *Nun danket* <br/> Johann Crüger (1647) |
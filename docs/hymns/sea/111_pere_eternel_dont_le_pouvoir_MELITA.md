---
tags:
  - Sur mer · At sea
---

# Père éternel dont le pouvoir



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/pere_eternel_dont_le_pouvoir_MELITA.pdf"}},
            	metaData:{fileName: "pere_eternel_dont_le_pouvoir_MELITA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Père éternel, dont le pouvoir<br>se plaît à sauver, tu fais voir<br>leurs limites aux vastes mers<br>et mets un frein aux flots amers:<br>Vois nos pleurs, entends nos sanglots<br>pour ceux en péril sur les flots. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ô Christ, dont les eaux autrefois<br>ouïrent la puissante voix,<br>qui sur les ondes as marché,<br>dans l'ouragan restas couché:<br>Vois nos pleurs, entends nos sanglots<br>pour ceux en péril sur les flots. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Toi qui, dans le temps du chaos,<br>en planant au-dessus des eaux,<br>Esprit Saint, couvas l'univers<br>et peuplas la terre et les mers:<br>Vois nos pleurs, entends nos sanglots<br>pour ceux en péril sur les flots. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Ô Trinité, puissance, amour,<br>garde nos frères nuit et jour;<br>et si du feu, des ennemis<br>loin du danger tu les as mis:<br>Des louanges, non des sanglots<br>montent de la terre et des flots. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Eternal Father, strong to save |
| Texte · Text  | William Whiting (1860);<br/>tr. *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Melita* <br/> John Bacchus Dykes (1861) |
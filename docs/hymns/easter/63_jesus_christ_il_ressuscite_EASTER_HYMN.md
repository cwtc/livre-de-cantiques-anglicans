---
tags:
  - Pâques · Easter
---

# Jésus Christ, il réssuscite



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/jesus_christ_il_ressuscite_EASTER_HYMN.pdf"}},
            	metaData:{fileName: "jesus_christ_il_ressuscite_EASTER_HYMN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Jésus-Christ, il réssuscite,<br>C'est son triomphe aujourd'hui;   <br>Sur la croix il a souffert,<br>Pour racheter notre perte. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Que l'on chante à notre Dieu, <br>À Jésus, le Roi des Cieux; <br>Pour ses pécheurs adorés,<br>La mort il a endurée. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Par ses plaies, obtenu,  <br>Alléluia!<br>Est le don de son salut;  <br>Alléluia!<br>Au cieux il sera assis,<br>Alléluia!<br>L'air céleste s'amplifie.<br>Alléluia! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Chante, l'armée céleste,<br>Comme il nous aime: sans cesse;<br>Chantons-lui tous aujourd'hui,<br>Père, Fils, et Saint-Esprit.  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Jesus Christ is risen today |
| Texte · Text  | *Lyrica Davidica* (1790);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Easter Hymn* <br/> *Lyrica Davidica* (1790) |
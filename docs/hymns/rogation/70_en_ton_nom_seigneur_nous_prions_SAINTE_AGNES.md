---
tags:
  - Rogation · Rogation
---

# En ton nom, Seigneur, nous prions



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/en_ton_nom_seigneur_nous_prions_SAINTE_AGNES.pdf"}},
            	metaData:{fileName: "en_ton_nom_seigneur_nous_prions_SAINTE_AGNES.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> En ton nom, Seigneur, nous prions;<br>tu nous promis d'entendre.<br>À toi semece et les moissons,<br>le printemps et décembre. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Lorsque le vent d'hiver souffla,<br>tu fus notre espérance;<br>nous sentons, quand avril est là,<br>ta sage providence. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Par la pluie au temps désiré,<br>l'air pur et la lumière,<br>le vert épi, le grain doré,<br>tu bénis la prière. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Aussi de toi viennent en nous<br>l'invisible croissance,<br>le sage effroi, l'amour si doux,<br>la calmante espérance. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Lord, in thy name thy servants plead |
| Texte · Text  | John Keble (1856);<br/>tr. *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Sainte Agnes* <br/> John Bacchus Dykes (1866) |
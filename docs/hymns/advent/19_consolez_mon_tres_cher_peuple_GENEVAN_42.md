---
tags:
  - Avent · Advent
---

# Consolez mon très cher peuple



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/consolez_mon_tres_cher_peuple_GENEVAN_42.pdf"}},
            	metaData:{fileName: "consolez_mon_tres_cher_peuple_GENEVAN_42.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> “Consolez mon très cher peuple,<br>consolez,” dit notre Dieu;<br>“Consolez ceux aux ténèbres<br>sous leur fardeau en tout lieu;<br>A Jérusalem parlez:<br>elle aura enfin sa paix;<br>chaque péché je vais couvrir,<br>et sa guerre je vais finir.” </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ses péchés seront pardonné,<br>chacun sera effacé;<br>ce qui mérite son ire<br>ne sera plus écouté.<br>Son chagrin qu'elle vécut<br>est maintenant disparu;<br>Dieu changera sa tristesse<br>en éternelle allégresse. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Car c'est la voix d'Esaïe;<br>le desert entend son cri:<br>l'appel à la repentence,<br>car le Royaume est ici.<br>Ce conseil nous écoutons;<br>son chemin nous préparons:<br>toute vallée exhaussée,<br>toute colline abaissée. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Applatisez le tortueux;<br>rendez lisse le rugueux;<br>rendez vos cœurs purs et humbles,<br>séant le règne de Dieu.<br>Car la gloire du Seigneur<br>est révéle aux pécheurs:<br>on voit de cette parure<br>que cette Parole dure. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Tröstet, tröstet meine Leben |
| Texte · Text  | Johann Olearius (1671);<br/>Catherine Winkworth (1863);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Genevan 42* <br/> Louis Bourgeois (1551) |
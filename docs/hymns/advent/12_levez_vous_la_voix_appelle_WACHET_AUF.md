---
tags:
  - Avent · Advent
  - Mariage · Marriage
---

# Levez vous! La voix appelle



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/levez_vous_la_voix_appelle_WACHET_AUF.pdf"}},
            	metaData:{fileName: "levez_vous_la_voix_appelle_WACHET_AUF.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> “Levez-vous!” la voix appelle;<br>les veilleurs crient des citadelles:<br>“Jérusalem, ouvre les yeux.”<br>À minuit, l'heure éclatante,<br>elles appellent, ces voix brillantes;<br>où sont les filles sages?<br>“Le fiancé, il vient:<br>ô, préparez vos lampes.”<br>Alléluia!<br>“Debout, debout, préparez-vous<br>de le voir devenir Époux.” </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Elle entend les veilleurs chanter;<br>l'âme de Sion est enchantée;<br>sa joie la fait partir en hâte.<br>Son amant descend en splendeur,<br>la grâce et vérité sa grandeur;<br>cet Astre matinal  épate.<br>Viens, couronne des cieux,<br>ô Jésus, Fils de Dieu!<br>Hosianna!<br>On est mené par sa charité<br>à célébrer son propre banquet. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Nous chantons tous ses louanges,<br>en nos langues et celles des anges,<br>sur harpe et cymbale bientôt.<br>Douze perles font les portes;<br>chez toi, nous sommes la reine consort,<br>les anges à ton trône très haut.<br>Jamais ni l'œil n'a vu,<br>ni l'oreille entendu<br>un tel bonheur<br>que notre joie: l’Éternel, le Roi,<br>que nos louanges soient à toi. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Wachet auf! Ruft uns die Stimme |
| Texte · Text  | Philipp Nicolai (1599);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Wachet auf* <br/> Philipp Nicolai (1599);<br/>harm. J. S. Bach (1731) |
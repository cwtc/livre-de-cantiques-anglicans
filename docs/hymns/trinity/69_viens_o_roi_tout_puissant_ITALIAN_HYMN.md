---
tags:
  - Trinité · Trinity
---

# Viens, ô Roi tout-puissant



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/viens_o_roi_tout_puissant_ITALIAN_HYMN.pdf"}},
            	metaData:{fileName: "viens_o_roi_tout_puissant_ITALIAN_HYMN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Viens, ô Roi tout-puissant;<br>accepte notre chant;<br>viens au secours!<br>Père tout glorieux,<br>sur tout victorieux:<br>chantons, élogieux,<br>l'Ancien des Jours! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Viens, ô Verbe incarné;<br>porte enfin ton épée<br>à deux tranchants!<br>Que ton secours puissant,<br>qui toujours nous défend,<br>pénétre jusqu'au fond<br>des cœurs croyants! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Viens, ô Consolateur;<br>entre dans chaque cœur<br>en ce moment!<br>Sur les eaux tu planais,<br>ton feu nous inspirait:<br>ne nous quitte jamais,<br>Esprit puissant! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> A ce grand Un en Trois<br>gloire éternelle soit,<br>gloire à jamais!<br>Que cette majesté<br>soit enfin dévoilée<br>pour que la Trinité<br>soit adorée! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Come, thou almighty King |
| Texte · Text  | inconnu (c. 1757);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Italian Hymn* <br/> Felice de Giardini (1716-1796) |
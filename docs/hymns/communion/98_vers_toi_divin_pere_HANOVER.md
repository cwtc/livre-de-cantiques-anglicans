---
tags:
  - Communion · Communion
---

# Vers toi, divin Père



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/vers_toi_divin_pere_HANOVER.pdf"}},
            	metaData:{fileName: "vers_toi_divin_pere_HANOVER.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Vers Toi, divin Père, s'élèvent mes yeux:<br>entends ma prière; exauce mes voeux;<br>Du fond de la terre, mon cœur malheureux<br>t'invoque, ô lumière, puissant Roi des Cieux! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Mon âme est joyeuse, Jésus, près de toi:<br>ma vie est heureuse soumise à ta Loi.<br>Du monde oublieuse, fidèle à sa foi,<br>mon âme amoureuse t'acclame, ô mon Roi. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ta main redoutable, terrible au pécheur,<br>me fut secourable, rempart protecteur.<br>Amour ineffable! Ce tendre Pasteur<br>invite à sa Table l'enfant voyageur. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> La ferme assurance d'un cœur paternel,<br>avec ta puissance, rassure un mortel;<br>J'ai douce espérance de voir dans le ciel<br>ta chère présence, ô Christ éternel. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Attr. Monsignor Pirio (date inconnue) |
| Musique · Music | *Hanover* <br/> William Croft (1708) |
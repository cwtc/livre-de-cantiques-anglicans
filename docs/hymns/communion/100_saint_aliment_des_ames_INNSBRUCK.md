---
tags:
  - Communion · Communion
---

# Saint aliment des âmes



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/saint_aliment_des_ames_INNSBRUCK.pdf"}},
            	metaData:{fileName: "saint_aliment_des_ames_INNSBRUCK.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Saint aliment des âmes,<br>pour tous doux Viatique,<br>ô manne, ô pain divin!<br>Lassés tu nous ranimes,<br>donnant la paix divine;<br>tu nous soutiens à notre mort. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Jésus, quittant la terre,<br>nous donne song sang même,<br>sa chair en aliment.<br>Comment jamais pourrai-je,<br>en toute gratitude,<br>assez louer ce don divin? </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Rempli de confiance,<br>je crois à ta Présence<br>dans ton humilité.<br>Ô Dieu voilé, je prie<br>qu'un jour je te contemple<br>sans voile aucun dans ta splendeur. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Oui, c'est le Pain de Vie<br>que l'ange même envie,<br>venu du ciel pour nous.<br>Il est pour l'Homme un gage<br>de charité divine<br>et d'une heureuse éternité. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | *Psallam Deo*, Schola Cantorum de Montréal (1922) |
| Musique · Music | *Innsbruck* <br/> Heinrich Isaac (1539);<br/>harm. J. S. Bach (1729) |
---
tags:
  - Matin · Morning
---

# O Dieu, Seigneur de vérité



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/o_dieu_seigneur_de_verite_SONG_34.pdf"}},
            	metaData:{fileName: "o_dieu_seigneur_de_verite_SONG_34.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ô Dieu, Seigneur de vérité,<br>le temps par ta main est rangé:<br>Envoie l'aurore brillant,<br>et ralume le jour luisant. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Éteins donc nos péchés brûlants;<br>enlève chaque tentation;<br>en gardant notre corps uni,<br>verse-nous ta paix curative. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> O Père clément, fait ainsi,<br>par Jésus Christ ton Fils chéri,<br>avec le Saint Esprit vivant<br>et règnant éternellement. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Rector potens, verax Deus |
| Texte · Text  | Saint Ambroise (340-397);<br/>tr. en anglais J. M. Neale (1818-1866);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Song 34* <br/> Orlando Gibbons (1623) |
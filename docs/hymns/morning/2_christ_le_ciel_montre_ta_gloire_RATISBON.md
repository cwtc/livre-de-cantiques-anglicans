---
tags:
  - Matin · Morning
---

# Christ, le ciel montre ta gloire



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/christ_le_ciel_montre_ta_gloire_RATISBON.pdf"}},
            	metaData:{fileName: "christ_le_ciel_montre_ta_gloire_RATISBON.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Christ, le ciel montre ta gloire,<br>Christ, la Vraie Lumière;<br>Vrai Soleil, prends ta victoire<br>sur cette nuit dernière.<br>Aube du ciel, montre-toi;<br>Étoile du matin, Roi! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Sombre et morne est le matin  <br>quand tu n'es pas avec moi;  <br>le jour est plein de chagrin  <br>sauf si tes rayons se voient;<br>ils écrasent tous mes pleurs,<br>brillant et chauffant mon cœur. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Visite cette âme à moi!  <br>Perce mes déficiences.<br>Comble-moi, rayonnement;  <br>console mon incroyance.<br>Ainsi tu t’es révélé,<br>luant jusqu’au jour parfait. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Christ, whose glory fills the skies |
| Texte · Text  | Charles Wesley (1740);<br/>tr. Arlie Coles (2021); |
| Musique · Music | *Ratisbon* <br/> Johann Gottlob Werner (1815) |
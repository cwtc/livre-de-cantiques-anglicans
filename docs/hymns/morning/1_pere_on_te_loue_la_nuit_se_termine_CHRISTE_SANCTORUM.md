---
tags:
  - Matin · Morning
---

# Père, on te loue; la nuit se termine



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/pere_on_te_loue_la_nuit_se_termine_CHRISTE_SANCTORUM.pdf"}},
            	metaData:{fileName: "pere_on_te_loue_la_nuit_se_termine_CHRISTE_SANCTORUM.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Père, on te loue; la nuit se termine;<br>nous s'assemblons tous devant toi, les veilleurs;<br>nous offrons par le chant toutes nos prières:<br>ainsi on t’adore. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Roi de la création, qu’on soit prêt d’y entrer;<br>chasse nos fautes; donne-nous la santé;<br>ramène-nous au ciel, où sont tes saints unis<br>en joie éternelle. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Au Père, au Fils, et à l’Esprit ensemble:<br>Trinité, donne-nous la bénédiction;<br>à toi la gloire, brillant et éclatant<br>en toute création. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Father, we praise thee; now the night is over |
| Texte · Text  | Grégoire le Grand (540-604);<br/>tr. en anglais Percy Dearmer (1867-1936);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Christe sanctorum* <br/> *La Feillée, Méthode du plain-chant* (1782) |
---
tags:
  - Passion · Passiontide
---

# Tout honneur, toute gloire



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/tout_honneur_toute_gloire_SAINT_THEODULPH.pdf"}},
            	metaData:{fileName: "tout_honneur_toute_gloire_SAINT_THEODULPH.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Tout honneur, toute gloire au Rédempteur, au Roi!<br>La bouche des enfants chanta tes doux Hosanna!<br>C'est toi le Roi d'Israël,<br>Fils royal de David,<br>qui vient au Nom du Seigneur,<br>le Roi et le Béni. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Les anges, les archanges<br>te magnifient aux cieux;<br>nous hommes aussi, nous offrons<br>nos louanges en tout lieu. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Les Hébreux ont fait ton chemin,<br>leurs rameaux apportant;<br>nous offrons nos prières<br>devant toi en chantant. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Leurs hymnes avant ta Passion<br>ils t'avaient chantés;<br>nos chants aussi sont pour toi,<br>maintenant exulté. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Gloria, laus, et honor |
| Texte · Text  | Theodulph, Evêque d'Orléans (820);<br/>tr. en anglais J. M. Neale (1854);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Saint Theodulph* <br/> Melchir Teschner (1613);<br/>harm. William H. Monk (1823-1889) |
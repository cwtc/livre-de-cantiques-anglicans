---
tags:
  - Passion · Passiontide
---

# L'étendard vient du Roi des rois



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/l'etendard_vient_du_roi_des_rois_VEXILLA_REGIS_PRODEUNT.pdf"}},
            	metaData:{fileName: "l'etendard_vient_du_roi_des_rois_VEXILLA_REGIS_PRODEUNT.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> L'étendard vient  du Roi  des rois; <br>le mystère  vient de  la  croix, <br>où pend en chair sainte et <br>sacrée,<br>lui qui  toute  chair a  créée.  </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Où de  plus est  à mort  blessé <br>le flanc par la  lance  percé; <br>pour nous rendre  nets de <br>souillure,<br>le sang  sort et  l'eau tout  à <br>l'heure. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ores  on voit  vérifié <br>ce que David  avait  crié: <br>que Dieu, par le  bois qui  le<br>serre,<br>régnerait un  jour sur  la <br>terre. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Arbre  beau, tout  resplendissant <br>de la pourpre  du Roi  puissant, <br>Arbre sur tous  autres <br>insigne,<br>par l'attoucher  de chair   si <br>digne! </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Je te salue, ô sainte croix,<br>notre espoir seul en ces endroits!<br>Donne aux bons accroît de justice;<br>pardonne aux pécheurs leur malice. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Dieu grand, haute Trinité,<br>tout esprit loue ta bonté.<br>Si la croix sauve les coupables,<br>rends-nous les perdus perdurables. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Vexilla regis prodeunt |
| Texte · Text  | Venantius Honorius Clementianus Fortunatus (569)<br/>tr. Saint François de Sales (1567-1661) |
| Musique · Music | *Vexilla regis prodeunt* <br/> Plain-chant de Sarum, VIIe siècle |
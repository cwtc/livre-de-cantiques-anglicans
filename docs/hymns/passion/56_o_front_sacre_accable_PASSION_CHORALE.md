---
tags:
  - Passion · Passiontide
---

# O front sacré, accablé



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/o_front_sacre_accable_PASSION_CHORALE.pdf"}},
            	metaData:{fileName: "o_front_sacre_accable_PASSION_CHORALE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ô front sac-- ré, accablé<br>d'injure et d'injustice!<br>Ô front royal entouré<br>d'une couronne d'épine!<br>Préservée est ta grandeur,<br>malgré cette souffrance,<br>Ô tête dont la splendeur<br>charme l'armée des anges! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ta beauté très désiré<br>a quitté notre vue.<br>Ton pouvoir a expiré,<br>Lumière disparue.<br>Ne cache pas ta grâce<br>de moi pour qui tu meurs!  <br>Montre en amour ta face,<br>que je voie sa lueur.   </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> De ta Passion amère<br>mon cœur veut par tager.  <br>Avec toi il espère<br>être à la croix cloué.  <br>Que ce cœur se dispose<br>à rester à tes pieds  <br>En lamentant, mais j'ose<br>aussi te remercier.   </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Donc maintenant je chante<br>ce merci, mon ami,  <br>Pour ta douleur mourante,<br>Ton amour infini!  <br>Faismoi le tien pour toujours!<br>Et si je suis tenté,  <br>Seigneur, que, par ton amour,<br>je n'y quitte jamais.   </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Salve caput cruentatum |
| Texte · Text  | Arnulf de Louvain (mort 1250)<br/>ad. en allemand Paul Gerhardt (1607-1676);<br/>tr. en anglais Robert Seymour Bridges (1844-1930), str. 1-3, 5<br/>tr. en anglais John W. Alexander (1804-1859), str. 4;<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Passion Chorale* <br/> Hans Leo Hassler (1601) |
---
tags:
  - Passion · Passiontide
---

# Hors pair est cet amour



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/hors_pair_est_cet_amour_LOVE_UNKNOWN.pdf"}},
            	metaData:{fileName: "hors_pair_est_cet_amour_LOVE_UNKNOWN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Hors pair est cet amour<br>de mon Sauveur pour moi:<br>l’amour aux sans-amour,<br>pour qu’il adorable soit.<br>Mais qui suis-je, que pour mes peines,<br>mon Seigneur prenne chair et meure? </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Il vint du trône saint<br>pour donner le salut;<br>son amour l'Homme a craint;<br>le Christ restant inconnu.<br>Mais mon Ami, en vérité,<br>il a donné pour moi sa vie! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> On dégagait sa voie<br>en chantant tous sa gloire;<br>et on poussait au Roi<br>des “Hosanna!” pleins de joie.<br>Puis on implorait, “Crucifie!”<br>et leur cri réclamait sa mort. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Qu’a-t-il fait, mon Seigneur?<br>D’où ces rages et colères?<br>Le boiteux peut courir,<br>et l'aveugle voit encore.<br>Blessures douces! On les déteste;<br>on manifeste et le repousse. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Il vivait sans maison<br>sur Terre, mon Seigneur;<br>et il mourrait sans tombe<br>hors le don d’un étranger.<br>Le lieu du Roi, c'est bien très haut,<br>et son tombeau était pour moi. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Ô quel compte divin!<br>Le monde n’a jamais vu<br>un chagrin comme le tien,<br>ni ton amour reconnu.<br>C’est mon Ami, que je louerai<br>pour chaque journée de ma vie. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | My song is love unknown |
| Texte · Text  | Samuel Crossman (1664);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Love unknown* <br/> John Ireland (1918) |
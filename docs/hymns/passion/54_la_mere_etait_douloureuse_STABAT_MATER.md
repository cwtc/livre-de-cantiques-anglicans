---
tags:
  - Passion · Passiontide
---

# La Mère était douloureuse



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/la_mere_etait_douloureuse_STABAT_MATER.pdf"}},
            	metaData:{fileName: "la_mere_etait_douloureuse_STABAT_MATER.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> La Mère était douloureuse<br>à la croix âpre et peneuse,<br>pleurant son Fils qui pendait. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Adonc son âme gémissant,<br>complaignant et douleur sentant,<br>le glaive le transperçait. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Comment afflicte et amère<br>fut cette bénoîte Mère<br>du vrai Dieu, vrai jour sans nuit! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Laquelle moult se doulousa<br>et qui, voyant son Fils, trembla<br>des peines du noble fruit. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Qui est-il qui ne pleurerait,<br>s'il la Mère du Fils voyait<br>complaindre si durement? </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Qui, sans tristesse agnoisseuse,<br>verrait la Mère pitieuse<br>et son Fils traités tellement? </td>
	</tr>
	<tr>
	<td> 7 </td>
	<td> Pour ôter les griefs de sa gent,<br>elle vit Jésus en tourment,<br>sujet à chaque despit. </td>
	</tr>
	<tr>
	<td> 8 </td>
	<td> Elle vit son très doux Enfant<br>laissé seul des siens et mourant<br>quand il mis hors son esprit. </td>
	</tr>
	<tr>
	<td> 9 </td>
	<td> Ô Mère, fontaine d'amour,<br>fais-moi voir ta forte douleur,<br>avec toi tristesse faire! </td>
	</tr>
	<tr>
	<td> 10 </td>
	<td> Fais mon cœur ardoir doucement<br>en aimant Jésus fortement,<br>pour que je lui puisse plaire. </td>
	</tr>
	<tr>
	<td> 11 </td>
	<td> Sainte Mère, hâtivement<br>fixe en mon cœur profondement<br>chaque plaie de ton Fils; </td>
	</tr>
	<tr>
	<td> 12 </td>
	<td> Et que j'aie ma portion<br>du fruit que par sa Passion<br>par ses souffrances acquis. </td>
	</tr>
	<tr>
	<td> 13 </td>
	<td> Permets-moi d'avec toi pleurer<br>à cette croix et lamenter<br>par dévote compassion. </td>
	</tr>
	<tr>
	<td> 14 </td>
	<td> Et qu'avec toi, sans départir,<br>puisse compagnie tenir<br>par sainte méditation. </td>
	</tr>
	<tr>
	<td> 15 </td>
	<td> Vièrge toute nette et claire,<br>ne sois envers moi amère.<br>Fais-moi ici lamenter, </td>
	</tr>
	<tr>
	<td> 16 </td>
	<td> et porter de Jésus la mort<br>et de sa passion, l'effort<br>de ses peines recorder. </td>
	</tr>
	<tr>
	<td> 17 </td>
	<td> Fais que mon cœur soit si navré<br>des plaies, et tout enivré<br>de la croix de ton cher Fils; </td>
	</tr>
	<tr>
	<td> 18 </td>
	<td> Et que par tel embrasement<br>je puisse avoir au jugement<br>défense contre le péril. </td>
	</tr>
	<tr>
	<td> 19 </td>
	<td> Fais que cette croix me garde,<br>que sa mort me sauvegarde;<br>par sa grâce il me nourrit; </td>
	</tr>
	<tr>
	<td> 20 </td>
	<td> Impètre à mon âme le don<br>de paradis, gloire et pardon<br>quand mon corps sera pourri. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Stabat mater dolorosa |
| Texte · Text  | attr. Jacopone de Todi, XIIIe siècle<br/>tr. *Livre d'heures à l'usage de Paris*, XVe siècle, str. 1-18;<br/>tr. *Livre d'heures à l'usage de Besançon*, XVe siècle, str. 19-20<br/>ad. Arlie Coles (2021) |
| Musique · Music | *Stabat Mater* <br/> *Gesangbuch*, Mainz (1661) |
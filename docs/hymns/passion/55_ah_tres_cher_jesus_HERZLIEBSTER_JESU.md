---
tags:
  - Passion · Passiontide
---

# Ah, très cher Jésus



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/ah_tres_cher_jesus_HERZLIEBSTER_JESU.pdf"}},
            	metaData:{fileName: "ah_tres_cher_jesus_HERZLIEBSTER_JESU.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ah, très cher Jésus, quelle est ta transgression,<br>que l'on te punit avec tant d'oppression?<br>Où est le tort? Quelles fautes as-tu commises<br>qu'ils interdisent? </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Qui est coupable? Quelle est donc la raison?<br>Ah, très cher Jésus, c'était ma trahison!<br>Moi, mon Seigneur, c'était moi qui t'ai nié,<br>et crucifié. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Qu'il est néanmoins étrange, ce conflit:<br>Le Bon Pasteur, il souffre pour ses brebis.<br>La dette payée, expié par le Seigneur<br>pour ses serviteurs! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> L'amour qui dépasse la compréhension,<br>il t'a mené sur ce chemin de Passion.<br>C'était pour mon salut, ton incarnation<br>et ton oblation. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td>  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Herzliebster Jesu |
| Texte · Text  | Johan Heermann (1585-1647);<br/>tr. en anglais Robert Bridges (1897);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Herzliebster Jesu* <br/> Johann Crüger (1598-1662) |
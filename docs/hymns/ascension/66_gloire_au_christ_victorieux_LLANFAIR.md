---
tags:
  - Ascension · Ascension
---

# Gloire au Christ victorieux



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/gloire_au_christ_victorieux_LLANFAIR.pdf"}},
            	metaData:{fileName: "gloire_au_christ_victorieux_LLANFAIR.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Gloire au Christ victorieux,    <br>Qui monte au plus haut des cieux,    <br>Christ après sa Résurrection,   <br>Ce jour fait son Ascension.     </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ouvrezvous, portes du ciel, Al léluia!<br>Pour ce vainqueur immortel, Al léluia!<br>Car il a détruit la mort, Al léluia!<br>Lui le Dieu saint, le Dieu fort. Al léluia! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Le ciel reçoit son Seigneur;     <br>Et la terre a son Plaideur;     <br>Malgré son retour divin,   <br>L'Homme demeure le sien.      </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Devant le trône divin,    <br>Pour nous il lève ses mains,    <br>Où se gravent pour toujours,   <br>les marques de son amour.     </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Tout-puissant Intercesseur, Alléluia!<br>Qui plaides pour nous pécheurs, Alléluia!<br>Fais-nous place auprès de toi, Alléluia!<br>Dans le ciel, ô Roi des rois. Alléluia! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Malgré ton départ, Seigneur, Alléluia!<br>Tourne-nous à ta hauteur, Alléluia!<br>Qu'on fasse la volonté de Dieu, Alléluia!<br>Sur la terre comme aux cieux. Alléluia! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Hail the day that sees him rise |
| Texte · Text  | Charles Wesley (1707-1788);<br/>tr. Richard Paquier (1905-1985), str. 1-2, 4-5, ad. Arlie Coles (2021), str. 1;<br/>tr. Arlie Coles (2021), str. 3, 6 |
| Musique · Music | *Llanfair* <br/> Robert Williams (1817) |
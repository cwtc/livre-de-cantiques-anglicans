---
tags:
  - Noël · Christmas
---

# Dans cette étable



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/dans_cette_etable_DANS_CETTE_ETABLE.pdf"}},
            	metaData:{fileName: "dans_cette_etable_DANS_CETTE_ETABLE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Dans cette étable,<br>Qu'il est aimable<br>Que d'attraits à la fois!<br>Tous les palais des rois<br>aux beautés que je vois<br>dans cette étable. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Que sa puissance<br>paraît bien en ce jour,<br>malgré l'enfance<br>où le réduit l'amour!<br>Le monde racheté,<br>en tout l'enfer dompté,<br>font voir qu'à sa naisance,<br>rien n'est si redouté<br>que sa puissance. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Touchant mystère!<br>Jésus, souffrant pour nous,<br>d'un Dieu sévère<br>apaise le courroux.<br>Du testament nouveau<br>il est le doux Agneau;<br>il doit sauver la terre<br>portant notre fardeau:<br>Touchant mystère. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> S'il est sensible,<br>ce n'est qu'à nos malheurs;<br>le froid pénible<br>ne cause point ses pleurs.<br>Mon cœur à tant d'attraits,<br>à de si doux bienfaits;<br>à ce charme invincible<br>doit céder désormais,<br>s'il est sensible. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | français, XVIIIe siècle |
| Musique · Music | *Dans cette étable* <br/> air français, XVIIIe siècle |
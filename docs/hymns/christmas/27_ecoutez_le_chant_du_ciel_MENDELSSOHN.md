---
tags:
  - Noël · Christmas
---

# Ecoutez le chant du ciel



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/ecoutez_le_chant_du_ciel_MENDELSSOHN.pdf"}},
            	metaData:{fileName: "ecoutez_le_chant_du_ciel_MENDELSSOHN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Écoutez le chant du ciel: le Roi naît en Is-- ra-- ël!<br>Paix sur terre par le Christ, <br>Dieu et l'homme réunis.<br>Tous les peuples et les anges,<br>Chantez ensemble en triomphe:<br>Avec les cieux proclamez:<br>à Bethléem, le Christ est né. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Christ, adoré par les cieux, <br>Christ, perpetuel Seigneur,<br>De la vierge tu es né, <br>Sur terre enfin arrivé!<br>Voici habillé en chair le<br>Fils consubstantiel au Père!<br>Verbe vivant parmi nous,<br>Notre Emmanuel, Jésus!<br>Écoutez le chant du ciel,<br>Le Roi naît en Israël. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Il adopta notre sort, <br>Né pour conquérir la mort,<br>Né pour nous ressusciter, <br>Né que l'on soit deux fois né.<br>Le Soleil de la justice,<br>sous ses ailes il guérit, <br>Admirable Conseiller!<br>Dieu et Prince de la Paix! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Hark, the herald angels sing |
| Texte · Text  | Charles Wesley (1707-1788);<br/>tr. Violette du Pasquier (1889-1952), ad. Arlie Coles (2021), str. 1;<br/>tr. Arlie Coles (2021), str. 2-3 |
| Musique · Music | *Mendelssohn* <br/> Felix Mendelssohn (1809-1847) |
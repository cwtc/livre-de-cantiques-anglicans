---
tags:
  - Noël · Christmas
---

# Cà, bergers



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/ca_bergers_CA_BERGERS.pdf"}},
            	metaData:{fileName: "ca_bergers_CA_BERGERS.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Çà, bergers, assemblonsnous;<br>allons voir le Messie;<br>Cherchons cet enfant si doux<br>dans les bras de Marie.<br>Je l'entends; il nous appelle tous:<br>ô sort digne d'envie! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Laissons là tout le troupeau;<br>qu'il erre à l'aventure:<br>Que sans nous sur ce coteau<br>il cherche sa pâture.<br>Allons voir dans un petit berceau<br>l'auteur de la nature. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Que l'hiver par ses frimas<br>ait endurci la plaine;<br>S'il croit arrêter nos pas,<br>cette espérance est vaine.<br>Quand on cherche un Dieu rempli d'appas,<br>on ne craint point de peine. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Sa naissance sur ces bords<br>ramène l'allégresse:<br>Répondons par nos transports<br>à l'ardeur qui le presse.<br>Secondons de nouveaux efforts<br>l'excès de sa tendresse. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Simon-Joseph Pellegrin (1663-1745) |
| Musique · Music | *Cà, bergers* <br/> air français, XIXe siècle |
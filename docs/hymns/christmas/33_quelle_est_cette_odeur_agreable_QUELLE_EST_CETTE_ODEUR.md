---
tags:
  - Noël · Christmas
---

# Quelle est cette odeur agréable



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/quelle_est_cette_odeur_agreable_QUELLE_EST_CETTE_ODEUR.pdf"}},
            	metaData:{fileName: "quelle_est_cette_odeur_agreable_QUELLE_EST_CETTE_ODEUR.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Quelle est cette odeur agréable,<br>bergers, qui ravit tous nos sens?<br>S'exhalet-il rien de semblable<br>au milieu des fleurs du printemps?<br>Quelle est cette odeur agréable,<br>bergers, qui ravit tous nos sens? </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Mais quelle éclatante lumière<br>dans la nuit vient frapper nos yeux!<br>L'astre de jour, dans sa carrière,<br>futil jamais si radieux!<br>Mais quelle éclatante lumière<br>dans la nuit vient frapper nos yeux. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> À Bethléem, dans une crèche,<br>il vient de vous naître un Sauveur.<br>Allons, que rien ne vous empêche<br>d'adorer votre Rédempteur.<br>À Bethléem, dans une crèche,<br>il vient de vous naître un Sauveur. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Dieu Tout- Puissant, gloire éternelle<br>te soit rendue jusqu'aux cieux.<br>Que la paix soit universelle;<br>que la grâce abonde en tout lieu.<br>Dieu Tout- Puissant, gloire éternelle<br>te soit rendue jusqu'aux cieux. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | français, XVIIe siècle |
| Musique · Music | *Quelle est cette odeur* <br/> John Gay (1728)<br/>arr. Roger Bergs (2003) |
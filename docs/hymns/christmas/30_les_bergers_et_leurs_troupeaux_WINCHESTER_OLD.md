---
tags:
  - Noël · Christmas
---

# Les bergers et leurs troupeaux



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/les_bergers_et_leurs_troupeaux_WINCHESTER_OLD.pdf"}},
            	metaData:{fileName: "les_bergers_et_leurs_troupeaux_WINCHESTER_OLD.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Les bergers et leurs troupeaux étaient<br>aux grands champs assis<br>quand, tout resplendissant, l'ange<br>du Seigneur descendit. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> “Ne craignez point, dit-il, car ils<br>furent saisi d'effroi;<br>J'annonce une bonne nouvelle,<br>d'une grande joie.” </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> “C'est dans la ville de David<br>qu'il est né un Sauveur;<br>c'est aujourd'hui qu'il apparaît,<br>le Christ et le Seigneur.” </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> “Et voici donc à quel signe<br>vous le reconnaîtrez:<br>c'est à la crèche qu'est l'Enfant,<br>des langes emmailloté.” </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Et soudain l'ange se joignit<br>au bataillon brillant<br>de toute l'armée céleste,<br>et voici leur doux chant: </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> “Soit gloire à Dieu dans les hauts lieux;<br>sur terre soit la paix;<br>que cette bienveillance aux hommes<br>ne cesse jamais!” </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | While shepherds watched their flocks by night |
| Texte · Text  | Nahum Tate (1700);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Winchester Old* <br/> attr. George Kirbye (1592) |
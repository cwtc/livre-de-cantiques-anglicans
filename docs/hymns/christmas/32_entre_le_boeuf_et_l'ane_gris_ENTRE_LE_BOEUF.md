---
tags:
  - Noël · Christmas
---

# Entre le boeuf et l'âne gris



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/entre_le_boeuf_et_l'ane_gris_ENTRE_LE_BOEUF.pdf"}},
            	metaData:{fileName: "entre_le_boeuf_et_l'ane_gris_ENTRE_LE_BOEUF.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Entre le bœuf et l'âne gris<br>dort, dort, dort le petit Fils.<br>Mille anges divins,<br>mille séraphins<br>volent à l'entour de ce grand<br>Dieu d'amour. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Entre les deux bras de Marie<br>dort, dort, dort le Fruit de Vie. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Entre les pastoureaux jolis<br>dort, dort Jésus qui sourit. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> En ce beau jour si solonnel<br>dort, dort, dort l'Emmanu el. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | français, XIIIe siècle |
| Musique · Music | *Entre le boeuf* <br/> air français, XIIIe siècle |
---
tags:
  - Noël · Christmas
---

# Minuit, chrétiens



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/minuit_chretiens_CANTIQUE_DE_NOEL.pdf"}},
            	metaData:{fileName: "minuit_chretiens_CANTIQUE_DE_NOEL.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Minuit, chrétiens! <br>C'est l'heure solennelle,<br>où l'homme Dieu descendit jusqu'à nous<br>pour effacer la tache originelle,<br>et de son Père arrêter le courroux.  <br>Le monde entier tressaile d'espérance,<br>à cette nuit qui lui donne un Sauveur.<br>Peu ple, à genoux,  <br>attends   ta délivrance!<br>Noël,    Noël!  <br>Voici   le Rédempteur!  <br>Noël,   Noël!  <br>Voici le Rédempteur! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> De notre foi <br>que la lumière ardente<br>nous guide tous au berceau de l'Enfant,<br>comme autrefois une étoile brillante<br>y conduisit les chefs de l'Orient.  <br>Le Roi des rois naît dans une humble crèche;<br>puissants du jour, fiers de votre grandeur:<br>À votre orgeuil,  <br>c'est de   là que Dieu prêche.<br>Courbez   vos fronts  <br>devant   le Rédempteur!  <br>Courbez   vos fronts  <br>devant le Rédempteur! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Le Rédempteur <br>a brisé toute entrave;<br>la Terre est libre et le Ciel est ouvert.<br>Il voit un frère où n'était qu'un esclave;<br>l'amour unit ceux qu'enchaînait le fer.  <br>Qui lui dira notre reconnaissance?<br>C'est pour nous tous qu'il naît, qu'il souffre et meurt.<br>Peuple, debout!  <br>Chantons   ta délivrance!<br>Noël,   Noël!  <br>Chantons  le Rédempteur!  <br>Noël,   Noël!  <br>Chantons le Rédempteur! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Placide Cappeau (1847) |
| Musique · Music | *Cantique de Noël* <br/> Adolphe Adam (1847) |
---
tags:
  - Noël · Christmas
---

# Il est né, le divin Enfant



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/il_est_ne_le_divin_enfant_IL_EST_NE.pdf"}},
            	metaData:{fileName: "il_est_ne_le_divin_enfant_IL_EST_NE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Il est né le divin Enfant!<br>Jouez hautbois, résonnez musettes!<br>Il est né le divin Enfant!<br>Chantons tous son avènement.<br>Depuis plus de quatre mille ans,<br>nous le promettaient les prophètes:<br>Depuis plus de quatre mille ans,<br>nous attendions cet heureux temps. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ah! qu'il est beau, qu'il est charmant!<br>Ah! que ses grâces sont parfaites!<br>Ah! qu'il est beau, qu'il est charmant!<br>qu'il est doux, ce divin Enfant! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Une étable est son logement,<br>un peu de paille est sa couchette:<br>Une étable est son logement,<br>pour un Dieu, quel abaissement! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Partez, grands rois de l'Orient!<br>Venez vous unir à nos fêtes!<br>Partez, grands rois de l'Orient!<br>Venez adorer cet enfant! </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Il veut nos cœurs, il les attend:<br>Il est là pour faire leur conquête.<br>Il veut nos cœurs, il les attend:<br>Donnons-les lui donc promptement! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Ô Jésus! Ô Roi tout-puissant,<br>Tout petit enfant que vous êtes,<br>Ô Jésus! Ô Roi tout-puissant,<br>Régnez sur nous entièrement! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | français, XIXe siècle; |
| Musique · Music | *Il est né* <br/> air français, XVIIIe siècle |
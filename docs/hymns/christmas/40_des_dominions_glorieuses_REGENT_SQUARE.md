---
tags:
  - Noël · Christmas
---

# Des dominions glorieuses



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/des_dominions_glorieuses_REGENT_SQUARE.pdf"}},
            	metaData:{fileName: "des_dominions_glorieuses_REGENT_SQUARE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Des dominions glorieuses,<br>vous les anges, approchez;<br>avec vos chansons pieuses<br>sa naisance proclamez;<br>Venez louer, venez louer,<br>louez le Roi nouveau-né! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Dans vos champs à minuit, <br>vous les bergers, entendez:<br>une Lumière luit; <br>l'Enfant divin ici naît; </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Vos profondes contemplations,<br>vous les sages, quittez-les<br>pour chercher l'espoir des nations,<br>par son étoile guidés; </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Votre espoir se comblera,<br>vous les saints devant l'autel;<br>Jésus Christ réapparaîtra<br>ce dernier jour éternel; </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Angels, from the realms of glory |
| Texte · Text  | James Montgomery (1816);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Regent Square* <br/> Henry Thomas Smart (1866) |
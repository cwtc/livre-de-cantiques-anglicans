---
tags:
  - Noël · Christmas
---

# Joie dans le monde



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/joie_dans_le_monde_ANTIOCH.pdf"}},
            	metaData:{fileName: "joie_dans_le_monde_ANTIOCH.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Joie dans le monde! Il est venu!<br>La terre, elle a son Roi!<br>Que tout cœur lui prépare un lieu:<br>Le ciel chante sa joie,<br>le ciel chante sa joie,<br>le ciel, le ciel chante sa joie. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Joie sur la terre! Le Sauveur règne!<br>Chantons donc du Seigneur!<br>Les champs, les fleuves, les rocs, les plaines<br>proclament leur bonheur,<br>proclament leur bonheur,<br>proclament, proclament leur bonheur. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Fini les ronces, les épines:<br>C'est sa bénédiction!<br>Chassant le serpent, il arrive<br>sauver sa création,<br>sauver sa création,<br>sauver, sauver sa création. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Il est le Roi de Vérité;<br>sa grâce remplit tout!<br>Donc chaque nation va chanter:<br>si grand est son amour,<br>si grand est son amour,<br>si grand, si grand est son amour. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Joy to the world |
| Texte · Text  | Isaac Watts (1719);<br/>tr. Arlie Coles (2022) |
| Musique · Music | *Antioch* <br/> George F. Handel (1742);<br/>ad. Lowell Mason (1792-1872) |
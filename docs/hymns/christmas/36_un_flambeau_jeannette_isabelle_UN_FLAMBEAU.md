---
tags:
  - Noël · Christmas
---

# Un flambeau, Jeannette, Isabelle



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/un_flambeau_jeannette_isabelle_UN_FLAMBEAU.pdf"}},
            	metaData:{fileName: "un_flambeau_jeannette_isabelle_UN_FLAMBEAU.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Un flambeau, Jeannette, Isabelle;<br>un flambeau, courons au berceau.<br>C'est Jésus, bonnes gens du hameau;<br>le Christ est né! Marie appelle:<br>Ah! Ah! Que la Mère est belle!<br>Ah! Ah! Que l'Enfant est beau. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> C'est un tort quand l'Enfant sommeille,<br>c'est un tort de crier si fort.<br>Taisez vous l'un et l'autre d'abord;<br>Au moindre bruit, Jésus s'éveille.<br>Chut! Chut! Chut, il dort à merveille!<br>Chut! Chut! Chut, voyez comme il dort. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Doucement dans l'étable close,<br>doucement venez un moment.<br>Approchez, que Jésus est charmant!<br>Comme il est pur, comme il est rose!<br>Do! Do! Do, que l'Enfant repose!<br>Do! Do! Do, qu'il rit en dormant. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | provençal, XVIIe siècle;<br/>ad. en français Emile Blémont (1839-1927);<br/>ad. Arlie Coles (2021), str. 3 |
| Musique · Music | *Un flambeau* <br/> air provençal, XVIIe siècle;<br/>ad. Nicolas Saboly (1614-1675) |
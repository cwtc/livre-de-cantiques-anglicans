---
tags:
  - Noël · Christmas
---

# Douce nuit, sainte nuit



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/douce_nuit_sainte_nuit_STILLE_NACHT.pdf"}},
            	metaData:{fileName: "douce_nuit_sainte_nuit_STILLE_NACHT.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Douce nuit, sainte nuit!<br>Tout s'étant endormi,<br>Veille un couple sur leur nouveau-né.<br>Enfant Saint, gentil et langé:<br>Dors en paix céleste!<br>Dors en paix céleste! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Douce nuit, sainte nuit!<br>Aux bergers on l'a dit:<br>Aléluia! L'ange a chanté,<br>Les nouvelles partout propagées.<br>Christ le Sauveur est né!  <br>Christ le Sauveur est né!   </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Douce nuit, sainte nuit!<br>Fils de Dieu; ô qu'il rit!<br>De ta bouche l'amour est venu;<br>Maintenant, l'heure de salut.<br>Seigneur dès ta naissance!<br>Seigneur dès ta naissance! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Stille Nacht, heilige Nacht |
| Texte · Text  | Franz Joseph Mohr (1792-1848);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Stille Nacht* <br/> Franz Xavier Gruber (1787-1863) |
---
tags:
  - Soir · Evening
---

# Le jour décline et la nuit tombe



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/le_jour_decline_et_la_nuit_tombe_ST_CLEMENT.pdf"}},
            	metaData:{fileName: "le_jour_decline_et_la_nuit_tombe_ST_CLEMENT.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Le jour décline, et la nuit tombe<br>selon ta volonté, Seigneur;<br>Au matin chantons tes louanges;<br>au soir, reposons-nous sans peur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Nous rendons grâce avec l'église<br>partout sur la Terre tournante;<br>Elle fait la veille, insoumise,<br>en poussant quelque part son chant. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Comme à chaque île de la Terre<br>la nouvelle aube vient toujours,<br>ainsi se poussent ses prières:<br>son los ne voit jamais la mort. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Le soleil part enfin réveiller<br>nos frères vivant dans l'Ouest,<br>heure après heure, pour relayer<br>ta bonté dans les lieux célestes. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td>  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | The day thou gavest, Lord, is ended |
| Texte · Text  | John Ellerton (1870);<br/>tr. Arlie Coles (2022) |
| Musique · Music | *St Clement* <br/> Clement C. Scholefield (1874) |
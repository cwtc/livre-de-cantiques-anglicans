---
tags:
  - Soir · Evening
  - Hymne du bréviaire · Office hymn
---

# Avant la fin du jour qui fuit



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/avant_la_fin_du_jour_qui_fuit_TE_LUCIS_ANTE_TERMINUM.pdf"}},
            	metaData:{fileName: "avant_la_fin_du_jour_qui_fuit_TE_LUCIS_ANTE_TERMINUM.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Avant la fin du jour qui fuit,<br>de ton grand amour infini,<br>Créateur des immenses cieux,<br>sois notre Gardien, ô Dieu. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Chasse les rêves dangereux<br>et les fantômes malheureux;<br>d'ici terrasse l'ennemi;<br>garde nos cœurs purs cette nuit. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> O Père clément, fait ainsi,<br>par Jésus Christ ton Fils chéri,<br>avec le Saint Esprit vivant<br>et règnant éternellement. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Te lucis ante terminum |
| Texte · Text  | inconnu, attr. Saint Ambroise (340-397)<br/>tr. *La Poésie du bréviaire* (1899), ad. Arlie Coles (2021), str. 1-2<br/>tr. Arlie Coles (2021), str. 3 |
| Musique · Music | *Te lucis ante terminum* <br/> chant grégorien, attr. *Liber Usualis*, XIe siècle |
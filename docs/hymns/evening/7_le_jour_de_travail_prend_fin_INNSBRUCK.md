---
tags:
  - Soir · Evening
---

# Le jour de travail prend fin



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/le_jour_de_travail_prend_fin_INNSBRUCK.pdf"}},
            	metaData:{fileName: "le_jour_de_travail_prend_fin_INNSBRUCK.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Le jour de travail prend fin,<br>s'endormant jusqu’à demain;<br>l’ombre voit le terrain:<br>Pendant la tombée de la nuit,<br>que l'on fasse appel sur lui;<br>louons le Créateur divin. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> La glorieuse splendeur,<br>brillante étoile tendre,<br>venant du ciel qui luit;<br>et l’Homme, voyant la merveille,<br>oublie son état tombé<br>dû à la beauté pas à lui. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td>  </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Bien que notre cécité<br>ait l’amour de Dieu manqué,<br>donc on est en querelle;<br>quand notre journée finira,<br>la nuit mortelle verra<br>les champs de la vie éternelle. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | The duteous day now closeth |
| Texte · Text  | Paul Gerhardt (1676);<br/>tr. en anglais Robert Bridges (1830)<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Innsbruck* <br/> Heinrich Isaac (1539);<br/>harm. J. S. Bach (1729) |
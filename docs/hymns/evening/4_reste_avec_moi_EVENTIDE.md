---
tags:
  - Soir · Evening
---

# Reste avec moi



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/reste_avec_moi_EVENTIDE.pdf"}},
            	metaData:{fileName: "reste_avec_moi_EVENTIDE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Reste avec moi: c'est l'heure où le jour baisse.<br>L'ombre grandit; Seigneur, attarde toi.<br>Tous les appuis manquent à ma faiblesse:<br>Force du faible, ô Christ, reste avec moi. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Le flot des jours rapidement s'écoule;<br>Leur gloire est vaine et leur bonheur déçoit.<br>Tout change et meurt, tout chancelle et s'écroule:<br>Toi qui ne changes point, reste avec moi. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> J'ose implorer plus qu'un regard qui passe;<br>Viens, comme à tes disciples autrefois.<br>Plein de douceur, de tendresse et de grâce,<br>et pour toujours, Seigneur, reste avec moi. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Viens, mais non pas armé de ta colère!<br>Parle à mon cœur, apaise mon émoi!<br>Étends sur moi ton aile tutélaire;<br>Ami des péagers, reste avec moi. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Heure après heure, il me faut ta présence;<br>Le tentateur ne redoute que toi.<br>Qui donc prendrait contre lui ma défense?<br>Dans l'ombre ou la clarté, reste avec moi. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Montre ta Croix à ma vue expirante!<br>Et que ton ciel s'illumine à ma foi!<br>L'ombre s'enfuit, voici l'aube éclatante!<br>Dans la vie et la mort, reste avec moi. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Abide with me |
| Texte · Text  | Henry F. Lyte (1847);<br/>tr. Ruben Sailens (1855-1942);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Eventide* <br/> William H. Monk (1861) |
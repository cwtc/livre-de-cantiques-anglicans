---
tags:
  - Soir · Evening
---

# Seigneur, quand vient l'obscurité



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/seigneur_quand_vient_l'obscurite_TALLIS_CANON.pdf"}},
            	metaData:{fileName: "seigneur_quand_vient_l'obscurite_TALLIS_CANON.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Seigneur, quand vient l'obscurité,<br>béni sois-tu pour ta clarté,<br>et quand sur nous la nuit descend,<br>dans tes bras garde tes enfants. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Pardonné-moi mon vil péché<br>au nom de ton Fils bien-aimé,<br>afin que je m'endorme en paix<br>sous ton regard, ô Dieu parfait. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Tu m'apprends, et, Seigneur, je vis:<br>la tombe n'est qu'un autre lit.<br>Tu m'appelles, Seigneur; je meurs:<br>j'attendrai donc mon Réveilleur. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Fais qu'en toi je repose, ô Dieu,<br>et quand je fermerai les yeux,<br>restaure-moi par le sommeil<br>pour mieux te servir au réveil. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Si parfois le sommeil me fuit,<br>en toi je cherche mon appui.<br>Seigneur, protège mon repos<br>et garde-moi de tous les maux. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Chantez à Dieu, vous ses enfants,<br>et vous ses anges triomphants,<br>Gloire à celui qui nous bénit,<br>au Père, au Fils, au Saint Esprit. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | All praise to thee, my God, this night |
| Texte · Text  | Thomas Ken (1637-1711);<br/>tr. Flossette du Pasquier (née 1922), str. 1-2, 4-6;<br/>tr. Arlie Coles (2022), str. 3 |
| Musique · Music | *Tallis Canon* <br/> Thomas Tallis (1505-1585) |
---
tags:
  - Epiphanie · Epiphany
---

# Astre brillant qui répands sur la terre



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/astre_brillant_qui_repands_sur_la_terre_LIEBSTER_IMMANUEL.pdf"}},
            	metaData:{fileName: "astre_brillant_qui_repands_sur_la_terre_LIEBSTER_IMMANUEL.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Astre brillant qui répands sur la terre,<br>Un vif éclat qui dirige nos pas,<br>À l'orient nos sentiers tu éclaires,<br>Car un Sauveur nous est né ici-bas. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Dans son berceau la rosée étincelle,<br>Jésus repose auprès des animaux,<br>Les anges louent l'enfant qui sommeille<br>et qui déjà sur lui prend tous nos maux. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Donnerons-nous en offrande sacrée<br>parfums d'Edom ou perles de la mer?<br>Joyaux sans prix ou la myrrhe embaumée,<br>or précieux, trésors de l'univers? </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Les dons les plus précieux de la terre<br>n'obtiendront pas sa grâce et ses faveurs:<br>des pauvres gens Dieu bénit la misère<br>et ce qu'il veut, c'est le don de nos cœurs. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td>  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Brightest and best of the sons of the morning |
| Texte · Text  | Reginald Heber (1811);<br/>tr. Flossette du Pasquier (née 1922) |
| Musique · Music | *Liebster Immanuel* <br/> *Himmels-Lust* (1675);<br/>harm. J. S. Bach (1685-1750) |
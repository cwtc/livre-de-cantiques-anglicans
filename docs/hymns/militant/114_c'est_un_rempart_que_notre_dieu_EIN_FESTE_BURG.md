---
tags:
  - L'Eglise militante · The Church militant
---

# C'est un rempart que notre Dieu



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/c'est_un_rempart_que_notre_dieu_EIN_FESTE_BURG.pdf"}},
            	metaData:{fileName: "c'est_un_rempart_que_notre_dieu_EIN_FESTE_BURG.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> C'est un rempart que notre Dieu,<br>Une invincible armure,<br>notre délivrance en tout lieu,<br>notre défense sûre.<br>L'ennemi contre nous<br>redouble de courroux,<br>Vaine colère! Que pourrait l'adversaire?<br>L'Éternel détourne ses coups. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Seuls, nous bronchons à chaque pas;<br>notre force est faiblesse;<br>mais un héros dans les combats<br>pour nous lutte sans cesse.<br>Quel est ce défenseur?<br>C'est toi, divin Sauveur!<br>Dieu des armées, tes tribus opprimées<br>connaissent leur libérateur. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Que les démons forgent des fers<br>pour accabler l'Église;<br>Ta Sion brave les enfers,<br>sur le Rocher assise.<br>Constant dans son effort,<br>en vain avec la mort<br>Satan conspire<br>pour saper son empire:<br>Il suffit d'un mot du Dieu fort. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Dis-le, ce mot victorieux,<br>dans toutes nos détresses;<br>répands sur nous du haut des cieux<br>tes divines largesses.<br>Qu'on nous ôte nos biens,<br>qu'on serre nos liens,<br>que nous importe!<br>Ta grâce est la plus forte,<br>et ton Royaume est pour les tiens. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Ein feste Burg ist unser Gott |
| Texte · Text  | Martin Luther (1529);<br/>tr. Henri Lutteroth (1845) |
| Musique · Music | *Ein feste Burg* <br/> Martin Luther (1529);<br/>harm. J. S. Bach (c. 1735) |
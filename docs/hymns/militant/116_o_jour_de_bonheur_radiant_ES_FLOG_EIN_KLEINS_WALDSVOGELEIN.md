---
tags:
  - L'Eglise militante · The Church militant
---

# O jour de bonheur radiant



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/o_jour_de_bonheur_radiant_ES_FLOG_EIN_KLEINS_WALDSVOGELEIN.pdf"}},
            	metaData:{fileName: "o_jour_de_bonheur_radiant_ES_FLOG_EIN_KLEINS_WALDSVOGELEIN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ô jour de bonheur radiant,<br>jour de félicité,<br>ô baume réconciliant,<br>d’une unique beauté;<br>les humbles comme les puissants,<br>ce jour, ils ont chanté<br>le Saint, saint, saint! <br>vers Dieu en Trinité. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ce jour à la création,<br>la lumière naquit;<br>ce jour pour la rédemption<br>ressuscita le Christ;<br>notre Seigneur victorieux<br>nous envoya l’Esprit,<br>donc c’est ce jour très glorieux<br>qu’on voit la lumière triple. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Aujourd'hui tombe aux nations<br>cette Manne du ciel;<br>aux saintes convocations<br>cette trompette appelle.<br>Le don de l'Évangile<br>rayonne purement;<br>de la Source de Vie<br>coule cette eau vivante. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Fais-nous voir un aperçu<br>dans ce jour de repos<br>du repos si attendu<br>de ton grand saint troupeau!<br>À toi nous rendons gloire,<br>toi, trois en un unit,<br>en chantant ta victoire,<br>Père, Fils, Saint-Esprit. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O day of radiant gladness |
| Texte · Text  | Christopher Wordsworth (1862)<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Es flog ein kleins Waldsvögelein* <br/> air allemand, XVIIe siècle |
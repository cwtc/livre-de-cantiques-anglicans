

## A

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Aberystwyth | *Jésus, ô Nom qui surpasse* | Louange · Praise | [84](../../hymns\praise\84_jesus_o_nom_qui_surpasse_ABERYSTWYTH) |
| Adeste fideles | *O peuple fidèle* | Noël · Christmas | [39](../../hymns\christmas\39_o_peuple_fidele_ADESTE_FIDELES) |
| Aeterna Christi Munera | *Les dons éternels du grand Roi* | L'Eglise triomphante · The Church triumphant | [118](../../hymns\triumphant\118_les_dons_eternels_du_grand_roi_AETERNA_CHRISTI_MUNERA) |
| Antioch | *Joie dans le monde* | Noël · Christmas | [38](../../hymns\christmas\38_joie_dans_le_monde_ANTIOCH) |
| Aurelia | *L'église universelle a pour roc Jésus Christ* | L'Eglise militante · The Church militant | [112](../../hymns\militant\112_l'eglise_universelle_a_pour_roc_jesus_christ_AURELIA) |
| Azmon | *Seigneur, que n'ai-je mille voix* | Louange · Praise | [89](../../hymns\praise\89_seigneur_que_n'ai_je_mille_voix_AZMON) |


## B

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Bedford | *Ouvre tes portes, ô Sion* | Chandeleur · Candlemas | [49](../../hymns\candlemas\49_ouvre_tes_portes_o_sion_BEDFORD) |
| Bethany | *Mon Dieu, plus près de toi* | Louange · Praise | [78](../../hymns\praise\78_mon_dieu_plus_pres_de_toi_BETHANY) |


## C

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Cantique de Noël | *Minuit, chrétiens* | Noël · Christmas | [34](../../hymns\christmas\34_minuit_chretiens_CANTIQUE_DE_NOEL) |
| Christe sanctorum | *Père, on te loue; la nuit se termine* | Matin · Morning | [1](../../hymns\morning\1_pere_on_te_loue_la_nuit_se_termine_CHRISTE_SANCTORUM) |
| Coelites plaudant | *Jésus, la gloire et l'honneur des anges* | L'Eglise militante · The Church militant | [115](../../hymns\militant\115_jesus_la_gloire_et_l'honneur_des_anges_COELITES_PLAUDANT) |
| Conditor alme siderum | *Créateur des cieux étoilés* | Avent · Advent | [15](../../hymns\advent\15_createur_des_cieux_etoiles_CONDITOR_ALME_SIDERUM) |
| Coronation | *Salut, le Nom du Redempteur* | Louange · Praise | [71](../../hymns\praise\71_salut_le_nom_du_redempteur_CORONATION) |
| Cradle Song | *Dans l'humble mangeoire* | Noël · Christmas | [20](../../hymns\christmas\20_dans_l'humble_mangeoire_CRADLE_SONG) |
| Cwm Rhondda | *Guide-moi, Berger fidèle* | Louange · Praise | [77](../../hymns\praise\77_guide_moi_berger_fidele_CWM_RHONDDA) |
| Cà, bergers | *Cà, bergers* | Noël · Christmas | [31](../../hymns\christmas\31_ca_bergers_CA_BERGERS) |


## D

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Dans cette étable | *Dans cette étable* | Noël · Christmas | [35](../../hymns\christmas\35_dans_cette_etable_DANS_CETTE_ETABLE) |
| Deus tuorum militum | *Amour profond majestueux* | Louange · Praise | [86](../../hymns\praise\86_amour_profond_majestueux_DEUS_TUORUM_MILITUM) |
| Diademata | *A l'Agneau sur son trône* | Louange · Praise | [87](../../hymns\praise\87_a_l'agneau_sur_son_trone_DIADEMATA) |
| Dix | *Jadis les rois mages heureux* | Epiphanie · Epiphany | [48](../../hymns\epiphany\48_jadis_les_rois_mages_heureux_DIX) |
| Duke Street | *Tant que le monde durera* | Louange · Praise | [76](../../hymns\praise\76_tant_que_le_monde_durera_DUKE_STREET) |
| Dulce carmen | *O sainte et douce harmonie* | Avant le Carême · Shrovetide | [50](../../hymns\shrovetide\50_o_sainte_et_douce_harmonie_DULCE_CARMEN) |


## E

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Easter Hymn | *Jésus Christ, il réssuscite* | Pâques · Easter | [63](../../hymns\easter\63_jesus_christ_il_ressuscite_EASTER_HYMN) |
| Ebenezer | *Torrents d'amour et de grâce* | Louange · Praise | [85](../../hymns\praise\85_torrents_d'amour_et_de_grace_EBENEZER) |
| Ein feste Burg | *C'est un rempart que notre Dieu* | L'Eglise militante · The Church militant | [114](../../hymns\militant\114_c'est_un_rempart_que_notre_dieu_EIN_FESTE_BURG) |
| Entre le boeuf | *Entre le boeuf et l'âne gris* | Noël · Christmas | [32](../../hymns\christmas\32_entre_le_boeuf_et_l'ane_gris_ENTRE_LE_BOEUF) |
| Es flog ein kleins Waldsvögelein | *O jour de bonheur radiant* | L'Eglise militante · The Church militant | [116](../../hymns\militant\116_o_jour_de_bonheur_radiant_ES_FLOG_EIN_KLEINS_WALDSVOGELEIN) |
| Es ist ein Ros | *D'un arbre séculaire* | Noël · Christmas | [37](../../hymns\christmas\37_d'un_arbre_seculaire_ES_IST_EIN_ROS) |
| Eventide | *Reste avec moi* | Soir · Evening | [4](../../hymns\evening\4_reste_avec_moi_EVENTIDE) |


## F

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Forest Green | *Petite ville, Bethléem* | Noël · Christmas | [23](../../hymns\christmas\23_petite_ville_bethleem_FOREST_GREEN) |


## G

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Gabriel's Message | *Quand Gabriel du ciel est descendu* | Annonciation · Annunciation | [9](../../hymns\annunciation\9_quand_gabriel_du_ciel_est_descendu_GABRIELS_MESSAGE) |
| Gaudeamus Pariter | *Venez, fidèles, chantez* | Pâques · Easter | [65](../../hymns\easter\65_venez_fideles_chantez_GAUDEAMUS_PARITER) |
| Genevan 42 | *Consolez mon très cher peuple* | Avent · Advent | [19](../../hymns\advent\19_consolez_mon_tres_cher_peuple_GENEVAN_42) |
| | *Comme un cerf altéré brâme* | Louange · Praise | [96](../../hymns\praise\96_comme_un_cerf_altere_brame_GENEVAN_42) |
| Gloria | *Les anges dans nos campagnes* | Noël · Christmas | [26](../../hymns\christmas\26_les_anges_dans_nos_campagnes_GLORIA) |
| God rest you merry | *Que Dieu vous garde heureux, ô gens* | Noël · Christmas | [28](../../hymns\christmas\28_que_dieu_vous_garde_heureux_o_gens_GOD_REST_YOU_MERRY) |
| Greensleeves | *Qui est cet Enfant endormi* | Noël · Christmas | [44](../../hymns\christmas\44_qui_est_cet_enfant_endormi_GREENSLEEVES) |


## H

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Hanover | *Louons du Seigneur* | Louange · Praise | [90](../../hymns\praise\90_louons_du_seigneur_HANOVER) |
| | *Protège l'enfance, Jésus, Bon Pasteur* | Louange · Praise | [95](../../hymns\praise\95_protege_l'enfance_jesus_bon_pasteur_HANOVER) |
| | *Vers toi, divin Père* | Communion · Communion | [98](../../hymns\communion\98_vers_toi_divin_pere_HANOVER) |
| Heinlein | *Pendant ces quarante jours* | Carême · Lent | [51](../../hymns\lent\51_pendant_ces_quarante_jours_HEINLEIN) |
| Helmsley | *Voici, il descend parmi les nuages* | Avent · Advent | [13](../../hymns\advent\13_voici_il_descend_parmi_les_nuages_HELMSLEY) |
| Herzliebster Jesu | *Ah, très cher Jésus* | Passion · Passiontide | [55](../../hymns\passion\55_ah_tres_cher_jesus_HERZLIEBSTER_JESU) |
| Hyfrydol | *Amour dépassant tout autre* | Louange · Praise | [73](../../hymns\praise\73_amour_depassant_tout_autre_HYFRYDOL) |
| | *Alléluia, chante à Jésus* | Communion · Communion | [102](../../hymns\communion\102_alleluia_chante_a_jesus_HYFRYDOL) |


## I

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Ihr Kinderlein kommet | *Bambins et gamines* | Noël · Christmas | [42](../../hymns\christmas\42_bambins_et_gamines_IHR_KINDERLEIN_KOMMET) |
| Il est né | *Il est né, le divin Enfant* | Noël · Christmas | [25](../../hymns\christmas\25_il_est_ne_le_divin_enfant_IL_EST_NE) |
| In dulci jubilo | *Chantons Noël, voici le jour* | Noël · Christmas | [41](../../hymns\christmas\41_chantons_noel_voici_le_jour_IN_DULCI_JUBILO) |
| Innsbruck | *Le jour de travail prend fin* | Soir · Evening | [7](../../hymns\evening\7_le_jour_de_travail_prend_fin_INNSBRUCK) |
| | *Saint aliment des âmes* | Communion · Communion | [100](../../hymns\communion\100_saint_aliment_des_ames_INNSBRUCK) |
| Italian Hymn | *Viens, ô Roi tout-puissant* | Trinité · Trinity | [69](../../hymns\trinity\69_viens_o_roi_tout_puissant_ITALIAN_HYMN) |


## J

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Judas Maccabaeus | *A toi la gloire* | Pâques · Easter | [62](../../hymns\easter\62_a_toi_la_gloire_JUDAS_MACCABAEUS) |


## K

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| King's Weston | *Qu'au saint Nom de Jésus* | Louange · Praise | [72](../../hymns\praise\72_qu'au_saint_nom_de_jesus_KINGS_WESTON) |


## L

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Lasst uns erfreuen | *Vous, créatures du Seigneur* | Louange · Praise | [81](../../hymns\praise\81_vous_creatures_du_seigneur_LASST_UNS_ERFREUEN) |
| Lauda anima | *Loue, âme, le Roi de gloire* | Louange · Praise | [75](../../hymns\praise\75_loue_ame_le_roi_de_gloire_LAUDA_ANIMA) |
| Leoni | *Le grand Dieu d'Abraham* | Louange · Praise | [91](../../hymns\praise\91_le_grand_dieu_d'abraham_LEONI) |
| Liebster Immanuel | *Astre brillant qui répands sur la terre* | Epiphanie · Epiphany | [47](../../hymns\epiphany\47_astre_brillant_qui_repands_sur_la_terre_LIEBSTER_IMMANUEL) |
| Llanfair | *Gloire au Christ victorieux* | Ascension · Ascension | [66](../../hymns\ascension\66_gloire_au_christ_victorieux_LLANFAIR) |
| Lobe den Herren | *Louons le Dieu puissant* | Louange · Praise | [93](../../hymns\praise\93_louons_le_dieu_puissant_LOBE_DEN_HERREN) |
| Lourdes Hymn | *Que l'amour m'enflamme* | Louange · Praise | [94](../../hymns\praise\94_que_l'amour_m'enflamme_LOURDES_HYMN) |
| Love unknown | *Hors pair est cet amour* | Passion · Passiontide | [58](../../hymns\passion\58_hors_pair_est_cet_amour_LOVE_UNKNOWN) |


## M

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Melita | *Père éternel dont le pouvoir* | Sur mer · At sea | [111](../../hymns\sea\111_pere_eternel_dont_le_pouvoir_MELITA) |
| Mendelssohn | *Ecoutez le chant du ciel* | Noël · Christmas | [27](../../hymns\christmas\27_ecoutez_le_chant_du_ciel_MENDELSSOHN) |
| Merton | *Une voix retentissante* | Avent · Advent | [17](../../hymns\advent\17_une_voix_retentissante_MERTON) |
| Mueller | *Dans l'humble mangeoire* | Noël · Christmas | [21](../../hymns\christmas\21_dans_l'humble_mangeoire_MUELLER) |


## N

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Noël | *Montez à Dieu, chants d'allegresse* | Noël · Christmas | [29](../../hymns\christmas\29_montez_a_dieu_chants_d'allegresse_NOEL) |
| Noël nouvelet | *Noël nouvelet* | Noël · Christmas | [46](../../hymns\christmas\46_noel_nouvelet_NOEL_NOUVELET) |
| Nun danket | *Louons le Créateur* | Louange · Praise | [92](../../hymns\praise\92_louons_le_createur_NUN_DANKET) |


## O

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| O Quantia Qualia | *Il est aux cieux une joie immortelle* | L'Eglise triomphante · The Church triumphant | [119](../../hymns\triumphant\119_il_est_aux_cieux_une_joie_immortelle_O_QUANTIA_QUALIA) |
| O Traurigkeit | *Quelle douleur* | Passion · Passiontide | [59](../../hymns\passion\59_quelle_douleur_O_TRAURIGKEIT) |
| Old Hundredth | *Vous qui sur la terre habitez* | Louange · Praise | [79](../../hymns\praise\79_vous_qui_sur_la_terre_habitez_OLD_HUNDREDTH) |


## P

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Paderborn | *O Vièrge Marie* | Annonciation · Annunciation | [10](../../hymns\annunciation\10_o_vierge_marie_PADERBORN) |
| Pange lingua | *Chante, ô langue, du mystère* | Communion · Communion | [105](../../hymns\communion\105_chante_o_langue_du_mystere_PANGE_LINGUA) |
| Passion Chorale | *O front sacré, accablé* | Passion · Passiontide | [56](../../hymns\passion\56_o_front_sacre_accable_PASSION_CHORALE) |
| Picardy | *Viens silencieux, chair mortelle* | Communion · Communion | [106](../../hymns\communion\106_viens_silencieux_chair_mortelle_PICARDY) |


## Q

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Quam Dilecta | *Nous aimons le saint lieu* | Dedication d'une église · Dedication of a church | [107](../../hymns\dedication\107_nous_aimons_le_saint_lieu_QUAM_DILECTA) |
| Quelle est cette odeur | *Quelle est cette odeur agréable* | Noël · Christmas | [33](../../hymns\christmas\33_quelle_est_cette_odeur_agreable_QUELLE_EST_CETTE_ODEUR) |


## R

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Ratisbon | *Christ, le ciel montre ta gloire* | Matin · Morning | [2](../../hymns\morning\2_christ_le_ciel_montre_ta_gloire_RATISBON) |
| Regent Square | *Des dominions glorieuses* | Noël · Christmas | [40](../../hymns\christmas\40_des_dominions_glorieuses_REGENT_SQUARE) |
| Rendez à Dieu | *Rendez à Dieu louange et gloire* | Louange · Praise | [80](../../hymns\praise\80_rendez_a_dieu_louange_et_gloire_RENDEZ_A_DIEU) |
| | *Dans l'amour, il rompit ce Pain* | Communion · Communion | [104](../../hymns\communion\104_dans_l'amour_il_rompit_ce_pain_RENDEZ_A_DIEU) |
| Richmond | *Entendez-vous ces cris joyeux* | Avent · Advent | [16](../../hymns\advent\16_entendez_vous_ces_cris_joyeux_RICHMOND) |


## S

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Sagina | *Dans ton amour, divin Sauveur* | Louange · Praise | [82](../../hymns\praise\82_dans_ton_amour_divin_sauveur_SAGINA) |
| Saint Anne | *O Dieu, notre aide aux temps anciens* | Louange · Praise | [74](../../hymns\praise\74_o_dieu_notre_aide_aux_temps_anciens_SAINT_ANNE) |
| Saint Cuthbert | *Avant de quitter ce bas lieu* | Pentecôte · Pentecost | [68](../../hymns\pentecost\68_avant_de_quitter_ce_bas_lieu_SAINT_CUTHBERT) |
| Saint George Windsor | *Reconnaissants, nous venons* | Action de grâce · Thanksgiving | [110](../../hymns\thanksgiving\110_reconnaissants_nous_venons_SAINT_GEORGE_WINDSOR) |
| Saint Louis | *Petite ville, Bethléem* | Noël · Christmas | [22](../../hymns\christmas\22_petite_ville_bethleem_SAINT_LOUIS) |
| Saint Theodulph | *Tout honneur, toute gloire* | Passion · Passiontide | [52](../../hymns\passion\52_tout_honneur_toute_gloire_SAINT_THEODULPH) |
| Sainte Agnes | *En ton nom, Seigneur, nous prions* | Rogation · Rogation | [70](../../hymns\rogation\70_en_ton_nom_seigneur_nous_prions_SAINTE_AGNES) |
| Sainte Catherine | *La foi de nos aieux vivante* | L'Eglise triomphante · The Church triumphant | [117](../../hymns\triumphant\117_la_foi_de_nos_aieux_vivante_SAINTE_CATHERINE) |
| Sainte Elisabeth | *Beau Seigneur Jésus* | Louange · Praise | [88](../../hymns\praise\88_beau_seigneur_jesus_SAINTE_ELISABETH) |
| Salzburg | *Au banquet du Roi on chante* | Communion · Communion | [101](../../hymns\communion\101_au_banquet_du_roi_on_chante_SALZBURG) |
| Schmücke dich | *Orne-toi, âme, de bonheur* | Communion · Communion | [99](../../hymns\communion\99_orne_toi_ame_de_bonheur_SCHMUCKE_DICH) |
| Sine Nomine | *Pour tous les saints* | L'Eglise triomphante · The Church triumphant | [120](../../hymns\triumphant\120_pour_tous_les_saints_SINE_NOMINE) |
| Slane | *Sois ma vision* | Louange · Praise | [97](../../hymns\praise\97_sois_ma_vision_SLANE) |
| Song 34 | *O Dieu, Seigneur de vérité* | Matin · Morning | [3](../../hymns\morning\3_o_dieu_seigneur_de_verite_SONG_34) |
| Song 46 | *Gouttez, mes larmes* | Passion · Passiontide | [53](../../hymns\passion\53_gouttez_mes_larmes_SONG_46) |
| St Clement | *Le jour décline et la nuit tombe* | Soir · Evening | [6](../../hymns\evening\6_le_jour_decline_et_la_nuit_tombe_ST_CLEMENT) |
| St Magnus Clarke | *Le front d'épines couronné* | Ascension · Ascension | [67](../../hymns\ascension\67_le_front_d'epines_couronne_ST_MAGNUS_CLARKE) |
| St Thomas (Williams) | *J'aime ton beau royaume* | Dedication d'une église · Dedication of a church | [109](../../hymns\dedication\109_j'aime_ton_beau_royaume_ST_THOMAS_(WILLIAMS)) |
| Stabat Mater | *La Mère était douloureuse* | Passion · Passiontide | [54](../../hymns\passion\54_la_mere_etait_douloureuse_STABAT_MATER) |
| Stille Nacht | *Douce nuit, sainte nuit* | Noël · Christmas | [24](../../hymns\christmas\24_douce_nuit_sainte_nuit_STILLE_NACHT) |
| Stuttgart | *Viens, ô Jésus très attendu* | Avent · Advent | [18](../../hymns\advent\18_viens_o_jesus_tres_attendu_STUTTGART) |


## T

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Tallis Canon | *Seigneur, quand vient l'obscurité* | Soir · Evening | [5](../../hymns\evening\5_seigneur_quand_vient_l'obscurite_TALLIS_CANON) |
| Te lucis ante terminum | *Avant la fin du jour qui fuit* | Soir · Evening | [8](../../hymns\evening\8_avant_la_fin_du_jour_qui_fuit_TE_LUCIS_ANTE_TERMINUM) |
| The First Noel | *La première fois que Noël se disait* | Noël · Christmas | [45](../../hymns\christmas\45_la_premiere_fois_que_noel_se_disait_THE_FIRST_NOEL) |


## U

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Un flambeau | *Un flambeau, Jeannette, Isabelle* | Noël · Christmas | [36](../../hymns\christmas\36_un_flambeau_jeannette_isabelle_UN_FLAMBEAU) |
| Unde et memores | *Attentifs à l'amour qui nous rachète* | Communion · Communion | [103](../../hymns\communion\103_attentifs_a_l'amour_qui_nous_rachete_UNDE_ET_MEMORES) |
| Une jeune pucelle | *Chrétiens, prenez courage* | Noël · Christmas | [43](../../hymns\christmas\43_chretiens_prenez_courage_UNE_JEUNE_PUCELLE) |


## V

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Venez, divin Messie | *Venez, divin Messie* | Avent · Advent | [11](../../hymns\advent\11_venez_divin_messie_VENEZ_DIVIN_MESSIE) |
| Veni veni Emmanuel | *O viens, ô viens, Emmanuel* | Avent · Advent | [14](../../hymns\advent\14_o_viens_o_viens_emmanuel_VENI_VENI_EMMANUEL) |
| Vexilla regis prodeunt | *L'étendard vient du Roi des rois* | Passion · Passiontide | [60](../../hymns\passion\60_l'etendard_vient_du_roi_des_rois_VEXILLA_REGIS_PRODEUNT) |
| Victory | *Le grand combat est terminé* | Pâques · Easter | [61](../../hymns\easter\61_le_grand_combat_est_termine_VICTORY) |


## W

| Mélodie · Tune | Prémiere phrase · First line | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Wachet auf | *Levez vous! La voix appelle* | Avent · Advent | [12](../../hymns\advent\12_levez_vous_la_voix_appelle_WACHET_AUF) |
| Webb | *Debout, sainte cohorte* | L'Eglise militante · The Church militant | [113](../../hymns\militant\113_debout_sainte_cohorte_WEBB) |
| Were you there | *Vois là-bas, mettre le Seigneur en croix* | Passion · Passiontide | [57](../../hymns\passion\57_vois_la_bas_mettre_le_seigneur_en_croix_WERE_YOU_THERE) |
| Westminster Abbey | *Le Christ, la pierre angulaire* | Dedication d'une église · Dedication of a church | [108](../../hymns\dedication\108_le_christ_la_pierre_angulaire_WESTMINSTER_ABBEY) |
| Winchester Old | *Les bergers et leurs troupeaux* | Noël · Christmas | [30](../../hymns\christmas\30_les_bergers_et_leurs_troupeaux_WINCHESTER_OLD) |
| Woodworth | *Tel que je suis* | Louange · Praise | [83](../../hymns\praise\83_tel_que_je_suis_WOODWORTH) |
| Würtemburg | *Le Christ est ressuscité* | Pâques · Easter | [64](../../hymns\easter\64_le_christ_est_ressuscite_WURTEMBURG) |
